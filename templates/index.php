<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

$this->setGenerator('PRICEMETER');

defined('_JEXEC') or die;

JHTML::_('behavior.modal');

// Getting params from template
$params = JFactory::getApplication()->getTemplate(true)->params;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->getCfg('sitename');

if($task == "edit" || $layout == "form" )
{
	$fullWidth = 1;
}
else
{
	$fullWidth = 0;
}

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');
$doc->addScript('templates/' .$this->template. '/js/template.js');
$doc->addScript('templates/' .$this->template. '/js/jquery-ui/jquery-ui.min.js');

// Add Stylesheets
$doc->addStyleSheet('templates/general/css/grid.css');
$doc->addStyleSheet('templates/general/css/fonts.css');
$doc->addStyleSheet('templates/general/css/modal.css');
$doc->addStyleSheet('templates/'.$this->template.'/css/fonts.css');
$doc->addStyleSheet('templates/'.$this->template.'/css/template.css');
$doc->addStyleSheet('templates/'.$this->template.'/css/prettyGallery.css');
$doc->addStyleSheet('templates/'.$this->template.'/js/jquery-ui/jquery-ui.css');


// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', false, $this->direction);

// Add current user information
$user = JFactory::getUser();

// Logo file or site title param
if ($this->params->get('logoFile'))
{
	$logo = '<img src="'. JUri::root() . $this->params->get('logoFile') .'" alt="'. $sitename .'" />';
}
elseif ($this->params->get('sitetitle'))
{
	$logo = '<span class="site-title" title="'. $sitename .'">'. htmlspecialchars($this->params->get('sitetitle')) .'</span>';
}
else
{
	$logo = '<span class="site-title" title="'. $sitename .'">'. $sitename .'</span>';
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<meta name="viewport" content="width=1024">
	<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,700italic,400italic&subset=latin,cyrillic-ext,latin-ext,cyrillic' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic&subset=latin,cyrillic-ext,greek-ext,greek,vietnamese,latin-ext,cyrillic' rel='stylesheet' type='text/css'>
	<jdoc:include type="head" />
  
	<!--[if lt IE 9]>
		<script src="<?php echo $this->baseurl ?>/media/jui/js/jquery.prettyGallery.js"></script>
	<![endif]-->
	
	<script>
	 jQuery(document).ready(function(){
     jQuery('a[href*=#]').bind("click", function(e){
        var anchor = jQuery(this);
        jQuery('#contacts').show();
        jQuery('html, body').stop().animate({
           scrollTop: jQuery(anchor.attr('href')).offset().top
        }, 1000);
        e.preventDefault();
     });
     return false;
  });
  jQuery('document').ready(function(){
    jQuery('.clearList a').click(function() {
      jQuery(this).bound.close;
      return false;
    });
  });
	</script> 
  
  <script>
	 jQuery(document).ready(function(){
    jQuery('.geo-com').click(function() {
    		jQuery('.geo-comments').toggle(200,function() {
          jQuery("form.pwebcontact-form .pweb-field-cities .pweb-field .pweb-fields-group").data('jsp').reinitialise();
        });
        jQuery('.geo-com.more').toggle(200);
        jQuery('.geo-com.close').toggle(200);
      
    	return false;
    });
  }); 
	</script>  
  
  <style>
	.modalDialog {display:none;}
  #sbox-window .modalDialog {display:block;}
	</style>  
	
<link rel="stylesheet" type="text/css" media="all" href="<?php echo $this->baseurl ?>/templates/protostar/css/style.css" />
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/protostar/js/jquery.jcarousel.min.js"></script>

<!-- styles needed by jScrollPane -->
<link type="text/css" href="<?php echo $this->baseurl ?>/templates/protostar/css/jquery.jscrollpane.css" rel="stylesheet" media="all" />

<!-- the mousewheel plugin - optional to provide mousewheel support -->
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/protostar/js/jquery.mousewheel.js"></script>

<!-- the jScrollPane script -->
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/protostar/js/jquery.jscrollpane.js"></script>

<script src="<?php echo $this->baseurl ?>/templates/protostar/js/cities.js" type="text/javascript"></script>
		
<script type="text/javascript" id="sourcecode">
			jQuery(function()
			{
				jQuery('form.pwebcontact-form .pweb-field-cities .pweb-field .pweb-fields-group').jScrollPane(); 
			});
</script>  

<script type="text/javascript">

jQuery(function() {

	jQuery.fn.startCarousel = function() {
  		var bodywidth = jQuery('.moduletable-clients').width(),
			itemwidth = jQuery('.mycarousel li').outerWidth(true),		
			mycontwidth = bodywidth > itemwidth ? bodywidth - bodywidth%itemwidth : itemwidth,
			licount = jQuery('.mycarousel li').size(),
			jscroll = 1;
			
		if(licount > mycontwidth/itemwidth){
			jscroll =  mycontwidth/itemwidth;
		} else {
			jscroll = 0;
			mycontwidth = licount * itemwidth;
		}
		
		jQuery('.mycont').width(mycontwidth);
			
    	jQuery('.mycarousel').jcarousel({
			scroll:jscroll
		});
  	};
	
	jQuery(this).startCarousel();
	
	jQuery(window).resize(function(){
		jQuery(this).startCarousel();
	}); 
	
	jQuery('.mycarousel').jcarousel({
		scroll:6
	});
});
</script>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
  
</head>

<body class="Itemid-<?php echo $_REQUEST['Itemid']; ?>">
  <div id="wrapper">
  	<div id="headcontainer">
  		<header>
    		<div class="section group">
  				<div class="col span_1_of_6">
  				  <a class="logo" href="<?php echo $this->baseurl; ?>">
              <?php echo $logo;?> <?php if ($this->params->get('sitedescription')) { echo '<div class="site-description">'. htmlspecialchars($this->params->get('sitedescription')) .'</div>'; } ?>
            </a>
  				</div>
  				<div class="col span_1_of_12">
  				  &nbsp;
  				</div>
  				<div class="col span_1_of_4">
  				  <div class="moduletable-plancalc">
  				    <ul>
    				    <li class="item-15"><a href="#contacts">Заявка на расчет медиаплана</a></li>
    				  </ul>
  				  </div>
  				</div>
  				<div class="col span_1_of_4">
  				  <div class="moduletable-phone">
  				    <p>+7 495 646-88-50</p>
  				  </div>
  				</div>
  				<div class="col span_1_of_4">
                                  <jdoc:include type="modules" name="login" style="xhtml" />
  				</div>
  				<div class="col span_1_of_12">
  				  &nbsp;
  				</div>
  				<div class="col span_1_of_2">
  				  <jdoc:include type="modules" name="mainmenu" style="xhtml" />
  				</div>
  				<div class="col span_1_of_4">
  				  <div class="moduletable-special">
  				    <ul>
    				    <li class="item-17"><a href="#">Спец. предложения</a></li>
    				  </ul>
  				  </div>
  				</div>
  			</div>
  		</header>
  	</div>
  	<div id="maindomaincontainer">
  		<div id="maindomain">
  		  <jdoc:include type="modules" name="mediamenu" style="xhtml" /> 
  		</div>
  	</div>
  	<div id="maincontentcontainer">
      <jdoc:include type="modules" name="cover" style="xhtml" />
  		<div id="maincontent">
  		  <jdoc:include type="modules" name="services" style="xhtml" />
      </div>
    </div>
    <jdoc:include type="modules" name="services-page" style="xhtml" />
  	<jdoc:include type="message" />
    <jdoc:include type="component" />
  	<jdoc:include type="modules" name="map" style="xhtml" />
  	<div id="maincontentcontainer">
  		<div id="maincontent">
        <jdoc:include type="modules" name="clients" style="xhtml" />
  		</div>
  	</div>
  	<div id="contacts">
      <div class="wrapper">
        <jdoc:include type="modules" name="contacts" style="xhtml" />
      </div>
    </div>
    <div id="contacts page">
      <div class="wrapper">
        <jdoc:include type="modules" name="contacts-page" style="xhtml" />
      </div>
    </div>
    <div id="maincontentcontainer">
  		<div id="maincontent">
        <jdoc:include type="modules" name="clients-1" style="xhtml" />
  		</div>
  	</div>
  	<div id="footercontainer">
  		<footer class="group">
  		  <div class="wrapper">
    			<div class="col span_1_of_4">
            <img src="images/akar.png">
    			</div>
    			<div class="col span_1_of_4 footpad-1">
    			  <p class="copyright">&copy; <?php echo $sitename; ?>, 2005-<?php echo date('Y');?></p> 
    			  <p>Все права принадлежат<br />&laquo;ООО Медиа Импакт&raquo;</p>
    			</div>
          <div class="col span_5_of_12 footpad-1">
    			  <p><span class="phone">+7 495 646-88-50</span><span class="email"><a href="mailto:info@mediaimpact.ru">info@mediaimpact.ru</a></span></p>
    			  
    			  <p>125130, Россия, город Москва,<br />Старопетровский проезд, дом 7А, строение 6
    			</div>
    			<div class="col span_1_of_12 footpad-2">
            <div class="moduletable-developer">
              <a href="http://www.niknightingale.com" target="_blank"><img src="images/niknightingale.png"></a>
    			  </div>
    			</div>
  			</div>
  		</footer>
  	</div>
  </div>
  <jdoc:include type="modules" name="metrika" style="none" />
	<jdoc:include type="modules" name="debug" style="none" />
</body>
</html>
