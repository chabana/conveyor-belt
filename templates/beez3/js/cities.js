jQuery(function () {
	var allElement = "Вся Россия";
	var citiesFSet = jQuery("[id$='cities']");
	var citiesList = [];
	var citiesChecked = new Set();

	var search = jQuery("<input class='search-cities'/>")
			  .keyup(function () {
				  var value = search.val().toUpperCase();
				  citiesFSet.find("input").each(function () {
					  if (!value || jQuery(this).val().toUpperCase().indexOf(value) == 0) {
						  jQuery(this).parent().show();
					  } else {
						  jQuery(this).parent().hide();
					  }
				  });
			  });
	citiesFSet.parent().before(search);

	var selectedArea = jQuery("<div class='cities-selected-area'><div class='cities-selected-area-scroll'></div></div>");
	jQuery("[class$='cities']").first()
			  .append("<div class='cities-selected-area-title' style='display:none'>Вы выбрали</div>")
			  .append(selectedArea);

   selectedArea.jScrollPane();

	citiesFSet.find("input").each(function () {
		citiesList.push(jQuery(this).val());
		jQuery(this).change(function () {
			var val = jQuery(this).val();

			if (allElement == val) {
				if (jQuery(this).attr("checked")) {
					citiesFSet.find("input").attr("checked", true);
					citiesChecked.clear().add(allElement);
				} else {
					citiesFSet.find("input").attr("checked", false);
					citiesChecked.clear();
				}
			} else {
				var allRus = citiesFSet.find("input[value='"+allElement+"']");
				if(jQuery(this).attr("checked")){
					citiesChecked.add(val);
				} else if (!jQuery(this).attr("checked") && allRus.attr("checked")) {
					allRus.attr("checked", false);
					citiesChecked.clear();
					citiesFSet.find("input").each(function () {
						if(jQuery(this).attr("checked")){
							citiesChecked.add(jQuery(this).val());
						}
					});
				} else {
					citiesChecked.remove(val);
				}
			}
			rerenderSelectedArea();
		});
	});

	rerenderSelectedArea = function () {
		var selectedAreaScroll = selectedArea.find('.cities-selected-area-scroll');
		selectedAreaScroll.html("");
		var cities = citiesChecked.getCities();
		if (cities.length > 0) {
			for (i = 0; i < cities.length; i++) {
				selectedAreaScroll.append(getSelectedCity(cities[i]));
			}
			jQuery(".cities-selected-area-title").show();
		} else {
			jQuery(".cities-selected-area-title").hide();
		}
		jQuery(".cities-selected-area").data('jsp').reinitialise(); 
	};

	getSelectedCity = function (city) {
		return jQuery("<div class='selected-city'>" + city
				  + "<div class='selected-city-del-icon' "
				  + "onclick='unselectCity(\"" + city + "\")'></div></div>");
	};

	unselectCity = function (city) {
		if (allElement == city) {
			citiesFSet.find("input").attr("checked", false);
			citiesChecked.remove(city);
		} else {
			citiesFSet.find("input[value='" + city + "']").attr("checked", false);
			citiesChecked.remove(city);
		}
		rerenderSelectedArea();
	};

	function Set() {
		var citiesChecked = [];

		return {
			add: function (city) {
				if (citiesChecked.indexOf(city) < 0) {
					citiesChecked.push(city);
					//this.sort();
				}
				return this;
			},
			remove: function (city) {
				for (var i = 0; i < citiesChecked.length; i++) {
					if (citiesChecked[i] == city) {
						citiesChecked.splice(i, 1);
						break;
					}
				}
				return this;
			},
			clear: function () {
				citiesChecked = [];
				return this;
			},
			sort: function () {
				citiesChecked = citiesChecked.sort();
				return this;
			},
			getCities: function () {
				return citiesChecked;
			}
		}
	}
});