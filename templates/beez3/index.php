<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

$this->setGenerator('PRICEMETER');

defined('_JEXEC') or die;

JHTML::_('behavior.modal');

// Getting params from template
$params = JFactory::getApplication()->getTemplate(true)->params;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->getCfg('sitename');

if($task == "edit" || $layout == "form" )
{
	$fullWidth = 1;
}
else
{
	$fullWidth = 0;
}

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');
$doc->addScript('templates/' .$this->template. '/js/template.js');

// Add Stylesheets
$doc->addStyleSheet('templates/general/css/grid.css');
$doc->addStyleSheet('templates/general/css/fonts.css');
$doc->addStyleSheet('templates/general/css/modal.css');
$doc->addStyleSheet('templates/'.$this->template.'/css/fonts.css');
$doc->addStyleSheet('templates/'.$this->template.'/css/template.css');
$doc->addStyleSheet('templates/'.$this->template.'/css/prettyGallery.css');


// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', false, $this->direction);

// Add current user information
$user = JFactory::getUser();

// Logo file or site title param
if ($this->params->get('logoFile'))
{
	$logo = '<img src="'. JUri::root() . $this->params->get('logoFile') .'" alt="'. $sitename .'" />';
}
elseif ($this->params->get('sitetitle'))
{
	$logo = '<span class="site-title" title="'. $sitename .'">'. htmlspecialchars($this->params->get('sitetitle')) .'</span>';
}
else
{
	$logo = '<span class="site-title" title="'. $sitename .'">'. $sitename .'</span>';
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<meta name="viewport" content="width=1024">
	<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,700italic,400italic&subset=latin,cyrillic-ext,latin-ext,cyrillic' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic&subset=latin,cyrillic-ext,greek-ext,greek,vietnamese,latin-ext,cyrillic' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	<jdoc:include type="head" />
  
	<!--[if lt IE 9]>
		<script src="<?php echo $this->baseurl ?>/media/jui/js/jquery.prettyGallery.js"></script>
	<![endif]-->
	
<link rel="stylesheet" type="text/css" media="all" href="<?php echo $this->baseurl ?>/templates/protostar/css/style.css" />
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/protostar/js/jquery-1.6.2.min.js"></script>

  <script>
	 jQuery(document).ready(function(){
    jQuery('.login-info #sbox-btn-close').click(function() {
      jQuery('.login-info').hide();
    	return false;
    });
  }); 
	</script> 
  
</head>

<body class="Itemid-<?php echo $_REQUEST['Itemid']; ?>">
  <div id="wrapper">
  	<div id="headcontainer">
  		<div id="headerbox">
  		<header>
    		<div class="section group">
  				<div class="col span_4_of_11">
  				  <a class="logo" href="<?php echo $this->baseurl; ?>">
              <?php echo $logo;?> <?php if ($this->params->get('sitedescription')) { echo '<div class="site-description">'. htmlspecialchars($this->params->get('sitedescription')) .'</div>'; } ?>
            </a>
  			</div>
  				<div class="col span_4_of_8">
  				  <jdoc:include type="modules" name="mainmenu" style="xhtml" />
					<div class="col span_8_of_8">
  				  <jdoc:include type="modules" name="adress" style="xhtml" />
					</div>
  				</div>
  				<div class="col span_1_of_6">
  				  <div class="moduletable-special">
  				    <jdoc:include type="modules" name="specpr" style="xhtml" />
  				  </div>
  				</div>
  			</div>
			
  		</header>
		</div>
  	</div>
		<!--для сервиса-->
	
	<div id="headerbb">
		<jdoc:include type="modules" name="headerbb" style="xhtml" />
	</div>
	<div id="servallcontainer">
		<div id="servallbox">
			<div class="wrapper">
				<div class="col span_2_of_7">
					<jdoc:include type="modules" name="leftmenuh" style="xhtml" />
					<jdoc:include type="modules" name="leftmenu" style="xhtml" />
				</div>
				<div class="col span_5_of_7">
					<jdoc:include type="modules" name="allposition" style="xhtml" />
					<jdoc:include type="modules" name="servblogh" style="xhtml" />
					<jdoc:include type="modules" name="servblog" style="xhtml" />
				</div><jdoc:include type="modules" name="opros" style="xhtml" />
				<jdoc:include type="component" />
			</div>
		</div>
	</div>

	<!--для сервиса-->
  
  <div id="footercontainer">
  		<footer class="group">
  		  <div class="wrapper">
    			<div class="col span_4_of_8">
					<div class="col span_2_of_2">
  				  <jdoc:include type="modules" name="mainmenu" style="xhtml" />
					<div class="col span_2_of_3">
						<p>© "Таргет" 2005–2015</p>
					</div>
					</div>
			
    			</div>
				<div class="col span_4_of_8 footpad-1">
    			  <p>ООО "ТРП "Таргет"<span class="email"><a href="mailto:office@konvejer.com">office@konvejer.com</a></span></p>
    			  
    			  <p>ул. Отакара Яроша, 18-Г 61045 Харьков, Украина
    			</div>
  			</div>
  		</footer>
  	</div>
	</div>
</body>
</html>
