<?php
/**
 * @version		$Id: default.php 1812 2013-01-14 18:45:06Z lefteris.kavadas $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2013 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;
JHTML::_('behavior.modal');
?>

<div id="k2ModuleBox<?php echo $module->id; ?>" class="k2ItemsBlock<?php if($params->get('moduleclass_sfx')) echo ' '.$params->get('moduleclass_sfx'); ?>">
   
	<?php if(count($items)): ?>
  <ul>
    <?php $i = 0; ?>
    <?php foreach ($items as $key=>$item):	?>
    <li class="<?php echo ($key%2) ? "odd" : "even"; if(count($items)==$key+1) echo ' lastItem'; ?>">

      <!-- Plugins: BeforeDisplay -->
      <?php echo $item->event->BeforeDisplay; ?>

      <!-- K2 Plugins: K2BeforeDisplay -->
      <?php echo $item->event->K2BeforeDisplay; ?>

      <?php if($params->get('itemAuthorAvatar')): ?>
      <a class="k2Avatar moduleItemAuthorAvatar" rel="author" href="<?php echo $item->authorLink; ?>">
				<img src="<?php echo $item->authorAvatar; ?>" alt="<?php echo K2HelperUtilities::cleanHtml($item->author); ?>" style="width:<?php echo $avatarWidth; ?>px;height:auto;" />
			</a>
      <?php endif; ?>

      <?php if($params->get('itemTitle')): ?>
      <a class="moduleItemTitle" href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a>
      <?php endif; ?>

      <?php if($params->get('itemAuthor')): ?>
      <div class="moduleItemAuthor">
	      <?php echo K2HelperUtilities::writtenBy($item->authorGender); ?>
	
				<?php if(isset($item->authorLink)): ?>
				<a rel="author" title="<?php echo K2HelperUtilities::cleanHtml($item->author); ?>" href="<?php echo $item->authorLink; ?>"><?php echo $item->author; ?></a>
				<?php else: ?>
				<?php echo $item->author; ?>
				<?php endif; ?>
				
				<?php if($params->get('userDescription')): ?>
				<?php echo $item->authorDescription; ?>
				<?php endif; ?>
				
			</div>
			<?php endif; ?>

      <!-- Plugins: AfterDisplayTitle -->
      <?php echo $item->event->AfterDisplayTitle; ?>

      <!-- K2 Plugins: K2AfterDisplayTitle -->
      <?php echo $item->event->K2AfterDisplayTitle; ?>

      <!-- Plugins: BeforeDisplayContent -->
      <?php echo $item->event->BeforeDisplayContent; ?>

      <!-- K2 Plugins: K2BeforeDisplayContent -->
      <?php echo $item->event->K2BeforeDisplayContent; ?>

      <?php if($params->get('itemImage') || $params->get('itemIntroText')): ?>
      <div class="moduleItemIntrotext">
	      <?php if($params->get('itemImage') && isset($item->image)): ?>
	      <a class="moduleItemImage" href="<?php echo $item->link; ?>" title="<?php echo JText::_('K2_CONTINUE_READING'); ?> &quot;<?php echo K2HelperUtilities::cleanHtml($item->title); ?>&quot;">
	      	<img src="<?php echo $item->image; ?>" alt="<?php echo K2HelperUtilities::cleanHtml($item->title); ?>"/>
	      </a>
	      <?php endif; ?>

      	<?php if($params->get('itemIntroText')): ?>
      	<?php echo $item->introtext; ?>
      	<?php endif; ?>
      </div>
      <?php endif; ?> 

      <?php if($params->get('itemExtraFields') && count($item->extra_fields)): ?>
      <div class="moduleItemExtraFields">
	      <b><?php echo JText::_('K2_ADDITIONAL_INFO'); ?></b>
	      <ul>
	        <?php foreach ($item->extra_fields as $extraField): ?>
					<?php if($extraField->value != ''): ?>
					<li class="type<?php echo ucfirst($extraField->type); ?> group<?php echo $extraField->group; ?>">
						<?php if($extraField->type == 'header'): ?>
						<h4 class="moduleItemExtraFieldsHeader"><?php echo $extraField->name; ?></h4>
						<?php else: ?>
						<span class="moduleItemExtraFieldsLabel"><?php echo $extraField->name; ?></span>
						<span class="moduleItemExtraFieldsValue"><?php echo $extraField->value; ?></span>
						<?php endif; ?>
						<div class="clr"></div>
					</li>
					<?php endif; ?>
	        <?php endforeach; ?>
	      </ul>
      </div>
      <?php endif; ?>

      <div class="clr"></div>

      <?php if($params->get('itemVideo')): ?>
      <div class="moduleItemVideo">
      	<?php echo $item->video ; ?>
      	<span class="moduleItemVideoCaption"><?php echo $item->video_caption ; ?></span>
      	<span class="moduleItemVideoCredits"><?php echo $item->video_credits ; ?></span>
      </div>
      <?php endif; ?>

      <div class="clr"></div>

      <!-- Plugins: AfterDisplayContent -->
      <?php echo $item->event->AfterDisplayContent; ?>

      <!-- K2 Plugins: K2AfterDisplayContent -->
      <?php echo $item->event->K2AfterDisplayContent; ?>

      <?php if($params->get('itemDateCreated')): ?>
      <span class="moduleItemDateCreated"><?php echo JText::_('K2_WRITTEN_ON') ; ?> <?php echo JHTML::_('date', $item->created, JText::_('K2_DATE_FORMAT_LC2')); ?></span>
      <?php endif; ?>

      <?php if($params->get('itemCategory')): ?>
      <?php echo JText::_('K2_IN') ; ?> <a class="moduleItemCategory" href="<?php echo $item->categoryLink; ?>"><?php echo $item->categoryname; ?></a>
      <?php endif; ?>

      <?php if($params->get('itemTags') && count($item->tags)>0): ?>
      <div class="moduleItemTags">
      	<b><?php echo JText::_('K2_TAGS'); ?>:</b>
        <?php foreach ($item->tags as $tag): ?>
        <a href="<?php echo $tag->link; ?>"><?php echo $tag->name; ?></a>
        <?php endforeach; ?>
      </div>
      <?php endif; ?>

      <?php if($params->get('itemAttachments') && count($item->attachments)): ?>
			<div class="moduleAttachments">
				<?php foreach ($item->attachments as $attachment): ?>
				<a title="<?php echo K2HelperUtilities::cleanHtml($attachment->titleAttribute); ?>" href="<?php echo $attachment->link; ?>"><?php echo $attachment->title; ?></a>
				<?php endforeach; ?>
			</div>
      <?php endif; ?>

			<?php if($params->get('itemCommentsCounter') && $componentParams->get('comments')): ?>		
				<?php if(!empty($item->event->K2CommentsCounter)): ?>
					<!-- K2 Plugins: K2CommentsCounter -->
					<?php echo $item->event->K2CommentsCounter; ?>
				<?php else: ?>
					<?php if($item->numOfComments>0): ?>
					<a class="moduleItemComments" href="<?php echo $item->link.'#itemCommentsAnchor'; ?>">
						<?php echo $item->numOfComments; ?> <?php if($item->numOfComments>1) echo JText::_('K2_COMMENTS'); else echo JText::_('K2_COMMENT'); ?>
					</a>
					<?php else: ?>
					<a class="moduleItemComments" href="<?php echo $item->link.'#itemCommentsAnchor'; ?>">
						<?php echo JText::_('K2_BE_THE_FIRST_TO_COMMENT'); ?>
					</a>
					<?php endif; ?>
				<?php endif; ?>
			<?php endif; ?>

			<?php if($params->get('itemHits')): ?>
			<span class="moduleItemHits">
				<?php echo JText::_('K2_READ'); ?> <?php echo $item->hits; ?> <?php echo JText::_('K2_TIMES'); ?>
			</span>
			<?php endif; ?>

			<?php if($params->get('itemReadMore') && $item->fulltext): ?>
			<a class="moduleItemReadMore" href="<?php echo $item->link; ?>">
				<?php echo JText::_('K2_READ_MORE'); ?>
			</a>
			<?php endif; ?>

      <!-- Plugins: AfterDisplay -->
      <?php echo $item->event->AfterDisplay; ?>

      <!-- K2 Plugins: K2AfterDisplay -->
      <?php echo $item->event->K2AfterDisplay; ?>

      <div class="clr"></div>
    </li>
    <?php
      $i++;
      if($i > 23) break; 
      endforeach; 
    ?>
    <li class="clearList"></li>
  </ul>
  <?php endif; ?>
  
  <a class="modal" href="#openModal" rel="{size:{x:1000,y:600}}"><span>Cмотреть весь список</span></a>

  <div id="openModal" class="modalDialog">
    <?php if($params->get('itemPreText')): ?>
  	 <h4><?php echo $params->get('itemPreText'); ?></h4>
  	<?php endif; ?>
    <h5>Автоиздания</h5>
<ul>
<li><a href='#'>Quattroruote</a></li>
<li><a href='#'>Top Gear</a></li>
<li><a href='#'>Автомир</a></li>
<li><a href='#'>Автомир.Тест-драйв</a></li>
<li><a href='#'>Автомобили</a></li>
<li><a href='#'>Автомобили и цены</a></li>
<li><a href='#'>Автопанорама</a></li>
<li><a href='#'>Автопилот (приложение к газете КоммерсантЪ )</a></li>
<li><a href='#'>Авторевю</a></li>
<li><a href='#'>За рулем</a></li>
<li><a href='#'>За рулем (газета)</a></li>
<li><a href='#'>Клаксон</a></li>
<li><a href='#'>Коммерческие авто</a></li>
<li><a href='#'>Купи Авто</a></li>
<li><a href='#'>Мото</a></li>
<li><a href='#'>Пятое колесо</a></li>
<li><a href='#'>Тюнинг автомобилей</a></li>
<li class="clear">&nbsp;</li>
</ul>
<h5>Астрология и изотерика</h5>
<ul>
<li><a href='#'>Лиза.Гороскоп</a></li>
<li><a href='#'>Оракул</a></li>
<li class="clear">&nbsp;</li>
</ul>
<h5>Бизнес</h5>
<ul>
<li><a href='#'>CEO</a></li>
<li><a href='#'>Forbes</a></li>
<li><a href='#'>The Moscow Times</a></li>
<li><a href='#'>Бизнес-журнал</a></li>
<li><a href='#'>Ведомости</a></li>
<li><a href='#'>Власть</a></li>
<li><a href='#'>Генеральный директор</a></li>
<li><a href='#'>Деньги</a></li>
<li><a href='#'>Коммерсантъ</a></li>
<li><a href='#'>Профиль</a></li>
<li><a href='#'>РБК</a></li>
<li><a href='#'>РБК daily</a></li>
<li><a href='#'>Свой бизнес</a></li>
<li><a href='#'>Секрет фирмы</a></li>
<li><a href='#'>Финансовый директор</a></li>
<li><a href='#'>Эксперт</a></li>
<li class="clear">&nbsp;</li>
</ul>
<h5>Женские</h5>
<ul>
<li><a href='#'>1000 секретов</a></li>
<li><a href='#'>Burda</a></li>
<li><a href='#'>Cosmopolitan</a></li>
<li><a href='#'>Cosmopolitan Shopping</a></li>
<li><a href='#'>Elle</a></li>
<li><a href='#'>Elle Girl</a></li>
<li><a href='#'>Glamour</a></li>
<li><a href='#'>Harper's Bazaar</a></li>
<li><a href='#'>InStyle</a></li>
<li><a href='#'>Joy</a></li>
<li><a href='#'>Marie Claire</a></li>
<li><a href='#'>Mini</a></li>
<li><a href='#'>Oops!</a></li>
<li><a href='#'>SnC</a></li>
<li><a href='#'>Vogue</a></li>
<li><a href='#'>Yes!</a></li>
<li><a href='#'>Атмосфера</a></li>
<li><a href='#'>Все для женщины</a></li>
<li><a href='#'>Даша</a></li>
<li><a href='#'>Домашний журнал </a></li>
<li><a href='#'>Домашний очаг</a></li>
<li><a href='#'>Женская магия</a></li>
<li><a href='#'>Женские секреты</a></li>
<li><a href='#'>Женские советы. Самая</a></li>
<li><a href='#'>Караван историй</a></li>
<li><a href='#'>Коллекция. Караван историй</a></li>
<li><a href='#'>Крестьянка</a></li>
<li><a href='#'>Лиза</a></li>
<li><a href='#'>Лиза.Girl</a></li>
<li><a href='#'>Лиза.Добрые советы</a></li>
<li><a href='#'>Счастливая и красивая</a></li>
<li class="clear">&nbsp;</li>
</ul>
<h5>Здоровье, медицина</h5>
<ul>
<li><a href='#'>Women's Health</a></li>
<li><a href='#'>Аргументы и Факты Здоровье</a></li>
<li><a href='#'>Домашний доктор</a></li>
<li><a href='#'>Женское здоровье</a></li>
<li><a href='#'>Здоровье</a></li>
<li><a href='#'>Красота & здоровье</a></li>
<li><a href='#'>Лечебные письма</a></li>
<li><a href='#'>Лечитесь с нами</a></li>
<li><a href='#'>Народный доктор</a></li>
<li><a href='#'>Народный совет</a></li>
<li><a href='#'>Похудей!</a></li>
<li><a href='#'>Худеем правильно</a></li>
<li class="clear">&nbsp;</li>
</ul>
<h5>Интерьер, дизайн</h5>
<ul>
<li><a href='#'>Architectural Digest</a></li>
<li><a href='#'>Elle Decoration</a></li>
<li><a href='#'>Salon interior</a></li>
<li><a href='#'>Идеи вашего дома</a></li>
<li><a href='#'>Интерьер + дизайн</a></li>
<li><a href='#'>Мезонин</a></li>
<li><a href='#'>Мой уютный дом</a></li>
<li class="clear">&nbsp;</li>
</ul>
<h5>Информационное</h5>
<ul>
<li><a href='#'>Metro</a></li>
<li><a href='#'>Мой район</a></li>
<li class="clear">&nbsp;</li>
</ul>
<h5>Компьютеры, Hi-tech</h5>
<ul>
<li><a href='#'>Chip</a></li>
<li><a href='#'>Computer Bild</a></li>
<li><a href='#'>МедиаМаг</a></li>
<li class="clear">&nbsp;</li>
</ul>
<h5>Кулинария</h5>
<ul>
<li><a href='#'>Афиша-Еда</a></li>
<li><a href='#'>Вкусно и полезно (приложение к журналу Домашний очаг)</a></li>
<li><a href='#'>Все для женщины.На нашей кухне</a></li>
<li><a href='#'>Гастрономъ</a></li>
<li><a href='#'>Добрые советы.Люблю готовить!</a></li>
<li><a href='#'>Золотые рецепты наших читателей</a></li>
<li><a href='#'>Кулинарные советы от Нашей кухни</a></li>
<li><a href='#'>Кулинарный практикум</a></li>
<li><a href='#'>Кухонька Михалыча</a></li>
<li><a href='#'>Наша кухня</a></li>
<li><a href='#'>Приготовь</a></li>
<li><a href='#'>Приятного аппетита</a></li>
<li><a href='#'>Просто. Вкусно</a></li>
<li><a href='#'>Самая.Кулинарный практикум</a></li>
<li><a href='#'>Хлеб Соль</a></li>
<li><a href='#'>Школа гастронома</a></li>
<li><a href='#'>Школа Гастронома. Коллекция рецептов</a></li>
<li class="clear">&nbsp;</li>
</ul>
<h5>Мода, красота, стиль</h5>
<ul>
<li><a href='#'>Top Beauty</a></li>
<li><a href='#'>Стильные прически</a></li>
<li class="clear">&nbsp;</li>
</ul>
<h5>Мужское</h5>
<ul>
<li><a href='#'>Esquire</a></li>
<li><a href='#'>FHM</a></li>
<li><a href='#'>GQ</a></li>
<li><a href='#'>Maxim</a></li>
<li><a href='#'>Men's Health</a></li>
<li><a href='#'>Playboy</a></li>
<li class="clear">&nbsp;</li>
</ul>
<h5>Наука, техника</h5>
<ul>
<li><a href='#'>Discovery</a></li>
<li><a href='#'>Популярная механика</a></li>
<li class="clear">&nbsp;</li>
</ul>
<h5>Недвижимость</h5>
<ul>
<li><a href='#'>Недвижимость & цены</a></li>
<li class="clear">&nbsp;</li>
</ul>
<h5>Общественно-политическое</h5>
<ul>
<li><a href='#'>The New Times</a></li>
<li><a href='#'>Аргументы и факты</a></li>
<li><a href='#'>Вечерняя Москва</a></li>
<li><a href='#'>Известия</a></li>
<li><a href='#'>Комсомольская правда (w)</a></li>
<li><a href='#'>МК-Регион</a></li>
<li><a href='#'>Московский Комсомолец</a></li>
<li><a href='#'>Огонек</a></li>
<li><a href='#'>Российская газета</a></li>
<li><a href='#'>Русский репортер</a></li>
<li><a href='#'>Труд</a></li>
<li class="clear">&nbsp;</li>
</ul>
<h5>Психология</h5>
<ul>
<li><a href='#'>Cosmopolitan Психология</a></li>
<li><a href='#'>Psychologies</a></li>
<li><a href='#'>Психология и я</a></li>
<li class="clear">&nbsp;</li>
</ul>
<h5>Психология</h5>
<ul>
<li><a href='#'>Geo</a></li>
<li><a href='#'>National Geographic </a></li>
<li><a href='#'>National Geographic Traveler</a></li>
<li><a href='#'>Афиша-Мир</a></li>
<li><a href='#'>Вокруг света</a></li>
<li class="clear">&nbsp;</li>
</ul>
<h5>Путешествия</h5>
<ul>
<li><a href='#'>Работа & зарплата</a></li>
<li class="clear">&nbsp;</li>
</ul>
<h5>Работа, образование</h5>
<ul>
<li><a href='#'>777</a></li>
<li><a href='#'>Дом-2</a></li>
<li><a href='#'>Звезды и советы</a></li>
<li><a href='#'>Зятек</a></li>
<li><a href='#'>Лиза.Кроссворды</a></li>
<li><a href='#'>Лиза.Реши для души</a></li>
<li><a href='#'>Моя семья</a></li>
<li><a href='#'>Отдохни!</a></li>
<li><a href='#'>Разгадай!</a></li>
<li><a href='#'>Тещин язык</a></li>
<li><a href='#'>Экспресс газета</a></li>
<li class="clear">&nbsp;</li>
</ul>
<h5>Развлекательное</h5>
<ul>
<li><a href='#'>Rolling Stone</a></li>
<li><a href='#'>Афиша</a></li>
<li><a href='#'>Ваш досуг</a></li>
<li class="clear">&nbsp;</li>
</ul>
<h5>Родители и дети</h5>
<ul>
<li><a href='#'>9 месяцев</a></li>
<li><a href='#'>Жду малыша</a></li>
<li><a href='#'>Здоровый малыш</a></li>
<li><a href='#'>Лиза.Мой ребенок</a></li>
<li><a href='#'>Мама и малыш</a></li>
<li><a href='#'>Мама, это я!</a></li>
<li><a href='#'>Мой кроха и я</a></li>
<li><a href='#'>Счастливая мама</a></li>
<li><a href='#'>Счастливые родители</a></li>
<li class="clear">&nbsp;</li>
</ul>
<h5>Рукоделие. Сделай сам</h5>
<ul>
<li><a href='#'>Делаем сами</a></li>
</ul>
<h5>Сад. Ландшафтный дизайн</h5>
<ul>
<li><a href='#'>АиФ "На даче"</a></li>
<li><a href='#'>Добрые советы. Дом в саду</a></li>
<li><a href='#'>Домашние цветы</a></li>
<li><a href='#'>Любимая дача</a></li>
<li><a href='#'>Мой прекрасный сад</a></li>
<li><a href='#'>Моя прекрасная дача</a></li>
<li><a href='#'>Садовник</a></li>
<li><a href='#'>Садовод и огородник</a></li>
<li><a href='#'>Цветок</a></li>
<li class="clear">&nbsp;</li>
</ul>
<h5>Светская жизнь</h5>
<ul>
<li><a href='#'>GALA Биография</a></li>
<li><a href='#'>HELLO!</a></li>
<li><a href='#'>OK!</a></li>
<li><a href='#'>StarHit</a></li>
<li><a href='#'>Story</a></li>
<li><a href='#'>Тайны звезд</a></li>
<li class="clear">&nbsp;</li>
</ul>
<h5>Спорт</h5>
<ul>
<li><a href='#'>EUROFOOTBALL</a></li>
<li><a href='#'>PROспорт</a></li>
<li><a href='#'>Большой спорт</a></li>
<li><a href='#'>Советский спорт</a></li>
<li><a href='#'>Советский спорт Футбол</a></li>
<li><a href='#'>Спорт-Экспресс</a></li>
<li><a href='#'>Футбол</a></li>
<li class="clear">&nbsp;</li>
</ul>
<h5>Строительство и ремонт</h5>
<ul>
<li><a href='#'>Мой любимый дом</a></li>
<li><a href='#'>Новый дом</a></li>
<li><a href='#'>Обустройство & ремонт</a></li>
<li class="clear">&nbsp;</li>
</ul>
<h5>Телегид</h5>
<ul>
<li><a href='#'>7 Дней</a></li>
<li><a href='#'>Антенна/Телесемь</a></li>
<li><a href='#'>МК-Бульвар</a></li>
<li><a href='#'>Теленеделя</a></li>
<li><a href='#'>Телепрограмма</a></li>
<li class="clear">&nbsp;</li>
</ul>
<h5>Финансы, экономика</h5>
<ul>
<li><a href='#'>Главбух</a></li>
<li><a href='#'>Экономика и жизнь</a></li>
<li class="clear">&nbsp;</li>
</ul>
<h5>Хобби. Рыболовство</h5>
<ul>
<li><a href='#'>Охота</a></li>
<li><a href='#'>Охота и рыбалка XXI век</a></li>
<li><a href='#'>Рыбалка на Руси</a></li>
<li><a href='#'>Рыбачьте с нами</a></li>
</ul>

  	<?php /*/ if(count($items)): ?>
  <ul>
    <?php foreach ($items as $key=>$item):	?>
    <li class="<?php echo ($key%2) ? "odd" : "even"; if(count($items)==$key+1) echo ' lastItem'; ?>">

      <!-- Plugins: BeforeDisplay -->
      <?php echo $item->event->BeforeDisplay; ?>

      <!-- K2 Plugins: K2BeforeDisplay -->
      <?php echo $item->event->K2BeforeDisplay; ?>

      <?php if($params->get('itemAuthorAvatar')): ?>
      <a class="k2Avatar moduleItemAuthorAvatar" rel="author" href="<?php echo $item->authorLink; ?>">
				<img src="<?php echo $item->authorAvatar; ?>" alt="<?php echo K2HelperUtilities::cleanHtml($item->author); ?>" style="width:<?php echo $avatarWidth; ?>px;height:auto;" />
			</a>
      <?php endif; ?>

      <?php if($params->get('itemTitle')): ?>
      <a class="moduleItemTitle" href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a>
      <?php endif; ?>

      <?php if($params->get('itemAuthor')): ?>
      <div class="moduleItemAuthor">
	      <?php echo K2HelperUtilities::writtenBy($item->authorGender); ?>
	
				<?php if(isset($item->authorLink)): ?>
				<a rel="author" title="<?php echo K2HelperUtilities::cleanHtml($item->author); ?>" href="<?php echo $item->authorLink; ?>"><?php echo $item->author; ?></a>
				<?php else: ?>
				<?php echo $item->author; ?>
				<?php endif; ?>
				
				<?php if($params->get('userDescription')): ?>
				<?php echo $item->authorDescription; ?>
				<?php endif; ?>
				
			</div>
			<?php endif; ?>

      <!-- Plugins: AfterDisplayTitle -->
      <?php echo $item->event->AfterDisplayTitle; ?>

      <!-- K2 Plugins: K2AfterDisplayTitle -->
      <?php echo $item->event->K2AfterDisplayTitle; ?>

      <!-- Plugins: BeforeDisplayContent -->
      <?php echo $item->event->BeforeDisplayContent; ?>

      <!-- K2 Plugins: K2BeforeDisplayContent -->
      <?php echo $item->event->K2BeforeDisplayContent; ?>

      <?php if($params->get('itemImage') || $params->get('itemIntroText')): ?>
      <div class="moduleItemIntrotext">
	      <?php if($params->get('itemImage') && isset($item->image)): ?>
	      <a class="moduleItemImage" href="<?php echo $item->link; ?>" title="<?php echo JText::_('K2_CONTINUE_READING'); ?> &quot;<?php echo K2HelperUtilities::cleanHtml($item->title); ?>&quot;">
	      	<img src="<?php echo $item->image; ?>" alt="<?php echo K2HelperUtilities::cleanHtml($item->title); ?>"/>
	      </a>
	      <?php endif; ?>

      	<?php if($params->get('itemIntroText')): ?>
      	<?php echo $item->introtext; ?>
      	<?php endif; ?>
      </div>
      <?php endif; ?> 

      <?php if($params->get('itemExtraFields') && count($item->extra_fields)): ?>
      <div class="moduleItemExtraFields">
	      <b><?php echo JText::_('K2_ADDITIONAL_INFO'); ?></b>
	      <ul>
	        <?php foreach ($item->extra_fields as $extraField): ?>
					<?php if($extraField->value != ''): ?>
					<li class="type<?php echo ucfirst($extraField->type); ?> group<?php echo $extraField->group; ?>">
						<?php if($extraField->type == 'header'): ?>
						<h4 class="moduleItemExtraFieldsHeader"><?php echo $extraField->name; ?></h4>
						<?php else: ?>
						<span class="moduleItemExtraFieldsLabel"><?php echo $extraField->name; ?></span>
						<span class="moduleItemExtraFieldsValue"><?php echo $extraField->value; ?></span>
						<?php endif; ?>
						<div class="clr"></div>
					</li>
					<?php endif; ?>
	        <?php endforeach; ?>
	      </ul>
      </div>
      <?php endif; ?>

      <div class="clr"></div>

      <?php if($params->get('itemVideo')): ?>
      <div class="moduleItemVideo">
      	<?php echo $item->video ; ?>
      	<span class="moduleItemVideoCaption"><?php echo $item->video_caption ; ?></span>
      	<span class="moduleItemVideoCredits"><?php echo $item->video_credits ; ?></span>
      </div>
      <?php endif; ?>

      <div class="clr"></div>

      <!-- Plugins: AfterDisplayContent -->
      <?php echo $item->event->AfterDisplayContent; ?>

      <!-- K2 Plugins: K2AfterDisplayContent -->
      <?php echo $item->event->K2AfterDisplayContent; ?>

      <?php if($params->get('itemDateCreated')): ?>
      <span class="moduleItemDateCreated"><?php echo JText::_('K2_WRITTEN_ON') ; ?> <?php echo JHTML::_('date', $item->created, JText::_('K2_DATE_FORMAT_LC2')); ?></span>
      <?php endif; ?>

      <?php if($params->get('itemCategory')): ?>
      <?php echo JText::_('K2_IN') ; ?> <a class="moduleItemCategory" href="<?php echo $item->categoryLink; ?>"><?php echo $item->categoryname; ?></a>
      <?php endif; ?>

      <?php if($params->get('itemTags') && count($item->tags)>0): ?>
      <div class="moduleItemTags">
      	<b><?php echo JText::_('K2_TAGS'); ?>:</b>
        <?php foreach ($item->tags as $tag): ?>
        <a href="<?php echo $tag->link; ?>"><?php echo $tag->name; ?></a>
        <?php endforeach; ?>
      </div>
      <?php endif; ?>

      <?php if($params->get('itemAttachments') && count($item->attachments)): ?>
			<div class="moduleAttachments">
				<?php foreach ($item->attachments as $attachment): ?>
				<a title="<?php echo K2HelperUtilities::cleanHtml($attachment->titleAttribute); ?>" href="<?php echo $attachment->link; ?>"><?php echo $attachment->title; ?></a>
				<?php endforeach; ?>
			</div>
      <?php endif; ?>

			<?php if($params->get('itemCommentsCounter') && $componentParams->get('comments')): ?>		
				<?php if(!empty($item->event->K2CommentsCounter)): ?>
					<!-- K2 Plugins: K2CommentsCounter -->
					<?php echo $item->event->K2CommentsCounter; ?>
				<?php else: ?>
					<?php if($item->numOfComments>0): ?>
					<a class="moduleItemComments" href="<?php echo $item->link.'#itemCommentsAnchor'; ?>">
						<?php echo $item->numOfComments; ?> <?php if($item->numOfComments>1) echo JText::_('K2_COMMENTS'); else echo JText::_('K2_COMMENT'); ?>
					</a>
					<?php else: ?>
					<a class="moduleItemComments" href="<?php echo $item->link.'#itemCommentsAnchor'; ?>">
						<?php echo JText::_('K2_BE_THE_FIRST_TO_COMMENT'); ?>
					</a>
					<?php endif; ?>
				<?php endif; ?>
			<?php endif; ?>

			<?php if($params->get('itemHits')): ?>
			<span class="moduleItemHits">
				<?php echo JText::_('K2_READ'); ?> <?php echo $item->hits; ?> <?php echo JText::_('K2_TIMES'); ?>
			</span>
			<?php endif; ?>

			<?php if($params->get('itemReadMore') && $item->fulltext): ?>
			<a class="moduleItemReadMore" href="<?php echo $item->link; ?>">
				<?php echo JText::_('K2_READ_MORE'); ?>
			</a>
			<?php endif; ?>

      <!-- Plugins: AfterDisplay -->
      <?php echo $item->event->AfterDisplay; ?>

      <!-- K2 Plugins: K2AfterDisplay -->
      <?php echo $item->event->K2AfterDisplay; ?>

      <div class="clr"></div>
    </li>
    <?php endforeach; ?>
    <li class="clearList"></li>
  </ul>
  <?php endif; /*/ ?>
  </div>

	<?php if($params->get('itemCustomLink')): ?>
	<a class="moduleCustomLink" href="<?php echo $params->get('itemCustomLinkURL'); ?>" title="<?php echo K2HelperUtilities::cleanHtml($itemCustomLinkTitle); ?>"><?php echo $itemCustomLinkTitle; ?></a>
	<?php endif; ?>

	<?php if($params->get('feed')): ?>
	<div class="k2FeedIcon">
		<a href="<?php echo JRoute::_('index.php?option=com_k2&view=itemlist&format=feed&moduleID='.$module->id); ?>" title="<?php echo JText::_('K2_SUBSCRIBE_TO_THIS_RSS_FEED'); ?>">
			<span><?php echo JText::_('K2_SUBSCRIBE_TO_THIS_RSS_FEED'); ?></span>
		</a>
		<div class="clr"></div>
	</div>
	<?php endif; ?>

</div>
