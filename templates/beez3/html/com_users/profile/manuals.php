<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<?php if ($this->hasError) : ?>
    <span>Извините, сервер временно недоступен.</span>
<?php else : ?>
    <div id="report-request-error" style="display: none;"></div>
    <input type="hidden" name="format" value="raw">
    <input type="hidden" name="view" value="profile">
    <input type="hidden" name="layout" value="reports">
    <?php if (!empty($this->manualsArr['adv_camp'])) : ?>
        <div class="form-field">
            <div class="form-label">
                <label id="form-camp-lbl">Рекламные кампании</label>
            </div>
            <ul>
                <li>
                    <input type="checkbox" data-role="none" class="form-checkbox form-fieldset" id="form-adv_camp">
                    <label for="form-adv_camp"><span></span>Все компании</label>
                </li>
                <?php foreach($this->manualsArr['adv_camp'] as $adv_camp): ?>
                    <li>
                        <input type="checkbox" data-role="none" class="form-checkbox form-fieldset form-adv_camp" name="adv_camp[]" value="<?=$adv_camp['id']?>" id="form-camp_<?=$adv_camp['id']?>">
                        <label for="form-camp_<?=$adv_camp['id']?>"><span></span><?=$adv_camp['name']?></label>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>
    <?php if (!empty($this->manualsArr['city'])) : ?>
        <div class="form-field">
            <div class="form-label">
                <label id="form-camp-lbl">Города</label>
            </div>
            <ul>
                <li>
                    <input type="checkbox" data-role="none" class="form-checkbox form-fieldset" id="form-city">
                    <label for="form-city"><span></span>Все города</label>
                </li>
                <?php foreach($this->manualsArr['city'] as $city): ?>
                    <li>
                        <input type="checkbox" data-role="none" class="form-checkbox form-fieldset form-city" value="<?=$city['id']?>" name="city[]" id="form-city_<?=$city['id']?>">
                        <label for="form-city_<?=$city['id']?>"><span></span><?=$city['name']?></label>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>
    <?php if (!empty($this->manualsArr['media'])) : ?>
        <div class="form-field">
            <div class="form-label">
                <label id="form-camp-lbl">Медиа</label>
            </div>
            <ul>
                <li>
                    <input type="checkbox" data-role="none" class="form-checkbox form-fieldset" value="Все медиа"  id="form-media">
                    <label for="form-media"><span></span>Все медиа</label>
                </li>
                <?php foreach($this->manualsArr['media'] as $media): ?>
                    <li>
                        <input type="checkbox" data-role="none" class="form-checkbox form-fieldset form-media" name="media[]" value="<?=$media['id']?>" id="form-media_<?=$media['id']?>">
                        <label for="form-media_<?=$media['id']?>"><span></span><?=$media['name']?></label>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>
    <?php if (!empty($this->manualsArr['emails'])) : ?>
        <div class="form-field email">
            <div class="form-label">
                <label id="form-camp-lbl">Адреса для отправки</label>
            </div>
            <ul>
                <?php if (!is_array($this->manualsArr['emails'])) : ?>
                    <li>
                        <input type="checkbox" data-role="none" class="form-checkbox form-fieldset form-emails" value="<?=$this->manualsArr['emails']?>" name="email[]" id="form-email_0">
                        <label for="form-email_0"><span></span><?=$this->manualsArr['emails']?></label>
                    </li>
                <?php else : ?>
                    <?php foreach($this->manualsArr['emails'] as $key => $emails): ?>
                        <li>
                            <input type="checkbox" data-role="none" class="form-checkbox form-fieldset form-emails" value="<?=$emails?>" name="email[]" id="form-email_<?=$key?>">
                            <label for="form-email_<?=$key?>"><span></span><?=$emails?></label>
                        </li>
                    <?php endforeach; ?>
                <?php endif; ?>
            </ul>
            <!--<button class="btn" type="button">Добавить Еще</button>
            <a href="#">Удалить все</a>-->
        </div>
    <?php endif; ?>
    <button class="btn generate-report" type="button">Построить отчет</button>
<?php endif; ?>