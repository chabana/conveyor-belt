<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<?php if ($this->hasError) : ?>
    <span>Извините, сервер временно недоступен.</span>
<?php else : ?>
    <h3>Последние отчеты</h3>
    <ul class="last-reports">
        <?php foreach($this->reportHistoryArr as $key => $reportHistory): ?>
            <li>
                <div class="date"><?php echo $reportHistory->date_view; ?></div>
                <div class="camps"><a href="#"><span><?php echo $reportHistory->adv_camp; ?></span></a></div>
                <div class="period"><?php echo $reportHistory->period_view; ?></div>
                <div class="status requested <?php echo $reportHistory->status_class?>"><?=$reportHistory->status_view; ?></div>
                <?php if ($reportHistory->status_class != 'done') : ?>
                    <button class="btn get-status" type="button" data-id="<?php echo $reportHistory->report_id; ?>">Обновить</button>
                <?php endif; ?>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>