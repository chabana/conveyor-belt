<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
JHTML::_('behavior.calendar');
?>
<div class="profile <?php echo $this->pageclass_sfx ?>">
    <div class="wrapper">
        <h1>Личный кабинет рекламодателя</h1>

        <div class="inwrap">
            <h2>ЗАО «Московский индустриальный банк»</h2>
            <div class="faces-title">
              <h3>Контактные лица</h3>
              <a href="#" class="persons-list-toggle">Скрыть список</a>
            </div>

            <form method="post" action="<?php echo JRoute::_('index.php?option=com_users&task=profile.persons&user_id=' . (int)$this->data->id); ?>">
                <input type="hidden" name="format" value="json">
                <div class="contact-names">
                    <ul class="contact-persons">
                        <?php if (empty($this->contactsArr)) : ?>
                            <li>
                                <div class="field-name">
                                    <label for="field-name">Имя</label>
                                    <input type="text" class="field-contact-name" name="name[]">
                                </div>
                                <div class="field-position">
                                    <label for="field-position">Должность</label>
                                    <input type="text" class="field-contact-position" name="position[]">
                                </div>
                                <div class="field-email">
                                    <label for="field-email">Электронная почта</label>
                                    <input type="text" class="field-contact-email" name="email[]">
                                </div>
                                <div class="field-phone">
                                    <label for="field-phone">Телефон</label>
                                    +7 <input type="text" class="field-contact-phone" name="phone[]">
                                </div>
                                <div class="field-button">
                                    <button class="btn delete-person" type="button">Удалить</button>
                                </div>
                            </li>
                        <?php else : ?>
                            <?php foreach($this->contactsArr as $contacts): ?>
                                <li>
                                    <div class="field-name">
                                        <label for="field-name">Имя</label>
                                        <input type="text" class="field-contact-name" name="name[]" value="<?php echo $contacts->name; ?>">
                                    </div>
                                    <div class="field-position">
                                        <label for="field-position">Должность</label>
                                        <input type="text" class="field-contact-position" name="position[]" value="<?=$contacts->position?>">
                                    </div>
                                    <div class="field-email">
                                        <label for="field-email">Электронная почта</label>
                                        <input type="text" class="field-contact-email" name="email[]" value="<?=$contacts->email?>">
                                    </div>
                                    <div class="field-phone">
                                        <label for="field-phone">Телефон</label>
                                        +7 <input type="text" class="field-contact-phone" name="phone[]" value="<?=$contacts->phone?>">
                                    </div>
                                    <div class="field-button">
                                        <button class="btn delete-person" type="button">Удалить</button>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                    <div class="clear"></div>
                    <button class="btn add-more" type="button">Добавить Еще</button>
                    <button class="btn persons-save" type="button">Сохранить</button>
                </div>
            </form>

            <div class="report-request">
                <h3>Запрос отчета за период</h3>
                <form method="post" action="<?php echo JRoute::_('index.php?option=com_users&task=profile.manuals'); ?>">
                    <input type="hidden" name="format" value="raw">
                    <input type="hidden" name="view" value="profile">
                    <input type="hidden" name="layout" value="manuals">
                    <div class="period">
                        <?=JHtml::_('calendar', '', 'period-start-date', 'period-start-date', '%m/%Y');?>
                        -
                        <?=JHtml::_('calendar', '', 'period-end-date', 'period-end-date', '%m/%Y');?>
                        <button class="btn get-manuals" type="button">Показать</button>
                    </div>
                </form>
                <form method="post" action="<?php echo JRoute::_('index.php?option=com_users&task=profile.reports'); ?>" id="profile-manuals-forms">

                </form>
            </div>


            <form method="post" action="<?php echo JRoute::_('index.php?option=com_users&task=profile.checkReport'); ?>" id="profile-check-report">
                <input type="hidden" name="format" value="json">
                <div class="report-history">
                    <?php if (!empty($this->reportHistoryArr)) : ?>
                        <h3>Последние отчеты</h3>
                        <ul class="last-reports">
                            <?php foreach($this->reportHistoryArr as $key => $reportHistory): ?>
                                <li>
                                    <div class="date"><?=$reportHistory->date_view?></div>
                                    <div class="camps"><a href="#"><?=$reportHistory->adv_camp?></a></div>
                                    <div class="period"><?=$reportHistory->period_view?></div>
                                    <div class="status requested"><?=$reportHistory->status_view?></div>
                                    <button class="btn get-status" type="button" data-id="<?=$reportHistory->report_id?>">Обновить</button>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>
            </form>

            <?php if ($this->params->get('show_page_heading')) : ?>
                <div class="page-header">
                    <h1>
                        <?php echo $this->escape($this->params->get('page_heading')); ?>
                    </h1>
                </div>
            <?php endif; ?>

            <?php if (JFactory::getUser()->id == $this->data->id) : ?>
                <!--<ul class="btn-toolbar pull-right">
                    <li class="btn-group">
                        <a class="btn"
                           href="<?php echo JRoute::_('index.php?option=com_users&task=profile.edit&user_id=' . (int)$this->data->id); ?>">
                            <span class="icon-user"></span> <?php echo JText::_('COM_USERS_EDIT_PROFILE'); ?></a>
                    </li>
                </ul>-->
            <?php endif; ?>





            <?php echo $this->loadTemplate('custom'); ?>
        </div>
    </div>
</div>
