<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<h3>Последние отчеты</h3>
<ul class="last-reports">
    <?php foreach($this->reportHistoryArr as $key => $reportHistory): ?>
        <li>
            <div class="date"><?=$reportHistory->date_view?></div>
            <div class="camps"><a href="#"><?=$reportHistory->adv_camp?></a></div>
            <div class="period"><?=$reportHistory->period_view?></div>
            <div class="status requested"><?=$reportHistory->status_view?></div>
            <button class="btn get-status" type="button" data-id="<?=$reportHistory->report_id?>">Обновить</button>
        </li>
    <?php endforeach; ?>
</ul>