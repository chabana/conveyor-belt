<?php
/**
 * @version		$Id: default.php 1812 2013-01-14 18:45:06Z lefteris.kavadas $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2013 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;
JHTML::_('behavior.modal');
?>

<div id="k2ModuleBox<?php echo $module->id; ?>" class="k2ItemsBlock<?php if($params->get('moduleclass_sfx')) echo ' '.$params->get('moduleclass_sfx'); ?>">
  <p>Посмотреть профиль аудитории других телеканалов: <a class="modal" href="#openModal" rel="{size:{x:1000,y:600}}"><span>Список телеканалов</span></a></p>

  <div id="openModal" class="modalDialog">
    <?php if($params->get('itemPreText')): ?>
     <?php echo $params->get('itemPreText'); ?>
  	<?php endif; ?>
    <?php if(count($items)): ?> 
      <ul>
        <?php foreach ($items as $key=>$item):	?>
          <li><?php if($params->get('itemTitle')): ?><a href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a><?php endif; ?></li>
        <?php endforeach; ?>
        <li class="clearList"></li>
      </ul>
    <?php endif; ?>
  </div>
</div>
