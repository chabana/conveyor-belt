<?php
/**
 * @version		$Id: default.php 1812 2013-01-14 18:45:06Z lefteris.kavadas $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2013 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;
JHTML::_('behavior.modal');
?>

<div id="k2ModuleBox<?php echo $module->id; ?>" class="k2ItemsBlock<?php if($params->get('moduleclass_sfx')) echo ' '.$params->get('moduleclass_sfx'); ?>">
  <p>Посмотреть профиль аудитории других Интернет площадок: <a class="modal" href="#openModal" rel="{size:{x:1000,y:600}}"><span>Список площадок</span></a></p>

  <div id="openModal" class="modalDialog">
    <?php if($params->get('itemPreText')): ?>
     <h4><?php echo $params->get('itemPreText'); ?></h4>
     <p>Мы публикуем на нашем сайте минимальное кол-во интернет площадок. Если вы не нашли, какую-то площадку, можете не сомневаться, что мы размещаем рекламу на ней, т.к. наша база данных охватывает все регионы РФ и насчитывает более 5000 сайтов.</p>
  	<?php endif; ?>
    <ul>
      <li><a href='#'>101.ru</a></li>
      <li><a href='#'>110km.ru</a></li>
      <li><a href='#'>1prime.ru</a></li>
      <li><a href='#'>1tv.ru</a></li>
      <li><a href='#'>2x2tv.ru</a></li>
      <li><a href='#'>5koleso.ru</a></li>
      <li><a href='#'>7days.ru</a></li>
      <li><a href='#'>7ya.ru</a></li>
      <li><a href='#'>Afisha.ru</a></li>
      <li><a href='#'>Ag.ru</a></li>
      <li><a href='#'>Aif.ru</a></li>
      <li><a href='#'>All.biz</a></li>
      <li><a href='#'>Am.ru</a></li>
      <li><a href='#'>Anekdot.ru</a></li>
      <li><a href='#'>Auto.ru</a></li>
      <li><a href='#'>Autonews.ru</a></li>
      <li><a href='#'>Autorambler.ru</a></li>
      <li><a href='#'>Avito.ru</a></li>
      <li><a href='#'>Baby.ru</a></li>
      <li><a href='#'>Babyblog.ru</a></li>
      <li><a href='#'>Banki.ru</a></li>
      <li><a href='#'>Bfm.ru</a></li>
      <li><a href='#'>Bg.ru</a></li>
      <li><a href='#'>Bibika.ru</a></li>
      <li><a href='#'>Carobka.ru</a></li>
      <li><a href='#'>Carsguru.ru</a></li>
      <li><a href='#'>Championat.com</a></li>
      <li><a href='#'>Cn.ru</a></li>
      <li><a href='#'>CNews.ru</a></li>
      <li><a href='#'>Cosmo.ru</a></li>
      <li><a href='#'>Ctc.ru</a></li>
      <li><a href='#'>Disney.ru</a></li>
      <li><a href='#'>Dnevnik.ru</a></li>
      <li><a href='#'>Dom2.ru</a></li>
      <li><a href='#'>Domashniy.ru</a></li>
      <li><a href='#'>Dp.ru</a></li>
      <li><a href='#'>Drive.ru</a></li>
      <li><a href='#'>E1.ru</a></li>
      <li><a href='#'>Echomsk.ru</a></li>
      <li><a href='#'>Eda.ru</a></li>
      <li><a href='#'>Eg.ru</a></li>
      <li><a href='#'>Ekabu.ru</a></li>
      <li><a href='#'>Elle.ru</a></li>
      <li><a href='#'>Esquire.ru</a></li>
      <li><a href='#'>Eva.ru</a></li>
      <li><a href='#'>F1news.ru</a></li>
      <li><a href='#'>Ferra.ru</a></li>
      <li><a href='#'>Filmpro.ru</a></li>
      <li><a href='#'>Finanz.ru</a></li>
      <li><a href='#'>Finmarket.ru</a></li>
      <li><a href='#'>Forbes.ru</a></li>
      <li><a href='#'>Friday.ru</a></li>
      <li><a href='#'>Furfur.me</a></li>
      <li><a href='#'>Gameguru.ru</a></li>
      <li><a href='#'>Gastronom.ru</a></li>
      <li><a href='#'>Gazeta.ru</a></li>
      <li><a href='#'>Geo.ru</a></li>
      <li><a href='#'>Geometria.ru</a></li>
      <li><a href='#'>Gismeteo.ru</a></li>
      <li><a href='#'>Glamour.ru</a></li>
      <li><a href='#'>Gmbox.ru</a></li>
      <li><a href='#'>Goodhouse.ru</a></li>
      <li><a href='#'>Google.ru</a></li>
      <li><a href='#'>Gq.ru</a></li>
      <li><a href='#'>Heat.ru</a></li>
      <li><a href='#'>Imhonet.ru</a></li>
      <li><a href='#'>Inopressa.ru</a></li>
      <li><a href='#'>Inosmi.ru</a></li>
      <li><a href='#'>Interfax.ru</a></li>
      <li><a href='#'>Interfax-russia.ru</a></li>
      <li><a href='#'>Iphones.ru</a></li>
      <li><a href='#'>Irr.ru</a></li>
      <li><a href='#'>Itar-tass.com</a></li>
      <li><a href='#'>Ivi.ru</a></li>
      <li><a href='#'>Izvestia.ru</a></li>
      <li><a href='#'>Job.ru</a></li>
      <li><a href='#'>Justlady.ru</a></li>
      <li><a href='#'>Jv.ru</a></li>
      <li><a href='#'>Kakprosto.ru</a></li>
      <li><a href='#'>Kanobu.ru</a></li>
      <li><a href='#'>Karaoke.ru</a></li>
      <li><a href='#'>Kinoafisha.info</a></li>
      <li><a href='#'>Kinopoisk.ru</a></li>
      <li><a href='#'>Kleo.ru</a></li>
      <li><a href='#'>Kommersant.ru</a></li>
      <li><a href='#'>Kp.ru</a></li>
      <li><a href='#'>Lenta.ru</a></li>
      <li><a href='#'>Letidor.ru</a></li>
      <li><a href='#'>Lifehacker.ru</a></li>
      <li><a href='#'>Lifenews.ru</a></li>
      <li><a href='#'>LiveJournal.com</a></li>
      <li><a href='#'>Lookatme.ru</a></li>
      <li><a href='#'>Loveplanet.ru</a></li>
      <li><a href='#'>M24.ru</a></li>
      <li><a href='#'>Mail.ru</a></li>
      <li><a href='#'>Mamba.ru</a></li>
      <li><a href='#'>Materinstvo.ru</a></li>
      <li><a href='#'>Maximonline.ru</a></li>
      <li><a href='#'>Megalyrics.ru</a></li>
      <li><a href='#'>Megogo.net</a></li>
      <li><a href='#'>Metronews.ru</a></li>
      <li><a href='#'>Mhealth.ru</a></li>
      <li><a href='#'>Mk.ru</a></li>
      <li><a href='#'>Mobiguru.ru</a></li>
      <li><a href='#'>Motor.ru</a></li>
      <li><a href='#'>Muz-tv.ru</a></li>
      <li><a href='#'>Nat-geo.ru</a></li>
      <li><a href='#'>Neva.today</a></li>
      <li><a href='#'>Newsru.com</a></li>
      <li><a href='#'>Newstube.ru</a></li>
      <li><a href='#'>Ngs.ru</a></li>
      <li><a href='#'>Nn.ru</a></li>
      <li><a href='#'>Now.ru</a></li>
      <li><a href='#'>Odnoklassniki.ru</a></li>
      <li><a href='#'>Onlineguru.ru</a></li>
      <li><a href='#'>Osp.ru</a></li>
      <li><a href='#'>Passion.ru</a></li>
      <li><a href='#'>Pinme.ru</a></li>
      <li><a href='#'>Planeta-online.tv</a></li>
      <li><a href='#'>Playground.ru</a></li>
      <li><a href='#'>Popmech.ru</a></li>
      <li><a href='#'>Price.ru</a></li>
      <li><a href='#'>Prm.ru</a></li>
      <li><a href='#'>Professionali.ru</a></li>
      <li><a href='#'>Psychologies.ru</a></li>
      <li><a href='#'>Qip.ru</a></li>
      <li><a href='#'>Quto.ru</a></li>
      <li><a href='#'>Rambler.ru</a></li>
      <li><a href='#'>RB.ru</a></li>
      <li><a href='#'>Rbc.ru</a></li>
      <li><a href='#'>Rbcdaily.ru</a></li>
      <li><a href='#'>Redigo.ru</a></li>
      <li><a href='#'>Regnum.ru</a></li>
      <li><a href='#'>Relax.ru</a></li>
      <li><a href='#'>Restate.ru</a></li>
      <li><a href='#'>Rg.ru</a></li>
      <li><a href='#'>Ria.ru</a></li>
      <li><a href='#'>Riarealty.ru</a></li>
      <li><a href='#'>Rollingstone.ru</a></li>
      <li><a href='#'>Rsport.ru</a></li>
      <li><a href='#'>Rusnovosti.ru</a></li>
      <li><a href='#'>Russia.tv</a></li>
      <li><a href='#'>Rutube.ru</a></li>
      <li><a href='#'>Sibnet.ru</a></li>
      <li><a href='#'>Slon.ru</a></li>
      <li><a href='#'>Smotri.com</a></li>
      <li><a href='#'>Sobaka.ru</a></li>
      <li><a href='#'>Solnet.ee</a></li>
      <li><a href='#'>Sovsport.ru</a></li>
      <li><a href='#'>Spletnik.ru</a></li>
      <li><a href='#'>Sportbox.ru</a></li>
      <li><a href='#'>Sport-express.ru</a></li>
      <li><a href='#'>Sports.ru</a></li>
      <li><a href='#'>Starhit.ru</a></li>
      <li><a href='#'>Strana.ru</a></li>
      <li><a href='#'>Supersadovnik.ru</a></li>
      <li><a href='#'>Svoy.ru</a></li>
      <li><a href='#'>Tatler.ru</a></li>
      <li><a href='#'>Theoryandpractice.ru</a></li>
      <li><a href='#'>The-village.ru</a></li>
      <li><a href='#'>Timeout.ru</a></li>
      <li><a href='#'>Tnt-online.ru</a></li>
      <li><a href='#'>Topface.com</a></li>
      <li><a href='#'>Topgearrussia.ru</a></li>
      <li><a href='#'>TourOut.ru</a></li>
      <li><a href='#'>Translate.ru</a></li>
      <li><a href='#'>Tv3.ru</a></li>
      <li><a href='#'>Tvc.ru</a></li>
      <li><a href='#'>Tvigle.ru</a></li>
      <li><a href='#'>Tvrain.ru</a></li>
      <li><a href='#'>U-tv.ru</a></li>
      <li><a href='#'>Vdolevke.ru</a></li>
      <li><a href='#'>Vedomosti.ru</a></li>
      <li><a href='#'>Vesti.ru</a></li>
      <li><a href='#'>Vestifinance.ru</a></li>
      <li><a href='#'>Videomore.ru</a></li>
      <li><a href='#'>Vk.com</a></li>
      <li><a href='#'>Vm.ru</a></li>
      <li><a href='#'>Vogue.ru</a></li>
      <li><a href='#'>Wday.ru</a></li>
      <li><a href='#'>Wmj.ru</a></li>
      <li><a href='#'>Woman.ru</a></li>
      <li><a href='#'>Yandex.ru</a></li>
      <li><a href='#'>Yopolis.ru</a></li>
      <li><a href='#'>Youtube.com</a></li>
      <li><a href='#'>Zoomby.ru</a></li>
      <li><a href='#'>Zr.ru</a></li>
      <li class="clearList"></li>
    </ul>
  	<?php /*/ if(count($items)): ?>
  <ul>
    <?php foreach ($items as $key=>$item):	?>
    <li class="<?php echo ($key%2) ? "odd" : "even"; if(count($items)==$key+1) echo ' lastItem'; ?>">

      <!-- Plugins: BeforeDisplay -->
      <?php echo $item->event->BeforeDisplay; ?>

      <!-- K2 Plugins: K2BeforeDisplay -->
      <?php echo $item->event->K2BeforeDisplay; ?>

      <?php if($params->get('itemAuthorAvatar')): ?>
      <a class="k2Avatar moduleItemAuthorAvatar" rel="author" href="<?php echo $item->authorLink; ?>">
				<img src="<?php echo $item->authorAvatar; ?>" alt="<?php echo K2HelperUtilities::cleanHtml($item->author); ?>" style="width:<?php echo $avatarWidth; ?>px;height:auto;" />
			</a>
      <?php endif; ?>

      <?php if($params->get('itemTitle')): ?>
      <a class="moduleItemTitle" href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a>
      <?php endif; ?>

      <?php if($params->get('itemAuthor')): ?>
      <div class="moduleItemAuthor">
	      <?php echo K2HelperUtilities::writtenBy($item->authorGender); ?>
	
				<?php if(isset($item->authorLink)): ?>
				<a rel="author" title="<?php echo K2HelperUtilities::cleanHtml($item->author); ?>" href="<?php echo $item->authorLink; ?>"><?php echo $item->author; ?></a>
				<?php else: ?>
				<?php echo $item->author; ?>
				<?php endif; ?>
				
				<?php if($params->get('userDescription')): ?>
				<?php echo $item->authorDescription; ?>
				<?php endif; ?>
				
			</div>
			<?php endif; ?>

      <!-- Plugins: AfterDisplayTitle -->
      <?php echo $item->event->AfterDisplayTitle; ?>

      <!-- K2 Plugins: K2AfterDisplayTitle -->
      <?php echo $item->event->K2AfterDisplayTitle; ?>

      <!-- Plugins: BeforeDisplayContent -->
      <?php echo $item->event->BeforeDisplayContent; ?>

      <!-- K2 Plugins: K2BeforeDisplayContent -->
      <?php echo $item->event->K2BeforeDisplayContent; ?>

      <?php if($params->get('itemImage') || $params->get('itemIntroText')): ?>
      <div class="moduleItemIntrotext">
	      <?php if($params->get('itemImage') && isset($item->image)): ?>
	      <a class="moduleItemImage" href="<?php echo $item->link; ?>" title="<?php echo JText::_('K2_CONTINUE_READING'); ?> &quot;<?php echo K2HelperUtilities::cleanHtml($item->title); ?>&quot;">
	      	<img src="<?php echo $item->image; ?>" alt="<?php echo K2HelperUtilities::cleanHtml($item->title); ?>"/>
	      </a>
	      <?php endif; ?>

      	<?php if($params->get('itemIntroText')): ?>
      	<?php echo $item->introtext; ?>
      	<?php endif; ?>
      </div>
      <?php endif; ?> 

      <?php if($params->get('itemExtraFields') && count($item->extra_fields)): ?>
      <div class="moduleItemExtraFields">
	      <b><?php echo JText::_('K2_ADDITIONAL_INFO'); ?></b>
	      <ul>
	        <?php foreach ($item->extra_fields as $extraField): ?>
					<?php if($extraField->value != ''): ?>
					<li class="type<?php echo ucfirst($extraField->type); ?> group<?php echo $extraField->group; ?>">
						<?php if($extraField->type == 'header'): ?>
						<h4 class="moduleItemExtraFieldsHeader"><?php echo $extraField->name; ?></h4>
						<?php else: ?>
						<span class="moduleItemExtraFieldsLabel"><?php echo $extraField->name; ?></span>
						<span class="moduleItemExtraFieldsValue"><?php echo $extraField->value; ?></span>
						<?php endif; ?>
						<div class="clr"></div>
					</li>
					<?php endif; ?>
	        <?php endforeach; ?>
	      </ul>
      </div>
      <?php endif; ?>

      <div class="clr"></div>

      <?php if($params->get('itemVideo')): ?>
      <div class="moduleItemVideo">
      	<?php echo $item->video ; ?>
      	<span class="moduleItemVideoCaption"><?php echo $item->video_caption ; ?></span>
      	<span class="moduleItemVideoCredits"><?php echo $item->video_credits ; ?></span>
      </div>
      <?php endif; ?>

      <div class="clr"></div>

      <!-- Plugins: AfterDisplayContent -->
      <?php echo $item->event->AfterDisplayContent; ?>

      <!-- K2 Plugins: K2AfterDisplayContent -->
      <?php echo $item->event->K2AfterDisplayContent; ?>

      <?php if($params->get('itemDateCreated')): ?>
      <span class="moduleItemDateCreated"><?php echo JText::_('K2_WRITTEN_ON') ; ?> <?php echo JHTML::_('date', $item->created, JText::_('K2_DATE_FORMAT_LC2')); ?></span>
      <?php endif; ?>

      <?php if($params->get('itemCategory')): ?>
      <?php echo JText::_('K2_IN') ; ?> <a class="moduleItemCategory" href="<?php echo $item->categoryLink; ?>"><?php echo $item->categoryname; ?></a>
      <?php endif; ?>

      <?php if($params->get('itemTags') && count($item->tags)>0): ?>
      <div class="moduleItemTags">
      	<b><?php echo JText::_('K2_TAGS'); ?>:</b>
        <?php foreach ($item->tags as $tag): ?>
        <a href="<?php echo $tag->link; ?>"><?php echo $tag->name; ?></a>
        <?php endforeach; ?>
      </div>
      <?php endif; ?>

      <?php if($params->get('itemAttachments') && count($item->attachments)): ?>
			<div class="moduleAttachments">
				<?php foreach ($item->attachments as $attachment): ?>
				<a title="<?php echo K2HelperUtilities::cleanHtml($attachment->titleAttribute); ?>" href="<?php echo $attachment->link; ?>"><?php echo $attachment->title; ?></a>
				<?php endforeach; ?>
			</div>
      <?php endif; ?>

			<?php if($params->get('itemCommentsCounter') && $componentParams->get('comments')): ?>		
				<?php if(!empty($item->event->K2CommentsCounter)): ?>
					<!-- K2 Plugins: K2CommentsCounter -->
					<?php echo $item->event->K2CommentsCounter; ?>
				<?php else: ?>
					<?php if($item->numOfComments>0): ?>
					<a class="moduleItemComments" href="<?php echo $item->link.'#itemCommentsAnchor'; ?>">
						<?php echo $item->numOfComments; ?> <?php if($item->numOfComments>1) echo JText::_('K2_COMMENTS'); else echo JText::_('K2_COMMENT'); ?>
					</a>
					<?php else: ?>
					<a class="moduleItemComments" href="<?php echo $item->link.'#itemCommentsAnchor'; ?>">
						<?php echo JText::_('K2_BE_THE_FIRST_TO_COMMENT'); ?>
					</a>
					<?php endif; ?>
				<?php endif; ?>
			<?php endif; ?>

			<?php if($params->get('itemHits')): ?>
			<span class="moduleItemHits">
				<?php echo JText::_('K2_READ'); ?> <?php echo $item->hits; ?> <?php echo JText::_('K2_TIMES'); ?>
			</span>
			<?php endif; ?>

			<?php if($params->get('itemReadMore') && $item->fulltext): ?>
			<a class="moduleItemReadMore" href="<?php echo $item->link; ?>">
				<?php echo JText::_('K2_READ_MORE'); ?>
			</a>
			<?php endif; ?>

      <!-- Plugins: AfterDisplay -->
      <?php echo $item->event->AfterDisplay; ?>

      <!-- K2 Plugins: K2AfterDisplay -->
      <?php echo $item->event->K2AfterDisplay; ?>

      <div class="clr"></div>
    </li>
    <?php endforeach; ?>
    <li class="clearList"></li>
  </ul>
  <?php endif; /*/ ?>
  </div>

	<?php if($params->get('itemCustomLink')): ?>
	<a class="moduleCustomLink" href="<?php echo $params->get('itemCustomLinkURL'); ?>" title="<?php echo K2HelperUtilities::cleanHtml($itemCustomLinkTitle); ?>"><?php echo $itemCustomLinkTitle; ?></a>
	<?php endif; ?>

	<?php if($params->get('feed')): ?>
	<div class="k2FeedIcon">
		<a href="<?php echo JRoute::_('index.php?option=com_k2&view=itemlist&format=feed&moduleID='.$module->id); ?>" title="<?php echo JText::_('K2_SUBSCRIBE_TO_THIS_RSS_FEED'); ?>">
			<span><?php echo JText::_('K2_SUBSCRIBE_TO_THIS_RSS_FEED'); ?></span>
		</a>
		<div class="clr"></div>
	</div>
	<?php endif; ?>

</div>
