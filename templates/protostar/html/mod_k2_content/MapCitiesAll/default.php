<?php
  defined('_JEXEC') or die;
?>

<?php if(count($items)): ?> 
  <ul>
    <?php foreach ($items as $key=>$item):	?>
      <li><?php if($params->get('itemTitle')): ?><a href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a><?php endif; ?></li>
    <?php endforeach; ?>
    <li class="clearList"></li>
  </ul>
<?php endif; ?>