<?php
/**
 * @version		$Id: default.php 1812 2013-01-14 18:45:06Z lefteris.kavadas $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2013 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;
?>

<div class="custom-map">
  <div id="mapcontentcontainer">
  <div id="mapcontent">
    <?php if($params->get('itemPreText')): ?>
      <h2><?php echo $params->get('itemPreText'); ?></h2>
  	<?php endif; ?>
    <div class="section group">
      <div class="col span_1_of_3">
        <div class="moduletable-search"><h4>Выберите из списка интересующий Вас город</h4>
          <?php if(count($items)): ?>
            <select id="city-search">
              <option value=""></option>
              <?php foreach ($items as $key=>$item):	?>
              <option value="<?php echo $item->link; ?>" ><?php if($params->get('itemTitle')): ?><?php echo $item->title; ?><?php endif; ?></option>
              <?php endforeach; ?>
              <option class="clearList"></option>
            </select>
          <?php endif; ?>
        </div>
        <?php
          // This loads the module position
          $document = JFactory::getDocument();
          $renderer = $document->loadRenderer('modules');
          $position = "map-cities";
          $options = array('style' => 'xhtml');
          echo $renderer->render($position, $options, null);
        ?>
      </div>
      <div class="col span_2_of_3"> 
      </div>
    </div>
  </div>
</div>
</div>
