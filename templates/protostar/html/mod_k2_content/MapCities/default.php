<?php
  defined('_JEXEC') or die;
  JHTML::_('behavior.modal');
?>

<?php if(count($items)): ?>
  <ul>
    <?php foreach ($items as $key=>$item):	?>
      <li><?php if($params->get('itemTitle')): ?><a href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a><?php endif; ?></li>
    <?php endforeach; ?>
    <li class="clearList"></li>
  </ul>
<?php endif; ?>
<span class="more">
  <a class="modal" href="#openModal" rel="{size:{x:1000,y:600}}"><span>Cмотреть весь список</span></a>
</span>
<div id="openModal" class="modalDialog">
  <?php
    // This loads the module position
    $document = JFactory::getDocument();
    $renderer = $document->loadRenderer('modules');
    $position = "map-cities-all";
    $options = array('style' => 'xhtml');
    echo $renderer->render($position, $options, null);
  ?>
</div>