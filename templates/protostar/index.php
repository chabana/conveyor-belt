<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

$this->setGenerator('PRICEMETER');

defined('_JEXEC') or die;

JHTML::_('behavior.modal');

// Getting params from template
$params = JFactory::getApplication()->getTemplate(true)->params;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->getCfg('sitename');

if($task == "edit" || $layout == "form" )
{
	$fullWidth = 1;
}
else
{
	$fullWidth = 0;
}

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');
$doc->addScript('templates/' .$this->template. '/js/template.js');
$doc->addScript('templates/' .$this->template. '/js/jquery-ui/jquery-ui.min.js');

// Add Stylesheets
$doc->addStyleSheet('templates/general/css/grid.css');
$doc->addStyleSheet('templates/general/css/fonts.css');
$doc->addStyleSheet('templates/general/css/modal.css');
$doc->addStyleSheet('templates/'.$this->template.'/css/fonts.css');
$doc->addStyleSheet('templates/'.$this->template.'/css/template.css');
$doc->addStyleSheet('templates/'.$this->template.'/css/prettyGallery.css');
$doc->addStyleSheet('templates/'.$this->template.'/js/jquery-ui/jquery-ui.css');


// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', false, $this->direction);

// Add current user information
$user = JFactory::getUser();

// Logo file or site title param
if ($this->params->get('logoFile'))
{
	$logo = '<img src="'. JUri::root() . $this->params->get('logoFile') .'" alt="'. $sitename .'" />';
}
elseif ($this->params->get('sitetitle'))
{
	$logo = '<span class="site-title" title="'. $sitename .'">'. htmlspecialchars($this->params->get('sitetitle')) .'</span>';
}
else
{
	$logo = '<span class="site-title" title="'. $sitename .'">'. $sitename .'</span>';
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<meta name="viewport" content="width=1024">
	<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,700italic,400italic&subset=latin,cyrillic-ext,latin-ext,cyrillic' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic&subset=latin,cyrillic-ext,greek-ext,greek,vietnamese,latin-ext,cyrillic' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	<jdoc:include type="head" />
  
	<!--[if lt IE 9]>
		<script src="<?php echo $this->baseurl ?>/media/jui/js/jquery.prettyGallery.js"></script>
	<![endif]-->
	
	<script>
	 jQuery(document).ready(function(){
     jQuery('a[href*=#]').bind("click", function(e){
        var anchor = jQuery(this);
        jQuery('#contacts').show();
        jQuery('html, body').stop().animate({
           scrollTop: jQuery(anchor.attr('href')).offset().top
        }, 1000);
        e.preventDefault();
     });
     return false;
  });
  jQuery('document').ready(function(){
    jQuery('.clearList a').click(function() {
      jQuery(this).bound.close;
      return false;
    });
  });
  jQuery('document').ready(function(){
    var $tmp = jQuery("#sbox-btn-close.bottom").children().remove();
    var tmp1 = jQuery("#sbox-btn-close.bottom").text();
    jQuery("#sbox-btn-close.bottom").text("").append($tmp).append(jQuery("<span>" + tmp1 + "</span>"))
  });
	</script> 
  
  <script>
	 jQuery(document).ready(function(){
    jQuery('.geo-com').click(function() {
    		jQuery('.geo-comments').toggle(200,function() {
          jQuery("form.pwebcontact-form .pweb-field-cities .pweb-field .pweb-fields-group").data('jsp').reinitialise();
        });
        jQuery('.geo-com.more').toggle(200);
        jQuery('.geo-com.close').toggle(200);
      
    	return false;
    });
    jQuery("a.glossarylink").click(function(event) {
      event.preventDefault();
    });
  }); 
	</script>  
  
  <style>
	.modalDialog {display:none;}
  #sbox-window .modalDialog {display:block;}
	</style>  
	
<link rel="stylesheet" type="text/css" media="all" href="<?php echo $this->baseurl ?>/templates/protostar/css/style.css" />
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/protostar/js/jquery.jcarousel.min.js"></script>

<!-- styles needed by jScrollPane -->
<link type="text/css" href="<?php echo $this->baseurl ?>/templates/protostar/css/jquery.jscrollpane.css" rel="stylesheet" media="all" />

<!-- the mousewheel plugin - optional to provide mousewheel support -->
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/protostar/js/jquery.mousewheel.js"></script>

<!-- the jScrollPane script -->
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/protostar/js/jquery.jscrollpane.js"></script>

<script src="<?php echo $this->baseurl ?>/templates/protostar/js/cities.js" type="text/javascript"></script> 
		
<script type="text/javascript" id="sourcecode">
			jQuery(function()
			{
				jQuery('form.pwebcontact-form .pweb-field-cities .pweb-field .pweb-fields-group').jScrollPane(); 
			});
</script>  

<script type="text/javascript">

jQuery(function() {

	jQuery.fn.startCarousel = function() {
  		var bodywidth = jQuery('.moduletable-clients').width(),
			itemwidth = jQuery('.mycarousel li').outerWidth(true),		
			mycontwidth = bodywidth > itemwidth ? bodywidth - bodywidth%itemwidth : itemwidth,
			licount = jQuery('.mycarousel li').size(),
			jscroll = 1;
			
		if(licount > mycontwidth/itemwidth){
			jscroll =  mycontwidth/itemwidth;
		} else {
			jscroll = 0;
			mycontwidth = licount * itemwidth;
		}
		
		jQuery('.mycont').width(mycontwidth);
			
    	jQuery('.mycarousel').jcarousel({
			scroll:jscroll
		});
  	};
	
	jQuery(this).startCarousel();
	
	jQuery(window).resize(function(){
		jQuery(this).startCarousel();
	}); 
	
	jQuery('.mycarousel').jcarousel({
		scroll:6
	});
});
</script>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
  
</head>

<body class="Itemid-<?php echo $_REQUEST['Itemid']; ?>">
  <div id="wrapper">
  	<div id="headcontainer">
		<div id="headerbox">
  		<header>
    		<div class="section group">
  				<div class="col span_4_of_11">
  				  <a class="logo" href="<?php echo $this->baseurl; ?>">
              <?php echo $logo;?> <?php if ($this->params->get('sitedescription')) { echo '<div class="site-description">'. htmlspecialchars($this->params->get('sitedescription')) .'</div>'; } ?>
            </a>
  			</div>
  				<div class="col span_4_of_8">
  				  <jdoc:include type="modules" name="mainmenu" style="xhtml" />
					<div class="col span_8_of_8">
  				  <jdoc:include type="modules" name="adress" style="xhtml" />
					</div>
  				</div>
  				<div class="col span_1_of_6">
  				  <div class="moduletable-special">
  				    <jdoc:include type="modules" name="specpr" style="xhtml" />
  				  </div>
  				</div>
  			</div>
			
  		</header>
		</div>
	
		<div id="headerslidercontainer">
				<div id="headerslider">
					<div id="headernet">
						<jdoc:include type="modules" name="hslider" style="xhtml" />
						<div class="hns"></div>
						<div class="clear"></div>
					</div>
				</div>
		</div>
  	</div>
	<div id="konvlinecontainer">
	<div class="header"><h2>Конвеерная лента</h2></div>
		<div id="klinebox">
			<jdoc:include type="modules" name="konvline" style="xhtml" />
		</div>
	</div>
  	<div id="servicecontainer">
  		<div id="servicebox">
  		  <jdoc:include type="modules" name="servicebox" style="xhtml" />
		</div>
    </div>
	<div id="konvrazncontainer">
	<div class="header"><h2>Конвейерные ленты: разновидности и применение</h2></div>
		<div id="kraznbox">
			<jdoc:include type="modules" name="konvrazn" style="xhtml" />
		</div>
	</div>
	<div id="psnvrcontainer">
		<div id="psnvrbox">
			<jdoc:include type="modules" name="psnvr" style="xhtml" />
		</div>
	</div>
	<div id="photogalerycontainer">
	<div class="header"><h2>Фотогалерея</h2></div>
		<div id="photogbox">
			<jdoc:include type="modules" name="photog" style="xhtml" />
		</div>
	</div>
	<div id="newscontainer">
	<div class="header"><h2>Новости</h2></div>
		<div id="newsbox">
			<jdoc:include type="modules" name="news" style="xhtml" />
		</div>
	</div>
	<div id="gorodacontainer">
		<div id="gorodabox">
			<div class="gorod">
			<jdoc:include type="modules" name="goroda" style="xhtml" />
			</div>
		</div>
	</div>
  	<div id="footercontainer">
  		<footer class="group">
  		  <div class="wrapper">
    			<div class="col span_4_of_8">
					<div class="col span_2_of_2">
  				  <jdoc:include type="modules" name="mainmenu" style="xhtml" />
					<div class="col span_2_of_3">
						<p>© "Таргет" 2005–2015</p>
					</div>
  				</div>
			
    			</div>
				<div class="col span_4_of_8 footpad-1">
    			  <p>ООО "ТРП "Таргет"<span class="email"><a href="mailto:office@konvejer.com">office@konvejer.com</a></span></p>
    			  
    			  <p>ул. Отакара Яроша, 18-Г 61045 Харьков, Украина
    			</div>
  			</div>
  		</footer>
  	</div>
  </div>
</body>
</html>
