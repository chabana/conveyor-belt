jQuery(function () {
	var allElement = "Вся Россия";
	var citiesFSet = jQuery("[id$='cities']");
	var citiesList = [];
	var citiesChecked = new Set();

	var search = jQuery("<input class='search-cities'/>")
			.keyup(function () {
				var value = search.val().toUpperCase();
				citiesFSet.find("input").each(function () {
					if (!value || jQuery(this).val().toUpperCase().indexOf(value) == 0) {
						jQuery(this).parent().show();
					} else {
						jQuery(this).parent().hide();
					}
				});
			});
	citiesFSet.parent().before(search);

	var selectedArea = jQuery("<div class='cities-selected-area'><div class='cities-selected-area-scroll'></div></div>");
	jQuery("[class$='cities']").first()
			.append("<div class='cities-selected-area-title' style='display:none'>Вы выбрали</div>")
			.append(selectedArea);

	selectedArea.jScrollPane();

	citiesFSet.find("input").each(function () {
		citiesList.push(jQuery(this).val());
		jQuery(this).change(function () {
			var val = jQuery(this).val();

			if (allElement == val) {
				if (jQuery(this).attr("checked")) {
					citiesFSet.find("input").attr("checked", true);
					citiesChecked.clear().add(allElement);
				} else {
					citiesFSet.find("input").attr("checked", false);
					citiesChecked.clear();
				}
			} else {
				var allRus = citiesFSet.find("input[value='" + allElement + "']");
				if (jQuery(this).attr("checked")) {
					citiesChecked.add(val);
				} else if (!jQuery(this).attr("checked") && allRus.attr("checked")) {
					allRus.attr("checked", false);
					citiesChecked.clear();
					citiesFSet.find("input").each(function () {
						if (jQuery(this).attr("checked")) {
							citiesChecked.add(jQuery(this).val());
						}
					});
				} else {
					citiesChecked.remove(val);
				}
			}
			rerenderSelectedArea();
		});
	});

	rerenderSelectedArea = function () {
		var selectedAreaScroll = selectedArea.find('.cities-selected-area-scroll');
		selectedAreaScroll.html("");
		var cities = citiesChecked.getCities();
		if (cities.length > 0) {
			for (i = 0; i < cities.length; i++) {
				selectedAreaScroll.append(getSelectedCity(cities[i]));
			}
			jQuery(".cities-selected-area-title").show();
		} else {
			jQuery(".cities-selected-area-title").hide();
		}
		jQuery(".cities-selected-area").data('jsp').reinitialise();
	};

	getSelectedCity = function (city) {
		return jQuery("<div class='selected-city'>" + city
		+ "<div class='selected-city-del-icon' "
		+ "onclick='unselectCity(\"" + city + "\")'></div></div>");
	};

	unselectCity = function (city) {
		if (allElement == city) {
			citiesFSet.find("input").attr("checked", false);
			citiesChecked.remove(city);
		} else {
			citiesFSet.find("input[value='" + city + "']").attr("checked", false);
			citiesChecked.remove(city);
		}
		rerenderSelectedArea();
	};

	function Set() {
		var citiesChecked = [];

		return {
			add: function (city) {
				if (citiesChecked.indexOf(city) < 0) {
					citiesChecked.push(city);
					//this.sort();
				}
				return this;
			},
			remove: function (city) {
				for (var i = 0; i < citiesChecked.length; i++) {
					if (citiesChecked[i] == city) {
						citiesChecked.splice(i, 1);
						break;
					}
				}
				return this;
			},
			clear: function () {
				citiesChecked = [];
				return this;
			},
			sort: function () {
				citiesChecked = citiesChecked.sort();
				return this;
			},
			getCities: function () {
				return citiesChecked;
			}
		}
	}

	//############ City Search ############
	(function ($) {
		$.widget("custom.combobox", {
			_create: function () {
				this.wrapper = $("<span>")
						.addClass("custom-combobox")
						.insertAfter(this.element);
				this.element.hide();
				this._createAutocomplete();
				//this._createShowAllButton();
			},
			_createAutocomplete: function () {
				var selected = this.element.children(":selected"),
						value = selected.val() ? selected.text() : "";
				this.input = $("<input>")
						.appendTo(this.wrapper)
						.val(value)
						.attr("title", "")
						.addClass("custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left")
						.autocomplete({
							delay: 0,
							minLength: 0,
							source: $.proxy(this, "_source")
						})
						.tooltip({
							tooltipClass: "ui-state-highlight"
						});
				this._on(this.input, {
					autocompleteselect: function (event, ui) {
						ui.item.option.selected = true;
						this._trigger("select", event, {
							item: ui.item.option
						});
					},
					autocompletechange: "_removeIfInvalid"
				});
			},
			_createShowAllButton: function () {
				var input = this.input,
						wasOpen = false;
				$("<a>")
						.attr("tabIndex", -1)
						.attr("title", "Show All Items")
						.tooltip()
						.appendTo(this.wrapper)
						.button({
							icons: {
								primary: "ui-icon-triangle-1-s"
							},
							text: false
						})
						.removeClass("ui-corner-all")
						.addClass("custom-combobox-toggle ui-corner-right")
						.mousedown(function () {
							wasOpen = input.autocomplete("widget").is(":visible");
						})
						.click(function () {
							input.focus();
							// Close if already visible
							if (wasOpen) {
								return;
							}
							// Pass empty string as value to search for, displaying all results
							input.autocomplete("search", "");
						});
			},
			_source: function (request, response) {
				var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(request.term), "i");
				response(this.element.children("option").map(function () {
					var text = $(this).text();
					if (this.value && ( !request.term || matcher.test(text) ))
						return {
							label: text,
							value: text,
							option: this
						};
				}));
			},
			_removeIfInvalid: function (event, ui) {
				// Selected an item, nothing to do
				if (ui.item) {
					return;
				}
				// Search for a match (case-insensitive)
				var value = this.input.val(),
						valueLowerCase = value.toLowerCase(),
						valid = false;
				this.element.children("option").each(function () {
					if ($(this).text().toLowerCase() === valueLowerCase) {
						this.selected = valid = true;
						return false;
					}
				});
				// Found a match, nothing to do
				if (valid) {
					return;
				}
				// Remove invalid value
				this.input
						.val("")
						.attr("title", value + " didn't match any item")
						.tooltip("open");
				this.element.val("");
				this._delay(function () {
					this.input.tooltip("close").attr("title", "");
				}, 2500);
				this.input.autocomplete("instance").term = "";
			},
			_destroy: function () {
				this.wrapper.remove();
				this.element.show();
			}
		});
	})(jQuery);
	jQuery("#city-search").combobox({
		select: function (event, ui) {
			window.location.href = jQuery(ui.item).val();
		}
	});
});