jQuery(document).ready(function(){
    var contactPersonsDef =  jQuery('ul.contact-persons li:first-child').clone();
    jQuery('#loader').hide();

    jQuery.ajaxSetup({
        beforeSend: function() {
            jQuery('#loader').show();
        },
        complete: function(){
            jQuery('#loader').hide();
        },
        statusCode: {
            403: function(data) {
                Joomla.renderMessages({'warning': [data.responseText]});
            }
        }
    });

    jQuery('.contact-names button.add-more').click(function(){
        var el = jQuery('ul.contact-persons').append("<li>" + contactPersonsDef.html() + "</li>");
        jQuery(el).find('li:last input').val('');
    });

    jQuery('ul.contact-persons').delegate('button.delete-person', 'click', function(){
        if (jQuery('ul.contact-persons li').length > 1) {
            jQuery(this).parents('li').remove();
        }
    });

    jQuery('.persons-list-toggle').click(function(ev){
        ev.preventDefault();
        jQuery('.contact-names').toggle(200);
    });

    jQuery('.contact-names button.persons-save').click(function(ev){
        var form = jQuery(this).parents('form');
        var data = jQuery(form).serializeArray();

        jQuery.ajax({
            url: jQuery(form).attr('action'),
            type:jQuery(form).attr('method'),
            dataType:'json',
            data: data,
            success: function(data) {

            }
        });
    });
    jQuery('.period button.get-manuals').click(function(ev){
        var form = jQuery(this).parents('form');
        var data = jQuery(form).serializeArray();

        jQuery.ajax({
            url: jQuery(form).attr('action'),
            type:jQuery(form).attr('method'),
            dataType:'html',
            data: data,
            success: function(data) {
                jQuery('#profile-manuals-forms').html(data);
            }
        });
    });

    jQuery('#profile-manuals-forms').delegate('#form-adv_camp, #form-city, #form-media', 'click', function() {
        var classFieldset = jQuery(this).attr('id');

        if(jQuery(this).attr('checked')){
            jQuery('.'+classFieldset+':enabled').attr('checked', true);
        } else {
            jQuery('.'+classFieldset+':enabled').attr('checked', false);
        }
    });

    jQuery('#profile-manuals-forms').delegate('button.generate-report', 'click', function(){
        var form = jQuery(this).parents('form');
        var data = jQuery(form).serializeArray();
        var dataManuals = jQuery(form).prev().serializeArray();

        var error = new Array();

        var advCampCount = jQuery('input[name*="adv_camp"]:checked').length;
        var cityCount = jQuery('input[name*="city"]:checked').length;
        var mediaCount = jQuery('input[name*="media"]:checked').length;
        var emailCount = jQuery('input[name*="email"]:checked').length;

        if (jQuery('#form-adv_camp').length > 0 && (advCampCount == 0)) {
            error.push('Рекламные кампании');
        }

        if (jQuery('#form-city').length > 0 && (cityCount == 0)) {
            error.push('Города');
        }

        if (jQuery('#form-media').length > 0 && (mediaCount == 0)) {
            error.push('Медиа');
        }

        if (emailCount == 0) {
            error.push('Адреса для отправки');
        }

        jQuery('#report-request-error').hide();
        if (error.length > 0) {
            jQuery('#report-request-error').html('Укажите критерии для создани отчета: ' + error.join(', '));
            jQuery('#report-request-error').show();
        } else {
            dataManuals.splice('format', 1);
            dataManuals.splice('layout', 2);

            data = data.concat(dataManuals);

            jQuery.ajax({
                url: jQuery(form).attr('action'),
                type:jQuery(form).attr('method'),
                dataType:'html',
                data: data,
                success: function(data) {
                    jQuery('.report-history').html(data);
                }
            });
        }
    });

    jQuery('#profile-check-report').delegate('button.get-status', 'click', function(){
        var form = jQuery(this).parents('form');
        var report_id = jQuery(this).data('id');
        var data = jQuery(form).serializeArray();
        var self = jQuery(this);

        data.push({'name':'report_id', 'value': report_id});

        jQuery.ajax({
            url: jQuery(form).attr('action'),
            type:jQuery(form).attr('method'),
            dataType:'html',
            data: data,
            success: function(data) {
                obj = JSON.parse(data);

                jQuery(self).parents('li').find('.requested').removeClass('done');
                jQuery(self).parents('li').find('.requested').removeClass('queue');
                jQuery(self).parents('li').find('.requested').removeClass('error');

                jQuery(self).parents('li').find('.requested').text(obj.data.status_text);
                jQuery(self).parents('li').find('.requested').addClass(obj.data.status_class);
                if (obj.data.status_class == 'done') {
                    jQuery(self).remove();
                }
            }
        });
    });

    return false;
});