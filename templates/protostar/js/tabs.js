jQuery(document).ready(function(){
  jQuery('#info p:not(:first)').hide();
  
  jQuery('#info-nav li').click(function(event) {
    event.preventDefault();
    jQuery('#info p').hide();
    jQuery('#info-nav .current').removeClass("current");
    jQuery(this).addClass('current');
    
    var clicked = jQuery(this).find('a:first').attr('href');
    jQuery('#info ' + clicked).fadeIn('fast');
  }).eq(0).addClass('current');
});
