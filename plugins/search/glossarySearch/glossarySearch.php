<?php

/**************************************************************
* This file is part of Glossary
* Copyright (c) 2008-9 Martin Brampton, P De Nictolis
* Issued as open source under GNU/GPL
* For support and other information, visit http://remository.com
* To contact Martin Brampton, write to martin@remository.com
*
* Please see glossary.php for more details
*/

if (!defined('_CMSAPI_ABSOLUTE_PATH')) die ('This software requires the Jaliro environment');

// Define CMS environment
if (defined('_CMSAPI_ABSOLUTE_PATH') AND is_readable($gclasspath = _CMSAPI_ABSOLUTE_PATH.'/components/com_glossary/glossary.class.php')) {
	require_once($gclasspath);
}
else return;
// End of CMS environment definition

// error_reporting(E_ALL);

if ('Joomla' == _CMSAPI_CMS_BASE) {
	if(version_compare(JVERSION,'1.6.0','<')) {
		jimport( 'joomla.event.plugin' );

		class plgSearchGlossarySearch extends JPlugin  {
			protected $plugin_type = 'search';
			
			// This is not required if there is nothing to be done in the constructor
			// public function glossarySearch ($params) {
			//	parent::__construct( $params );
			// }

			public function onSearch ($searchword, $matchtype, $order, $selector) {
				if (!empty($selector) AND !in_array('glossarySearch', $selector)) return array();
			 	$worker = new glossary_plugin_search();
			 	return $worker->onSearch ($this->params, $searchword, $matchtype, $order);
			}
			
			public function onSearchAreas () {
				$configuration = aliroComponentConfiguration::getInstance('com_glossary');
				$interface = glossaryInterface::getInstance();
				$forcelang = $configuration->language != '';
				$interface->loadLanguageFile ($configuration, $forcelang, 'images/glossary/language');
				return array ('glossarySearch' => _GLOSSARY_COMPONENT_TITLE);
			}
		}
	}
	else {
		class plgSearchGlossarySearch extends JPlugin  {
			protected $plugin_type = 'search';
			
			public function __construct(&$subject, $config = array()){
				parent::__construct($subject, $config);
				$this->loadLanguage();
			}

			public function onContentSearch ($searchword, $matchtype='', $order='', $selector=null) {
				if (!empty($selector) AND !in_array('glossarySearch', $selector)) return array();
			 	$worker = new glossary_plugin_search();
			 	return $worker->onSearch ($this->params, $searchword, $matchtype, $order);
			}
			
			public function onContentSearchAreas () {
				$configuration = aliroComponentConfiguration::getInstance('com_glossary');
				$interface = glossaryInterface::getInstance();
				$forcelang = $configuration->language != '';
				$interface->loadLanguageFile ($configuration, $forcelang, 'images/glossary/language');
				return array ('glossarySearch' => _GLOSSARY_COMPONENT_TITLE);
			}
		}
	}
}

else {
	global $_MAMBOTS;
	/** Register search function inside Mambo's API */
	$_MAMBOTS->registerFunction( 'onSearch', 'botSearchGlossary' );

	function botSearchGlossary ($text, $matchtype='', $order='') {
	 	$worker = new glossary_plugin_search();
		// Get parameters from Database
		$query = "SELECT params FROM #__mambots WHERE element = 'glossarySearch' AND folder = 'search'";
		$database = glossaryInterface::getInstance()->getDB();
		$database->setQuery($query);
		$paramstring = $database->loadResult();
		$params = new mosParameters($paramstring);
	 	return $worker->onSearch ($params, $text, $matchtype, $order);
	}
}