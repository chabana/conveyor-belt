<?php

/**Remove PEAR, align J15 and J25
 * Jaliro for Joomla 1.5+
 * License : http://www.gnu.org/copyleft/gpl.html ver 2
 * counterpoint@aliro.org
 * http://aliro.org
 */

/**
 * Jaliro upgrade Joomla with some Aliro capabilities
 *
 */

defined ('_JEXEC') OR die('Direct Access to this location is not allowed.');

jimport('joomla.plugin.plugin');
jimport('joomla.event.plugin');
jimport('joomla.html.parameter');

//JPlugin::loadLanguage('plg_jaliro', JPATH_ADMINISTRATOR);

class plgSystemJaliro extends JPlugin  {
	public static $active = false;
	public static $mootools = 2;
	public static $mootvers = 1;
	public static $jquery = 1;
	public static $jqueryui = 1;
	public static $cmsgroups = 0;

	private static $accesskey = '';
	private static $secretkey = '';
	private static $bucket = '';

	private static $japarams = null;
	// Must be kept the same as the declaration in the Jaliro loader.php
	private static $jalirolibs = array(
		'aliro', 'cmsapi', 'extclasses'
	);
	private static $jalirotables = array (
		'makeConfigTable', 'makeLogTable', 'makeAssignTable', 'makePermitTable', 'makeStringToObjectTable',
		'makeRoleLinkTable', 'populateRoleLinkTable', 'makeQuerySlowTable', 'makeQueryStatsTable'
	);
	private static $makeConfigTable = "CREATE TABLE IF NOT EXISTS `#__cmsapi_configurations` (
				  `component` varchar(100) NOT NULL,
				  `instance` int(10) NOT NULL default '0',
				  `configuration` mediumtext NOT NULL,
				  PRIMARY KEY  (`component`)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8";
	private static $makeLogTable = "CREATE TABLE IF NOT EXISTS `#__cmsapi_error_log` (
				  `id` int(11) NOT NULL auto_increment,
				  `timestamp` datetime NOT NULL,
				  `smessage` varchar(255) NOT NULL,
				  `dbname` varchar(255) NOT NULL default '',
				  `dberror` varchar(255) NOT NULL default '',
				  `dbmessage` text NOT NULL,
				  `dbtrace` text NOT NULL,
				  `sql` text NOT NULL,
				  `lmessage` text NOT NULL,
				  `errorkey` text NOT NULL,
				  `referer` text NOT NULL,
				  `ip` varchar(15) NOT NULL,
				  `get` text NOT NULL,
				  `post` text NOT NULL,
				  `trace` text NOT NULL,
				  PRIMARY KEY  (`id`),
				  KEY `stamp` (`timestamp`),
				  KEY `errorkey` (`errorkey`(40))
				) ENGINE=MyISAM  DEFAULT CHARSET=utf8";
	private static $makeAssignTable = "CREATE TABLE IF NOT EXISTS `#__assignments` (
				  `id` int(11) NOT NULL auto_increment,
				  `access_type` varchar(60) NOT NULL,
				  `access_id` text NOT NULL,
				  `role` varchar(60) NOT NULL,
				  PRIMARY KEY  (`id`),
				  KEY `access_type` (`access_type`,`access_id`(60),`role`)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8";
	private static $makePermitTable = "CREATE TABLE IF NOT EXISTS `#__permissions` (
				  `id` int(11) NOT NULL auto_increment,
				  `role` varchar(60) NOT NULL,
				  `control` tinyint(3) unsigned NOT NULL default '0',
				  `action` varchar(60) NOT NULL,
				  `subject_type` varchar(60) NOT NULL,
				  `subject_id` text NOT NULL,
				  `system` smallint(5) unsigned NOT NULL default '0',
				  PRIMARY KEY  (`id`),
				  KEY `role_type` (`role`,`action`,`subject_type`,`subject_id`(60)),
				  KEY `subaction` (`subject_type`,`action`,`subject_id`(60))
				) ENGINE=MyISAM DEFAULT CHARSET=utf8";
	private static $makeRoleLinkTable = "CREATE TABLE IF NOT EXISTS `#__role_link` (
				  `role` varchar(60) NOT NULL default '',
				  `implied` varchar(60) NOT NULL default '',
				  PRIMARY KEY  (`role`, `implied`)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8";
	private static $populateRoleLinkTable = "INSERT INTO `#__role_link` (`role`, `implied`) VALUES
				('Administrator', 'Manager'),
				('Author', 'Registered'),
				('Editor', 'Author'),
				('Manager', 'Publisher'),
				('Publisher', 'Editor'),
				('Super Administrator', 'Administrator') ON DUPLICATE KEY UPDATE role = role;";
	private static $makeStringToObjectTable = "CREATE TABLE IF NOT EXISTS `#__string_to_object` (
				  `string32` int(11) NOT NULL,
				  `string` mediumtext NOT NULL,
				  `object` mediumtext NOT NULL,
				  PRIMARY KEY `string32` (`string32`,`string`(255))
				) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
	private static $makeQuerySlowTable = "CREATE TABLE IF NOT EXISTS `#__query_slow` (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `queryid` int(11) NOT NULL DEFAULT '0',
				  `time` float NOT NULL DEFAULT '0',
				  `trace` text NOT NULL,
				  `querytext` text NOT NULL,
				  PRIMARY KEY (`id`),
				  KEY `querystats` (`queryid`)
				) ENGINE=MyISAM  DEFAULT CHARSET=utf8;";
	private static $makeQueryStatsTable = "CREATE TABLE IF NOT EXISTS `#__query_stats` (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `count` smallint(6) NOT NULL DEFAULT '0',
				  `mean` float NOT NULL DEFAULT '0',
				  `median` float NOT NULL DEFAULT '0',
				  `stdev` float NOT NULL DEFAULT '0',
				  `best` float NOT NULL DEFAULT '0',
				  `worst` float NOT NULL DEFAULT '0',
				  `total` float NOT NULL DEFAULT '0',
				  `elapsed` float NOT NULL DEFAULT '0',
				  `memory` int(11) NOT NULL DEFAULT '0',
				  `uri` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
				  `post` text NOT NULL,
				  `ip` varchar(15) NOT NULL DEFAULT '',
				  `stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
				  PRIMARY KEY (`id`),
				  KEY `stamp` (`stamp`)
				) ENGINE=MyISAM  DEFAULT CHARSET=utf8;";

	private $plugin_type = 'system';

	private $mydir = '';
	private $mylibraries = '';

	public function __construct ($subject, $config) {
		parent::__construct($subject, $config);
		self::$japarams = $this->params;
		$config = new JConfig();
		error_reporting((int) ($config->error_reporting & ~2048));

		//load the translation
		$this->loadLanguage( );

		// add css... BTW you can't add css in onAfterRender
		if ($this->params->get('add_css', 0) AND $this->isDebug()) {
			JFactory::getDocument()->addStyleSheet('/plugins/system/jaliro/css/marcosextendeddebug.css');
		}
	}

	public function onAfterInitialise () {
		self::$active = $this->params->get('active', true);
		self::$cmsgroups = $this->params->get('cmsgroups', 0);
		self::$mootools = $this->params->get('mootools', 2);
		self::$mootvers = '1.'.$this->params->get('mootvers', 1);
		self::$jquery = $this->params->get('jquery', 1);
		self::$jqueryui = $this->params->get('jqueryui', 1);
		self::$accesskey = $this->params->get('access_key');
		self::$secretkey = $this->params->get('secret_key');
		self::$bucket = $this->params->get('bucket');
		$this->mydir = dirname(__FILE__);
		$this->mylibraries = $this->mydir.'/jaliro/libraries/';
		$this->commonWork();
	}

	private function commonWork () {
		clearstatcache();
		if (self::$active AND file_exists($this->mydir.'/jaliro/loader.php')) {
			if (!$this->files_identical($this->mydir.'/jaliro/loader.php', JPATH_ROOT.'/libraries/loader.php')) {
				if (!is_writeable(JPATH_ROOT.'/libraries/')) {
					if (JPATH_BASE == JPATH_ADMINISTRATOR) {
						JFactory::getApplication()->enqueueMessage('Unable to properly activate Jaliro - ../libraries/ not writeable');
					}
					self::$active = false;
					return;
				}
				$jversion = new JVersion();
				if (file_exists(JPATH_ROOT.'/libraries/joomla.loader.php')) @unlink (JPATH_ROOT.'/libraries/loader.php');
				else @rename(JPATH_ROOT.'/libraries/loader.php', JPATH_ROOT.'/libraries/joomla.loader.php');
				copy($this->mydir.'/jaliro/loader.php', JPATH_ROOT.'/libraries/loader.php');
				copy($this->mydir.'/jaliro/json/core_database.json', $this->mydir.'/jaliro/json/run_core_database.json');
				if (is_writeable(JPATH_ROOT)) copy($this->mydir.'/jaliro/timed.php', JPATH_ROOT.'/timed.php');
				$database = JFactory::getDBO();
				foreach (self::$jalirotables as $table) {
					$database->setQuery(self::$$table);
					$database->query();
				}
				//$database->setQuery("UPDATE #__plugins SET published = 0 WHERE element = 'mtupgrade' OR element = 'debug'");
				//$database->query();
				JFactory::getApplication()->redirect('index.php?option=com_installer', 'Jaliro active');
			}
			if (class_exists('JALoader')) {
				$loader = call_user_func(array('JALoader', 'getInstance'));
				foreach (self::$jalirolibs as $jalib) $loader->jaAutoDiscover($this->mylibraries.$jalib);
			}
			require_once ($this->mylibraries.'extclasses/HTMLPurifier/Bootstrap.php');
			$dbjson = $this->mydir.'/jaliro/json/run_core_database.json';
			if (class_exists('aliroDatabaseManager') AND file_exists($dbjson)) {
				$manager = new aliroDatabaseManager();
				$manager->updateTables (file_get_contents($dbjson));
				@unlink($dbjson);
			}
		}
		elseif (class_exists('JALoader', false)) {
			JALoader::uninstall(true);
		}
	}
	
	public function onAfterRender () {
		if (class_exists('JALoader', false)) {
			if ($this->mydir) $this->commonWork();
			else $this->onAfterInitialise();
		}
		if ($this->isDebug()) {
			$putter = new cmsapiDebug($this->params);
			$putter->debugOutput();
		}
	}

	public static function isDebug () {
		if (JDEBUG AND class_exists('cmsapiInterface')) {
			$iplimits = self::$japarams->get('limit_to_ip', '');
			if ($iplimits) {
				$remoteIP = ip2long(cmsapiInterface::getInstance('com_cmsapi')->getIP());
				foreach (explode(',', $iplimits) as $ipString) {
					if (strpos($ipString, '/') !== false){
					    list ($netString, $maskString) = explode ("/", $ipString);
						// get the network
					    $net = ip2long ($netString);
						// compute netmask
					    $mask = ~((1 << (32 - (int)$maskString)) - 1);
						// compute network of remote ip by ANDing netmask
					    $netIP = $remoteIP & $mask;
						// compare two networks
					    if($netIP == $net) {
							if (!defined('JaDEBUG')) define ('JaDEBUG', 1);
							return true;
						}
					}
					elseif ($remoteIP == ip2long($ipString)) {
						if (!defined('JaDEBUG')) define ('JaDEBUG', 1);
						return true;
					}
				}
			}
		}
		if (!defined('JaDEBUG')) define ('JaDEBUG', 0);
		return false;
	}

	//   pass two file names
	//   returns TRUE if files are the same, FALSE otherwise
	private function files_identical ($fn1, $fn2) {
		$read_length = 4096;
		$same = true;
	    if (filetype($fn1) !== filetype($fn2) OR filesize($fn1) !== filesize($fn2)) return false;
	    if (!$fp1 = fopen($fn1, 'rb')) $same = false;
	    if (!$fp2 = fopen($fn2, 'rb')) $same = false;
	    while ($same AND !feof($fp1) AND !feof($fp2)) if(fread($fp1, $read_length) !== fread($fp2, $read_length)) $same = false;
		if($same AND feof($fp1) !== feof($fp2)) $same = false;
		@fclose($fp1);
		@fclose($fp2);
		return $same;
	}

	public static function setAmazonS3Params (&$accesskey, &$secretkey, &$bucket) {
		$accesskey = self::$accesskey;
		$secretkey = self::$secretkey;
		$bucket = self::$bucket;
	}
}
 