<?php // no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); ?>
<script language="javascript" type="text/javascript">
<!--
	function submitbutton3(pressbutton) {
		var form = document.adminForm;

		// do field validation
		if (form.install_directory.value == ""){
			alert( "<?php echo JText::_( 'Please select a directory', true ); ?>" );
		} else {
			form.installtype.value = 'folder';
			form.submit();
		}
	}

	function submitbutton4(pressbutton) {
		var form = document.adminForm;

		// do field validation
		if (form.install_url.value == "" || form.install_url.value == "http://"){
			alert( "<?php echo JText::_( 'Please enter a URL', true ); ?>" );
		} else {
			form.installtype.value = 'url';
			form.submit();
		}
	}

	function submitbutton5(pressbutton) {
		var form = document.adminForm;

		// do field validation
		if (form.install_jaliro.value == "0"){
			alert( "<?php echo JText::_( 'SELECT ITEM', true ); ?>" );
		} else {
			form.installtype.value = 'url';
			form.submit();
		}
	}
//-->
</script>

<form enctype="multipart/form-data" action="index.php" method="post" name="adminForm">

	<?php if ($this->ftp) : ?>
		<?php echo $this->loadTemplate('ftp'); ?>
	<?php endif; ?>

	<table class="adminform">
	<tr>
		<th colspan="2">Jaliro <?php echo JText::_( 'INSTALL' ); ?></th>
	</tr>
	<tr>
		<td>
			<select id="install_jaliro" name="install_jaliro" class="input_box">
				<option value="0"><?php echo JText::_( 'SELECT FROM LIST' ); ?></option>
				<option value="Jaliro">Jaliro</option>
				<option value="Glossary">Glossary</option>
				<option value="Remository">Remository</option>
			</select>
			<label for="jaliro_user"> Jaliro <?php echo JText::_( 'USERNAME' ); ?>:</label>
			<input class="input_box" id="jaliro_user" name="jaliro_user" type="text" size="20" value="<?php echo @aliroComponentConfiguration::getConfiguration('plugin_jaliro')->user; ?>" />
			<label for="jaliro_pass"> Jaliro <?php echo JText::_( 'PASSWORD' ); ?>:</label>
			<input class="input_box" id="jaliro_pass" name="jaliro_pass" type="password" size="20" value="<?php echo @aliroComponentConfiguration::getConfiguration('plugin_jaliro')->password; ?>" />
			<input type="button" class="button" value="<?php echo JText::_( 'INSTALL' ); ?>" onclick="submitbutton5()" />
		</td>
	</tr>
	<tr>
		<td>
			Installed Jaliro version: <?php echo jaliro::$version; ?>
		</td>
	</tr>
	</table>

	<table class="adminform">
	<tr>
		<th colspan="2"><?php echo JText::_( 'Upload Package File' ); ?></th>
	</tr>
	<tr>
		<td width="120">
			<label for="install_package"><?php echo JText::_( 'Package File' ); ?>:</label>
		</td>
		<td>
			<input class="input_box" id="install_package" name="install_package" type="file" size="57" />
			<input class="button" type="button" value="<?php echo JText::_( 'Upload File' ); ?> &amp; <?php echo JText::_( 'Install' ); ?>" onclick="submitbutton()" />
		</td>
	</tr>
	</table>

	<table class="adminform">
	<tr>
		<th colspan="2"><?php echo JText::_( 'Install from directory' ); ?></th>
	</tr>
	<tr>
		<td width="120">
			<label for="install_directory"><?php echo JText::_( 'Install directory' ); ?>:</label>
		</td>
		<td>
			<input type="text" id="install_directory" name="install_directory" class="input_box" size="70" value="<?php echo $this->state->get('install.directory'); ?>" />
			<input type="button" class="button" value="<?php echo JText::_( 'Install' ); ?>" onclick="submitbutton3()" />
		</td>
	</tr>
	</table>

	<table class="adminform">
	<tr>
		<th colspan="2"><?php echo JText::_( 'Install from URL' ); ?></th>
	</tr>
	<tr>
		<td width="120">
			<label for="install_url"><?php echo JText::_( 'Install URL' ); ?>:</label>
		</td>
		<td>
			<input type="text" id="install_url" name="install_url" class="input_box" size="70" value="http://" />
			<input type="button" class="button" value="<?php echo JText::_( 'Install' ); ?>" onclick="submitbutton4()" />
		</td>
	</tr>
	</table>

	<input type="hidden" name="type" value="" />
	<input type="hidden" name="installtype" value="upload" />
	<input type="hidden" name="task" value="doInstall" />
	<input type="hidden" name="option" value="com_installer" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>
