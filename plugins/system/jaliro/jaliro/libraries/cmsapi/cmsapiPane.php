<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

// CMS independent check to prevent direct execution
if (basename(@$_SERVER['REQUEST_URI']) == basename(__FILE__)) die ('This software is for use within a larger system');

JLoader::register('JPaneTabs', JPATH_PLATFORM.'/joomla/html/pane.php');

class cmsapiPane extends JPaneTabs {
	function startTab ($tabText, $tabid) {
		echo parent::startPanel ($tabText, $tabid);
	}
	function endTab () {
		echo parent::endPanel();
	}
	function startPane ($paneid) {
		echo parent::startPane($paneid);
	}
	function endPane () {
		echo parent::endPane();
	}
}