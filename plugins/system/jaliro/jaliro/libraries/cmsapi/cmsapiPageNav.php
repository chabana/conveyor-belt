<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

// CMS independent check to prevent direct execution
if (basename(@$_SERVER['REQUEST_URI']) == basename(__FILE__)) die ('This software is for use within a larger system');

jimport ('joomla.html.pagination');

class cmsapiPageNav extends JPagination {
    protected $cname = '';

    public function __construct ($total, $limitstart, $limit, $cname) {
        $this->cname = $cname;
        parent::__construct($total, $limitstart, $limit);
    }

    protected function T_($string) {
        return JText::_($string);
    }

    function writeLimitBox () {
        return $this->getLimitBox();
    }
    function writePagesLinks () {
        return $this->getPagesLinks();
    }
    function writePagesCounter () {
        return $this->getPagesCounter();
    }
    function listFormEnd ($pagecontrol=true) {
        $act = $_REQUEST['act'];
        $hiddenhtml = <<<HIDDEN_HTML

				<div>
					<input type="hidden" name="option" value="$this->cname" />
					<input type="hidden" name="task" value="" />
					<input type="hidden" name="limitstart" value="" />
					<input type="hidden" name="act" value="$act" />
					<input type="hidden" name="boxchecked" value="0" />
				</div>

HIDDEN_HTML;

        if ($pagecontrol) {
            $displaynum = $this->T_('Display #');
            $links = $this->writePagesLinks();
            $limits = $this->writeLimitBox();
            $counter = $this->writePagesCounter();
            echo <<<PAGE_CONTROL1

			<tfoot>
			<tr>
	    		<td colspan="15">
	    		<div class="container"><div class="pagination">
					<div class="limit">
						$limits
						$links
					</div>


				</div></div>
				$hiddenhtml
				</td>
			</tr>
			</tfoot>

PAGE_CONTROL1;

        }
        else {
            echo <<<END_PAGE

			<tfoot>
			<tr>
	    		<th align="center" colspan="13">
	    			&nbsp;
	    			$hiddenhtml
				</th>
			</tr>
			</tfoot>

END_PAGE;

        }
    }
}
