<?php

/* ******************************************************************
* This file is a generic interface to Aliro, Joomla 1.5+, Joomla 1.0.x and Mambo
* Copyright (c) 2008 Martin Brampton
* Issued as open source under GNU/GPL
* For support and other information, visit http://acmsapi.org
* To contact Martin Brampton, write to martin@remository.com
*
*/

// CMS independent check to prevent direct execution
if (basename(@$_SERVER['REQUEST_URI']) == basename(__FILE__)) die ('This software is for use within a larger system');

class anyJoomlaInstaller {
	protected $database = null;
	protected $http = null;
	protected $modulekeys = array();
	protected $pluginkeys = array();
	protected $jinstaller = null;
	protected $componentID = 0;

	public function __construct () {
		$this->database = aliroDatabase::getInstance();
		$this->jinstaller = JInstaller::getInstance();
	}
	
	// CMS Specific code
	protected function T_ ($string) {
		return JText::_($string);
	}

	// This method is specific to Jaliro, not for general use
	public function handleUserPass () {
		$jalirouser = JRequest::getVar('jaliro_user', '', 'post', 'string');
		$jaliropass = JRequest::getVar('jaliro_pass', '', 'post', 'string');
		$jaliroconfig = aliroComponentConfiguration::getConfiguration('plugin_jaliro');
		if ($jalirouser OR $jaliropass) {
			if ($jalirouser) $jaliroconfig->user = $jalirouser;
			if ($jaliropass) $jaliroconfig->password = $jaliropass;
			$jaliroconfig->save();
		}
		if (!(@$jaliroconfig->user AND @$jaliroconfig->password)) return false;
		$codeduser = base64_encode($jaliroconfig->user);
		$codedpass = base64_encode($jaliroconfig->password);
		$codedsite = base64_encode(JFactory::getApplication()->getSiteURL());
		return "Authorization: JALIRO $codedsite:$codeduser:$codedpass";
	}

	public function getFileData ($url, $security='') {
		$this->http = new eacHttpRequest();
		if ($security) $this->http->header($security);
		$contents = $this->http->get($url);
		$result = $this->http->getHttpStatus();
		if (200 != $result) {
			JError::raiseError($result, $this->T_('SERVER_CONNECT_FAILED').', '.$result);
			JError::refuseMore();
			return false;
		}
		return $contents;
	}

	public function storeFile ($contents, $url) {
		if ($this->http instanceof eacHttpRequest) {
			$response_headers = $this->http->getHeaders();
			foreach ($response_headers as $header) {
				if (0 === strpos($header, 'Content-Disposition')) {
					$contentfilename = explode ("\"", $header);
					if (isset($contentfilename[1])) {
						$target = $contentfilename[1];
						break;
					}
				}
			}
		}
		// Set the target path if not given
		$config =& JFactory::getConfig();
		if (empty($target)) $target = $config->getValue('config.tmp_path').DS.JInstallerHelper::getFilenameFromURL($url);
		else $target = $config->getValue('config.tmp_path').DS.basename($target);

		// Write buffer to file
		JFile::write($target, $contents);
		return $target;
	}

	public function unpack ($name) {
		$tmp_dest 	= JFactory::getConfig()->getValue('config.tmp_path');
		// Unpack the downloaded package file
		$package = JInstallerHelper::unpack($tmp_dest.DS.$name);
		return $package;
	}

	public function installPackage ($package) {
		// Install the package
		return $this->jinstaller->install($package['dir']);
	}

	public function cleanUp ($package) {
		// Cleanup the install files
		if (!is_file($package['packagefile'])) {
			$config =& JFactory::getConfig();
			$package['packagefile'] = $config->getValue('config.tmp_path').DS.$package['packagefile'];
		}
		JInstallerHelper::cleanupInstall($package['packagefile'], $package['extractdir']);
	}

	public function securityFieldsHTML () {
		$usertext = $this->T_( 'USERNAME' );
		$passtext = $this->T_( 'PASSWORD' );
		$username = @aliroComponentConfiguration::getConfiguration('plugin_jaliro')->user;
		$password = @aliroComponentConfiguration::getConfiguration('plugin_jaliro')->password;
		return <<<SECURITY_FIELDS

			<label for="jaliro_user"> Jaliro $usertext:</label>
			<input class="input_box" id="jaliro_user" name="jaliro_user" type="text" size="20" value="$username" />
			<label for="jaliro_pass"> Jaliro $passtext:</label>
			<input class="input_box" id="jaliro_pass" name="jaliro_pass" type="password" size="20" value="$password" />

SECURITY_FIELDS;

	}

	protected function handleModuleFile ($remove, $files, $mainpath, $filename, $modname, $title, $countsql) {
		if ($remove) {
			if (!empty($modname)) $this->modulekeys[] = $modname;
			@unlink($mainpath.'/'.$filename);
		}
		else {
			// Note - this will happen only once for a whole module, only one file has module name specified
			if (!empty ($modname)) {
				$this->jinstaller->setPath('extension_root', $mainpath);
				$message = $this->installFiles($files, 'module');
				if ($message) return '<br />'.$message;
				$this->database->setQuery($countsql);
				$id = $this->database->loadResult();
				$message = $this->makeModule($modname, $title, $id);
				if ($message) return '<br />'.$message;
				if (0 == $id) {
					echo '<br />'.sprintf($this->T_('Module %s has been installed'), $modname);
				}
				else echo '<br />'.sprintf($this->T_('Module %s has been updated'), $modname);
			}
		}
	} 

	protected function handlePluginFile ($remove, $files, $mainpath, $filename, $plugname, $title, $groupname, $countsql) {
		if ($remove) {
			if (!empty($plugname)) $this->pluginkeys[] = $plugname;
			@unlink($mainpath.'/'.$filename);
		}
		else {
			// Note - this will happen only once for a whole plugin, only one file has plugin name specified
			if (!empty ($plugname) AND !empty($groupname)) {
				$this->jinstaller->setPath('extension_root', $mainpath);
				$message = $this->installFiles($files, 'plugin');
				if ($message) return '<br />'.$message;
				$this->database->setQuery($countsql);
				$id = $this->database->loadResult();
				$message = $this->makePlugin($plugname, $groupname, $title, $id);
				if ($message) return '<br />'.$message;
				if (0 == $id) {
					echo '<br />'.sprintf($this->T_('Plugin %s has been installed'), $plugname);
				}
				else echo '<br />'.sprintf($this->T_('Plugin %s has been updated'), $plugname);
			}
		}
	} 

	protected function installFiles ($element, $type) {
		/*
		 * If the directory already exists, then we will assume that the
		 * add-on is already installed or another add-on is using that
		 * directory.
		 */
		if ('module' == $type AND file_exists($this->jinstaller->getPath('extension_root')) AND !$this->jinstaller->getOverwrite()) {
			$this->jinstaller->abort($this->T_('Module').' '.$this->T_('Install').': '.$this->T_('Another module is already using directory').': "'.$this->jinstaller->getPath('extension_root').'"');
			return sprintf($this->T_('Another %s is already using directory'), $type);
		}
		// If the add-on directory does not exist, lets create it
		$created = false;
		if (!file_exists($this->jinstaller->getPath('extension_root'))) {
			if (!$created = JFolder::create($this->jinstaller->getPath('extension_root'))) {
				$this->jinstaller->abort($this->T_($type).' '.$this->T_('Install').': '.$this->T_('Failed to create directory').': "'.$this->jinstaller->getPath('extension_root').'"');
				return $this->T_('Failed to create directory');
			}
		}
		/*
		 * Since we created the module directory and will want to remove it if
		 * we have to roll back the installation, lets add it to the
		 * installation step stack
		 */
		if ($created) {
			$this->jinstaller->pushStep(array ('type' => 'folder', 'path' => $this->jinstaller->getPath('extension_root')));
		}

		// Copy all necessary files
		if ($this->jinstaller->parseFiles($element, -1) === false) {
			// Install failed, roll back changes
			$this->jinstaller->abort();
			return $this->T_('Unable to parse files for copying');
		}
	}

	protected function removeFiles ($element, $type) {

	}

	public function makeMenuEntry () {
		$this->componentID = $this->getComponentID();
		if ($this->getMenuCount()) {
			$this->database->setQuery("UPDATE #__menu SET $this->cidname = $this->componentID WHERE link LIKE 'index.php?option=$this->cname%'");
			$this->database->query();
			return;
		}
		if ('Aliro' == _CMSAPI_CMS_BASE) $this->makeAliroMenuEntry();
		elseif ('Joomla' == _CMSAPI_CMS_BASE) $this->makeJoomlaMenuEntry();
		else {
			$this->database->setQuery("SELECT MAX(ordering) FROM `#__menu`");
			$ordering = intval($this->database->loadResult() + 1);
			$this->database->setQuery("INSERT INTO `#__menu` "
			." (`id`, `menutype`, `name`, `link`, `type`, `published`, `parent`, `componentid`, `sublevel`, `ordering`, `checked_out`, `checked_out_time`, `pollid`, `browserNav`, `access`, `utaccess`, `params`) "
			." VALUES (NULL , 'mainmenu', '$this->ctitle', 'index.php?option=$this->cname', 'components', '1', '0', $this->componentID, '0', $ordering, '0', '0000-00-00 00:00:00', '0', '0', '0', '0', '')");
			$this->database->query();
		}
	}
	
	protected function makeAliroMenuEntry () {
		
	}
	
	protected function getMenuCount () {
		$this->database->setQuery("SELECT count(*) FROM `#__menu` WHERE menutype = 'mainmenu' AND link LIKE 'index.php?option={$this->cname}%'");
		return intval($this->database->loadResult());
	}
}

class Joomla15Installer extends anyJoomlaInstaller {
	protected $cidname = 'componentid';

	protected $countSQL = "SELECT COUNT(*) FROM %s WHERE %s = '%s'";
	protected $deleteSQL = "DELETE FROM %s WHERE %s IN ('%s')";

	public function installModulesPlugins ($remove=false) {
		// Set the installation path
		// For the time being handle only user side modules
		$manifest = $this->jinstaller->getManifest()->document;
		$mtype = 'user';
		$files = $manifest->getElementByPath('files');
		$modules = $manifest->getElementByPath('modules');
		$ROOT_PATH = 'admin' == $mtype ? JPATH_ADMINISTRATOR : JPATH_SITE;
		$database = aliroDatabase::getInstance();
		if (is_object($modules)) foreach ($modules->children() as $module) {
			$title = $module->attributes('title');
			$files = $module->getElementByPath('files');
			$names = $files->children();
			foreach ($names as $filename) {
				$modname = $filename->attributes('module');
				$mainpath = JPATH_SITE.'/modules/'.$modname;
				$countsql = sprintf($this->countSQL, '#__modules', 'module', $modname);
				$this->handleModuleFile($remove, $files, $mainpath, $filename->data(), $modname, $title, $countsql);
			}
			if (!empty($this->modulekeys)) {
				$modulelist = implode("','", $this->modulekeys);
				$database->doSQL(sprintf($this->deleteSQL, '#__modules', 'module', $modulelist));
			}
		}

		$plugins = $manifest->getElementByPath('plugins');
		if (is_object($plugins)) foreach ($plugins->children() as $plugin) {
			$groupname = $plugin->attributes('group');
			$title = $plugin->attributes('title');
			$files = $plugin->getElementByPath('files');
			$names = $files->children();
			foreach ($names as $filename) {
				$plugname = $filename->attributes('plugin');
				$mainpath = JPATH_SITE.'/plugins/'.$groupname;
				$countsql = sprintf($this->countSQL, '#__plugins', 'element', $plugname);
				$this->handlePluginFile($remove, $files, $mainpath, $filename->data(), $plugname, $title, $groupname, $countsql);
			}
			if (!empty($this->pluginkeys)) {
				$pluginlist = implode("','", $this->pluginkeys);
				$database->doSQL(sprintf($this->deleteSQL, '#__plugins', 'element', $pluginlist));
			}
		}
	}
	
	protected function makeModule ($modname, $title, $id=1) {
		if ($id) return '';
		$clientId = 0;
		$row = JTable::getInstance('module');
		$row->title = $modname;
		$row->ordering = $row->getNextOrder( "position='left'" );
		$row->position = 'left';
		$row->showtitle = 1;
		$row->iscore = 0;
		$row->access = $clientId == 1 ? 2 : 0;
		$row->client_id = $clientId;
		$row->module = $modname;
		$row->published = 0;
		$row->params = '';
		if (!$row->store()) {
			// Install failed, roll back changes
			$this->jinstaller->abort($this->T_('Module').' '.$this->T_('Install').': '.$row->getDBO()->stderr(true));
			return $this->T_('Unable to write module information to database');
		}
	}

	protected function makePlugin ($plugname, $groupname, $title, $id=1) {
		if ($id) return '';
	    $row = JTable::getInstance('plugin');
	    $row->name = $title;
	    $row->ordering = 0;
	    $row->folder = $groupname;
	    $row->iscore = 0;
	    $row->access = 0;
	    $row->client_id = 0;
	    $row->element = $plugname;
	    $row->published = 1;
	    $row->params = '';

	    if (!$row->store()) {
	        // Install failed, roll back changes
	        $this->jinstaller->abort($this->T_('Plugin').' '.$this->T_('Install').': '.$row->getDBO()->stderr(true));
	        return $this->T_('Unable to write plugin information to database');
	    }
	}
	
	protected function makeJoomlaMenuEntry () {
		// Import JTableMenu
		JLoader::register('JTableMenu', JPATH_LIBRARIES . '/joomla/database/table/menu.php');
		$newmenu = new JTableMenu(JFactory::getDBO());
		$newmenu->menutype = 'mainmenu';
		$newmenu->name = $this->ctitle;
		$newmenu->alias = $this->calias;
		$newmenu->link = 'index.php?option='.$this->cname;
		$newmenu->type = 'component';
		$newmenu->published = 1;
		$property = $this->cidname;
		$newmenu->$property = $this->componentID;
		$newmenu->componentid = $this->componentID;
		$newmenu->ordering = $this->getMenuOrdering() + 1;
		$newmenu->store();
	}

	protected function getMenuOrdering () {
		$this->database->setQuery("SELECT MAX(ordering) FROM `#__menu` WHERE menutype = 'mainmenu'");
		return intval($this->database->loadResult());
	}

	protected function getComponentID () {
		$this->database->setQuery("SELECT MIN(id) FROM `#__components` WHERE `option` = '$this->cname'");
		return intval($this->database->loadResult());
	}
}

class Joomla25Installer extends anyJoomlaInstaller {
	protected static $infoformat = array(
		'legacy' => false,
		'name' => '',
		'type' => '',
		'creationDate' => '',
		'author' => '',
		'copyright' => '',
		'authorEmail' => '',
		'authorUrl' => '',
		'version' => '',
		'description' => '',
		'group' => ''
	);
	protected $cidname = 'component_id';

	protected $countSQL = "SELECT extension_id FROM #__extensions WHERE type = '%s' AND element = '%s'";
	protected $deleteSQL = "DELETE FROM #__extensions WHERE type = '%s' AND element IN ('%s')";
	protected $moduleSQL = "DELETE FROM #__modules WHERE module IN ('%s')";
	protected $info = array();
	
	public function installModulesPlugins ($remove=false) {
		// Set the installation path
		// For the time being handle only user side modules
		$manifest = $this->jinstaller->getManifest();
		$this->info = self::$infoformat;
		$alifest = new aliroXML();
		$alifest->loadElement($manifest);		
		foreach (array_keys($this->info) as $field) {
			$value = (string) $alifest->getXML($field);
			if ($value) $this->info[$field] = $value;
		}
		$mtype = 'user';
		$modules = $alifest->getXML('modules');
		$ROOT_PATH = 'admin' == $mtype ? JPATH_ADMINISTRATOR : JPATH_SITE;
		if (is_object($modules)) foreach ($modules->children() as $module) {
			$alimodule = new aliroXML();
			$alimodule->loadElement($module);
			$title = (string) $module['title'];
			$files = $alimodule->getXML('files');
			$names = $alimodule->getXML('files->filename');
			foreach ($names as $filename) {
				$modname = (string) $filename['module'];
				$mainpath = JPATH_SITE.'/modules/'.$modname;
				$countsql = sprintf($this->countSQL, 'module', $modname);
				$this->handleModuleFile($remove, $files, $mainpath, (string) $filename, $modname, $title, $countsql);
			}
		}
		if (!empty($this->modulekeys)) {
			$modulelist = implode("','", $this->modulekeys);
			$deletesql = sprintf($this->deleteSQL, 'module', $modulelist);
			$this->database->doSQL($deletesql);
			$deletesql = sprintf($this->moduleSQL, $modulelist);
			$this->database->doSQL($deletesql);
		}

		$plugins = $alifest->getXML('plugins');
		if (is_object($plugins)) foreach ($plugins->children() as $plugin) {
			$groupname = (string) $plugin['group'];
			$title = (string) $plugin['title'];
			$aliplugin = new aliroXML();
			$aliplugin->loadElement($plugin);
			$files = $aliplugin->getXML('files');
			$names = $aliplugin->getXML('files->filename');
			foreach ($names as $filename) {
				$plugname = (string) $filename['plugin'];
				$mainpath = JPATH_SITE.'/plugins/'.$groupname.'/'.$plugname;
				$countsql = sprintf($this->countSQL, 'plugin', $plugname);
				$this->handlePluginFile($remove, $files, $mainpath, (string) $filename, $plugname, $title, $groupname, $countsql);
			}
		}
		if (!empty($this->pluginkeys)) {
			$pluginlist = implode("','", $this->pluginkeys);
			$deletesql = sprintf($this->deleteSQL, 'plugin', $pluginlist);
			$this->database->doSQL($deletesql);
		}
	}

	protected function makeModule ($modname, $title, $id) {
		$clientId = 0;
		
		$row = JTable::getInstance('extension');
		if ($id) $row->load($id);
		$row->name = $title;
		$row->type = 'module';
		$row->ordering = 0;
		$row->access = 1;
		$row->client_id = $clientId;
		$row->element = $modname;
		$row->params = '';
        $row->enabled = 1;
		$this->info['type'] = 'module';
		$this->info['name'] = $title;
		$row->manifest_cache = json_encode($this->info);

		if (!$row->store()) {
			// Install failed, roll back changes
			$this->jinstaller->abort($this->T_('Module').' '.$this->T_('Install').': '.$row->getDBO()->stderr(true));
			return $this->T_('Unable to write module information to database');
		}
		
		if (!$id) {
			$row = JTable::getInstance('module');
			if ($id) $row->load($id);
			$row->title = $title;
			$row->ordering = 1;
			$row->showtitle = 1;
			$row->access = 1;
			$row->client_id = $clientId;
			$row->module = $modname;
			$row->params = '';
	        $row->published = 0;
			
			if (!$row->store()) {
				// Install failed, roll back changes
				$this->jinstaller->abort($this->T_('Module').' '.$this->T_('Install').': '.$row->getDBO()->stderr(true));
				return $this->T_('Unable to write module information to database');
			}
		}
	}
	
	protected function makePlugin ($plugname, $groupname, $title, $id) {
		$row = JTable::getInstance('extension');
		if ($id) $row->load($id);
		$row->name = $title;
		$row->type = 'plugin';
		$row->folder = $groupname;
		$row->access = 1;
		$row->element = $plugname;
		$row->params = '';
        $row->enabled = 1;
		$this->info['type'] = 'plugin';
		$this->info['name'] = $title;
		$row->manifest_cache = json_encode($this->info);

		if (!$row->store()) {
			// Install failed, roll back changes
			$this->jinstaller->abort($this->T_('Plugin').' '.$this->T_('Install').': '.$row->getDBO()->stderr(true));
			return $this->T_('Unable to write plugin information to database');
		}
	}

	protected function makeJoomlaMenuEntry () {
		// Import JTableMenu
		JLoader::register('JTableMenu', JPATH_LIBRARIES . '/joomla/database/table/menu.php');
		$newmenu = new JTableMenu(JFactory::getDBO());
		$newmenu->menutype = 'mainmenu';
		$newmenu->title = $this->ctitle;
		$newmenu->alias = $this->calias;
		$newmenu->link = 'index.php?option='.$this->cname;
		$newmenu->type = 'component';
		$newmenu->published = 1;
		$property = $this->cidname;
		$newmenu->$property = $this->componentID;
		$newmenu->parent_id = 1;
		$newmenu->level = 1;
		$newmenu->language = '*';
		$newmenu->component_id = $this->componentID;
		$newmenu->setLocation($this->getMenuOrdering());
		$newmenu->store();
	}

	protected function getMenuOrdering () {
		$this->database->setQuery("SELECT MAX(id) FROM `#__menu` WHERE menutype = 'mainmenu'");
		return intval($this->database->loadResult());
	}

	protected function getComponentID () {
		$this->database->setQuery("SELECT MIN(extension_id) FROM `#__extensions` WHERE `element` = '$this->cname'");	
		return intval($this->database->loadResult());
	}
}

if (defined('_CMSAPI_JOOMLA_VERSION') AND '2.5' == _CMSAPI_JOOMLA_VERSION) {
	class cmsapiInstaller extends Joomla25Installer {}
}
else {
	class cmsapiInstaller extends Joomla15Installer {}
}