<?php
/**
 * Plugin for Joomla 1.5 - Version 1.5.1
 * License: http://www.gnu.org/copyleft/gpl.html
 * Authors: marco maria leoni
 * Copyright (c) 2010 marco maria leoni web consulting - http: www.mmleoni.net
 * Copyright (C) 2005 - 2010 Open Source Matters. All rights reserved.
 * http://www.mmleoni.net/extended-debug-for-joomla-with-ip-restriction
 * *** Last update: Sep, 5th 2010 ***
*/

/**
 * Adapted and integrated into Jaliro
 *
 */

// CMS independent check to prevent direct execution
if (basename(@$_SERVER['REQUEST_URI']) == basename(__FILE__)) die ('This software is for use within a larger system');

class cmsapiDebug {
	private $params = null;

	public function __construct ($params) {
		$this->params = $params;
	}

	public function debugOutput () {
		global $database;

		jimport('joomla.error.profiler');
		$profiler = JProfiler::getInstance( 'Application' );

		ob_start();
		echo '<div id="system-debug" class="profiler">';

		// Display General Warning
		echo '<div id="system-debug-general-warning" class="system-debug-block">';
		echo '<h4>'.JText::_( 'General Warning' ).'</h4>';
		if ($this->params->get('limit_to_ip', '')) {
			echo "Debug info sent to: " . $this->params->get('limit_to_ip');
		}else{
			echo '<span class="system-debug-alert">Attention: Debug info sent to All!!</span>';
		}
		echo '</div>';



		// Display remote client Information
		if ($this->params->get('remote_client', 0)) {
			echo '<div id="system-debug-remote-client" class="system-debug-block">';
			echo '<h4>'.JText::_( 'Client Information' ).'</h4>';
			echo '<ul>';
			echo "<li>REMOTE_ADDR    :{$_SERVER['REMOTE_ADDR']}</li>";
			echo "<li>HTTP_USER_AGENT: {$_SERVER['HTTP_USER_AGENT']}</li>";
			echo "<li>REQUEST_METHOD : {$_SERVER['REQUEST_METHOD']}</li>";
			echo "<li>QUERY_STRING   : {$_SERVER['QUERY_STRING']}</li>";
			echo "<li>HTTP_REFERER   :{$_SERVER['HTTP_REFERER']}</li>";
			echo '</ul>';
			echo '</div>';
		}

		// Display GPC super globals Information
		if ($this->params->get('namespaces', 'GET,POST') != 'NONE') {
			echo '<div id="system-debug-namespaces" class="system-debug-block">';
			echo '<h4>'.JText::_( 'GPC Namespaces' ).'</h4>';


			$wr=array();
			foreach(explode(',', $this->params->get('namespaces', 'GET,POST' )) as $nsp){
				switch ($nsp){
					case 'GET':
						$nameSpace = $_GET;
						break;
					case 'POST':
						$nameSpace = $_POST;
						break;
					case 'COOKIE':
						$nameSpace = $_COOKIE;
						break;
					case 'REQUEST':
						$nameSpace = $_REQUEST;
						break;
				}
				echo '<h5>'.JText::_( 'Namespace: $_' ).$nsp.'</h5>';
				echo '<ul>';
				$i=0;
				foreach($nameSpace as $k => &$v){
					echo "<li>$k: <pre class=\"system-debug-row-" . ($i++ % 2) . "\">";
					var_dump ($v);
					echo '</pre></li>';
				} // namespace
				echo '</ul>';
			} //namespaces
			echo '</div>';
		}


		// Display $_SESSION vars Information
		if ($this->params->get('display_session', '1')) {
			echo '<div id="system-debug-session" class="system-debug-block">';
			echo '<h4>'.JText::_( 'Sessions Namespace' ).'</h4>';
			echo '<ul>';
			$i=0;
			foreach($_SESSION as $k => &$v){
				echo "<li>$k: <pre class=\"system-debug-row-" . ($i++ % 2) . "\">";
				var_dump ($v);
				echo '</pre></li>';
			} // namespace
			echo '</ul>';
			echo '</div>';
		}


		// original "System - Debug" code: non si inventa l'acqua calda!
		// (Italian idiomatic phrase: try to reinvent warm water (the wheel) and waste time ;) )
		// some classes/id added


		if ($this->params->get('profile', 1)) {
			echo '<div id="system-debug-profile" class="system-debug-block">';
			echo '<h4>'.JText::_( 'Profile Information' ).'</h4>';
			foreach ( $profiler->getBuffer() as $mark ) {
				echo '<div>'.$mark.'</div>';
			}
			echo '</div>';
		}

		if ($this->params->get('memory', 1)) {
			echo '<div id="system-debug-memory" class="system-debug-block">';
			echo '<h4>'.JText::_( 'Memory Usage' ).'</h4>';
			echo $profiler->getMemory();
			echo '</div>';
		}

		if ($this->params->get('queries', 1))
		{
			jimport('geshi.geshi');

			$geshi = new GeSHi( '', 'sql' );
			$geshi->set_header_type(GESHI_HEADER_DIV);
			//$geshi->enable_line_numbers( GESHI_FANCY_LINE_NONE );

			$newlineKeywords = '/<span class="system-debug-queries-keys">'
				.'(FROM|LEFT|INNER|OUTER|WHERE|SET|VALUES|ORDER|GROUP|HAVING|LIMIT|ON|AND)'
				.'<\\/span>/i'
			;

			$db	= JFactory::getDBO();

			echo '<div id="system-debug-queries" class="system-debug-block">';
			echo '<h4>'.JText::sprintf( 'Queries logged',  $db->getTicker() ).'</h4>';

			if ($log = $db->getLog())
			{
				echo '<ol>';
				$i=0;
				foreach ($log as $k=>$sql)				{
					$geshi->set_source($sql);
					$text = $geshi->parse_code();
					$text = preg_replace($newlineKeywords, '<br />&nbsp;&nbsp;\\0', $text);
					echo '<li class="system-debug-row-' . ($i++ % 2) . '">'.$text.'</li>';
				}
				echo '</ol>';
			}

			if(isset($database))
			{
				echo '<h4>'.JText::sprintf( 'Legacy Queries logged',  $database->getTicker() ).'</h4>';
				echo '<ol>';
					$i=0;
					foreach ($database->getLog() as $k=>$sql){
						$geshi->set_source($sql);
						$text = $geshi->parse_code();
						$text = preg_replace($newlineKeywords, '<br />&nbsp;&nbsp;\\0', $text);
						echo '<li class="system-debug-row-' . ($i++ % 2) . '">'.$text.'</li>';
					}

				echo '</ol>';
			}
			echo '</div>';
		}

		$lang = &JFactory::getLanguage();
		if ($this->params->get('language_files', 1))
		{
			echo '<div id="system-debug-languages" class="system-debug-block">';
			echo '<h4>'.JText::_( 'Language Files Loaded' ).'</h4>';
			echo '<ul>';
			$extensions	= $lang->getPaths();
			foreach ( $extensions as $extension => $files)
			{
				foreach ( $files as $file => $status )
				{
					echo "<li>$file $status</li>";
				}
			}
			echo '</ul>';
			echo '</div>';
		}

		$langStrings = $this->params->get('language_strings', -1);
		if ($langStrings < 0 OR $langStrings == 1) {
			echo '<div id="system-debug-untraslated" class="system-debug-block">';
			echo '<h4>'.JText::_( 'Untranslated Strings Diagnostic' ).'</h4>';
			echo '<pre>';
			$orphans = $lang->getOrphans();
			if (count( $orphans ))
			{
				ksort( $orphans, SORT_STRING );
				foreach ($orphans as $key => $occurance) {
					foreach ( $occurance as $i => $info) {
						$class	= @$info['class'];
						$func	= @$info['function'];
						$file	= @$info['file'];
						$line	= @$info['line'];
						echo strtoupper( $key )."\t$class::$func()\t[$file:$line]\n";
					}
				}
			}
			else {
				echo JText::_( 'None' );
			}
			echo '</pre>';
		}
		if ($langStrings < 0 OR $langStrings == 2) {
			echo '<h4>'.JText::_( 'Untranslated Strings Designer' ).'</h4>';
			echo '<pre>';
			$orphans = $lang->getOrphans();
			if (count( $orphans ))
			{
				ksort( $orphans, SORT_STRING );
				$guesses = array();
				foreach ($orphans as $key => $occurance) {
					if (is_array( $occurance ) AND isset( $occurance[0] )) {
						$info = &$occurance[0];
						$file = @$info['file'];
						if (!isset( $guesses[$file] )) {
							$guesses[$file] = array();
						}

						$guess = str_replace( '_', ' ', $info['string'] );
						if ($strip = $this->params->get('language_prefix')) {
							$guess = trim( preg_replace( chr(1).'^'.$strip.chr(1), '', $guess ) );
						}
						$guesses[$file][] = trim( strtoupper( $key ) ).'='.$guess;
					}
				}
				foreach ($guesses as $file => $keys) {
					echo "\n\n# ".($file ? $file : JText::_( 'Unknown file' ))."\n\n";
					echo implode( "\n", $keys );
				}
			}
			else {
				echo JText::_( 'None' );
			}
			echo '</pre>';
			echo '</div>';
		}
		echo '</div>';

		$debug = ob_get_clean();

		$body = JResponse::getBody();
		$body = str_replace('</body>', $debug.'</body>', $body);
		JResponse::setBody($body);
	}
}