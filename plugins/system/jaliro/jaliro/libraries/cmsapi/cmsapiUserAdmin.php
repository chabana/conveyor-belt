<?php

/**************************************************************
* This file is part of Jaliro
* Copyright (c) 2008-10 Martin Brampton
* Issued as open source under GNU/GPL
* For support and other information, visit http://remository.com
* To contact Martin Brampton, write to martin@remository.com
*
* Please see jaliro.php for more details
*/

// CMS independent check to prevent direct execution
if (basename(@$_SERVER['REQUEST_URI']) == basename(__FILE__)) die ('This software is for use within a larger system');

abstract class cmsapiUserAdmin {
	protected $interface = null;

	public function __construct ($control_name, $alternatives, $default, $title, $cname) {
		$shortcname = false === strpos($cname, 'com_') ? $cname : substr($cname, 4);
		$interclass = $shortcname.'Interface';
		$this->interface = call_user_func(array($interclass, 'getInstance'));
		$func = $this->interface->getParam ($_REQUEST, $control_name, $default);
		if ('dbjson' == $func AND $this->saveDatabaseJSON()) return;
		if (isset($alternatives[$func])) $method = $alternatives[$func];
		else $method = $func;
		$qual_method = $shortcname.'_'.$method;
		$classname = $qual_method.'_Controller';
		$no_html = $this->interface->getParam($_REQUEST, 'no_html', 0);
		if (class_exists($classname)) {
			$controller = new $classname($this);
			$title = $this->getTitle();
			if (method_exists($controller,$qual_method)) {
				$this->interface->SetPageTitle($title);
				if (!$no_html) {
					echo "\n<!-- Start of $title HTML -->";
					echo "\n<div id='$shortcname'>";
				}
				$controller->$qual_method($func);
				if (!$no_html) {
					echo "\n</div>";
					echo "\n<!-- End of $title HTML -->";
				}
			}
			else {
				header ('HTTP/1.1 404 Not Found');
				trigger_error("Component $title error: attempt to use non-existent method $qual_method in $controller");
			}
		}
		else {
			header ('HTTP/1.1 404 Not Found');
			trigger_error("Component $title error: attempt to use non-existent class $classname");
		}
	}

	protected function saveDatabaseJSON () {
		if ($this->interface->getUser()->isAdmin()) {
			$manager = new aliroDatabaseManager();
			$json = $manager->getTablesAsJSON ($this->tables, true);
			$database = $this->interface->getDB();
			$creators = "	protected static \$dbcreators = array(\n";
			foreach ($this->tables as $table) {
				$database->setQuery("SHOW CREATE TABLE $table");
				$result = $database->loadAssoc();
				$creators .= "'$table' => \"{$result['Create Table']}\",\n";
			}
			$creators .= ");\n";
			if (file_put_contents(_CMSAPI_ABSOLUTE_PATH.$this->componentpath.'/stored.sql', $creators)) {
				echo "<br />Database creation info has been placed in $this->componentpath/stored.sql";
			}
			if (file_put_contents(_CMSAPI_ABSOLUTE_PATH.$this->componentpath.'/stored.json', $json)) {
				echo "<br />Database structure info has been placed in $this->componentpath/stored.json";
				return true;
			}
		}
		return false;
	}
}