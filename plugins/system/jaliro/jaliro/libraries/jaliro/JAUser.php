<?php

/* **********************************************************************
* This file is a generic interface to Joomla 3.0+ with extra capabilities
* Copyright (c) 2012 Martin Brampton
* Issued as open source under GNU/GPL
* For support and other information, visit http://aliro.org
* To contact Martin Brampton, write to counterpoint@aliro.org
*
*/

class JAUser extends JUser {
	
	public function isLogged () {
		return 0 != $this->id;
	}
	
	public function isAdmin () {
		return $this->authorise('core.admin');
	}
	
	public function isUser () {
		if ($this->isAdmin()) return false;
		return $this->isLogged();
	}

	public function sendMailFrom ($emailto, $subject, $text='') {
		if ($this->email) return JUTility::sendMail($this->email, $this->name, $emailto, $subject, $text, null, null, null, null, null, null);
		else return false;
	}

	public function sendMailTo ($subject, $text='', $emailfrom='', $fromname='', $file=null) {
		if (!$emailfrom) {
			$repository = remositoryRepository::getInstance();
			if ($repository->Sub_Mail_Alt_Addr) {
				$emailfrom = $repository->Sub_Mail_Alt_Addr;
				$fromname = $repository->Sub_Mail_Alt_Name;
			}
			else {
				$interface = remositoryAbstract::getInterface();
				if ($interface->getCfg('Mailfrom')) {
					$emailfrom = $interface->getCfg('Mailfrom');
					$fromname = $interface->getCfg('Fromname');
				}
				else {
					$emailfrom = self::superAdminMail();
					$fromname = '';
				}
			}
		}
		$transformer = array(
			'{toname}' => $this->name,
			'{touser}' => $this->username,
			'{toemail}' => $this->email,
			'{fromname}' => $fromname,
			'{fromemail}' => $emailfrom
		);
		if (is_object($file)) {
			$filetransform = array(
				'{fileversion}' => $file->fileversion
			);
			$transformer = array_merge($transformer, $filetransform);
		}
		$text = str_replace(array_keys($transformer), array_values($transformer), $text);
		if ($email) return JUTility::sendMail($emailfrom, $fromname, $this->email, $subject, $text, null, null, null, null, null, null);
		else return false;
	}

	public static function find ($id) {
		
	}
	
	public static function superAdminMail () {
		$database = remositoryAbstract::getDB();
		$sql="select email from #__users where usertype='superadministrator' or usertype='super administrator'";
		$database->setQuery( $sql );
		return $database->loadResult();
	}
}