<?php
 /**
 * Glossarbot Plugin 1.1.8 for Joomla 1.5.x
 * Support site: http://www.remository.com
 * All rights reserved
 * Released under GNU/GPL License Version 2: http://www.gnu.org/copyleft/gpl.html
 * 
 * @by Martin Brampton (martin@remository.com)
 * @Copyright (C)2009 Martin Brampton
 * 
 * This code was originally developed by Martin Brampton from partially written code by Sascha Claren
 * 
 * It was then taken to form the plugin for "Definitions" by Stefan Granholm and published in slightly
 * modified form without any attribution, but the following line:
 *
 * Definition file - By GranholmCMS.com - www.granholmcms.com
 * 
 * The latest Glossarbot for Joomla 1.5.x created some problems because of the attempt to shift to 
 * using mootools to generate the popup.  To quickly solve those problems, this file is being released
 * by Martin Brampton.  It contains some changes commissioned by Stefan Granholm, but is mainly a 
 * simple adaptation of the original Glossarbot code, taking account of differences in Joomla 1.5.
 * 
 * The basis for releasing in this way is that since Glossarbot was released as GPL, it follows that
 * Definitions (being a derived work) must also have been released as GPL.  It is therefore legitimate
 * to base another version of Glossarbot on Definitions, by the rights granted in the GPL.
 * 
 **/
 
defined( '_VALID_MOS' ) OR defined ( '_JEXEC' ) OR die( 'Direct Access to this location is not allowed.' );

// Define CMS environment
if (defined('_CMSAPI_ABSOLUTE_PATH') AND is_readable($gclasspath = _CMSAPI_ABSOLUTE_PATH.'/components/com_glossary/glossary.class.php')) {
	require_once($gclasspath);
}
else return;
// End of CMS environment definition

if ('Joomla' == _CMSAPI_CMS_BASE) {
	if(version_compare(JVERSION,'1.6.0','<')){
		jimport( 'joomla.event.plugin' );

		class plgContentGlossaryPopups extends JPlugin  {
			protected $plugin_type = 'content';
			
			// This is not required if there is nothing to be done in the constructor
			// public function plgContentGlossaryPopups ($params) {
			//	parent::__construct( $params );
			// }
			
			public function onPrepareContent ($article) {
			 	$worker = new glossary_plugin_content($this->params);
			 	return $worker->onPrepareContent ($article);
			}
		}
	}
	else {
		class plgContentGlossaryPopups extends JPlugin  {
			protected $plugin_type = 'content';
			
			public function __construct($subject, $config = array()){
				parent::__construct($subject, $config);
				$this->loadLanguage();
			}
			
			public function onContentPrepare ($context, $article, $params, $page = 0) {
				$worker = new glossary_plugin_content($this->params);
				return $worker->onPrepareContent ($article);
			}
		}
	}
}

else {
	global $_MAMBOTS;
	/** Register search function inside Mambo's API */
	$_MAMBOTS->registerFunction( 'onPrepareContent', 'botContentGlossary' );

	function botContentGlossary ($article) {
	 	$worker = new glossary_plugin_content();
		// Get parameters from Database
		$query = "SELECT params FROM #__mambots WHERE element = 'glossaryPopups' AND folder = 'content'";
		$database = glossaryInterface::getInstance()->getDB('glossary');
		$database->setQuery($query);
		$paramstring = $database->loadResult();
		$params = new mosParameters($paramstring);
	 	return $worker->onSearch ($article);
	}
}