<?php
/*
# ------------------------------------------------------------------------
# Extensions for Joomla 2.5.x - Joomla 3.x
# ------------------------------------------------------------------------
# Copyright (C) 2011-2014 Ext-Joom.com. All Rights Reserved.
# @license - PHP files are GNU/GPL V2.
# Author: Ext-Joom.com
# Websites:  http://www.ext-joom.com 
# Date modified: 07/12/2013 - 13:00
# ------------------------------------------------------------------------
*/

// no direct access
defined('_JEXEC') or die ;
if(!defined('DS')){
	define('DS',DIRECTORY_SEPARATOR);
}

$document 					= JFactory::getDocument();
$document->addStyleSheet(JURI::base() . 'modules/mod_ext_bxslider_k2_content/assets/css/jquery.bxslider.css');


$moduleclass_sfx			= $params->get('moduleclass_sfx');
$ext_generate_id			=(int)$params->get('ext_generate_id', 1);
$ext_id						= $params->get('ext_id', '1');
if ($ext_generate_id == 1) {
	$rand1 = rand(1,100);
	$rand2 = rand(1,100);
	$ext_id = $rand1.$rand2;
}

// Load jQuery
//------------------------------------------------------------------------

$ext_jquery_ver				= $params->get('ext_jquery_ver', '1.9.1');
$ext_load_jquery			= (int)$params->get('ext_load_jquery', 1);
$ext_load_base				= (int)$params->get('ext_load_base', 1);
$ext_load_easing			= (int)$params->get('ext_load_easing', 1);
$ext_load_bxslider			= (int)$params->get('ext_load_bxslider', 1);

$ext_script = <<<SCRIPT


var jQ = false;
function initJQ() {
	if (typeof(jQuery) == 'undefined') {
		if (!jQ) {
			jQ = true;
			document.write('<scr' + 'ipt type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/$ext_jquery_ver/jquery.min.js"></scr' + 'ipt>');
		}
		setTimeout('initJQ()', 50);
	}
}
initJQ(); 
 
 if (jQuery) jQuery.noConflict();    
  
  
 

SCRIPT;

if ($ext_load_jquery  > 0) {
	$document->addScriptDeclaration($ext_script);		
}
if ($ext_load_easing  > 0) { 
	$document->addCustomTag('<script type = "text/javascript" src = "'.JURI::root().'modules/mod_ext_bxslider_k2_content/assets/js/jquery.easing.1.3.js"></script>'); 
}
if ($ext_load_bxslider > 0) {	
	$document->addCustomTag('<script type = "text/javascript" src = "'.JURI::root().'modules/mod_ext_bxslider_k2_content/assets/js/jquery.bxslider.min.js"></script>');
}





// Options bxSlider - http://bxslider.com/options
//------------------------------------------------------------------------

$ext_mode					= $params->get('ext_mode', 'horizontal');
$ext_speed					= (int)$params->get('ext_speed', 500);
$ext_controls				= $params->get('ext_controls', 'true');
$ext_auto					= $params->get('ext_auto', 'false');
$ext_autohover				= $params->get('ext_autohover', 'false');
$ext_pause					= (int)$params->get('ext_pause', 3000);
$ext_auto_controls			= $params->get('ext_auto_controls', 'false');
$ext_auto_delay				= (int)$params->get('ext_auto_delay', 0);
$ext_pager					= $params->get('ext_pager', 'true');
$ext_pager_type 			= $params->get('ext_pager_type', 'full');
$ext_pager_saparator		= $params->get('ext_pager_saparator', '/');
$ext_min_slides				= (int)$params->get('ext_min_slides', 1);
$ext_max_slides				= (int)$params->get('ext_max_slides', 5);
$ext_slide_width			= (int)$params->get('ext_slide_width', 200);
$ext_slide_margin			= (int)$params->get('ext_slide_margin', 10);
$ext_adaptive_height		= $params->get('ext_adaptive_height', 'false');
$ext_adaptive_height_speed	= (int)$params->get('ext_adaptive_height_speed', 500);
$ext_easing					= $params->get('ext_easing', 'null');
$ext_random_start			= $params->get('ext_random_start', 'false');


// K2
//------------------------------------------------------------------------

if (K2_JVERSION != '15')
{
    $language = JFactory::getLanguage();
    $language->load('mod_k2.j16', JPATH_ADMINISTRATOR, null, true);
}
require_once (dirname(__FILE__).DS.'helper.php');

// Get component params
$componentParams = JComponentHelper::getParams('com_k2');

$items = modK2bxSliderContentHelper::getItems($params);
if (count($items)){
	require JModuleHelper::getLayoutPath('mod_ext_bxslider_k2_content', $params->get('layout', 'default'));
}