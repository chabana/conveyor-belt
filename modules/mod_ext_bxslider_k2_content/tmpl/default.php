<?php
/*
# ------------------------------------------------------------------------
# Extensions for Joomla 2.5.x - Joomla 3.x
# ------------------------------------------------------------------------
# Copyright (C) 2011-2014 Ext-Joom.com. All Rights Reserved.
# @license - PHP files are GNU/GPL V2.
# Author: Ext-Joom.com
# Websites:  http://www.ext-joom.com 
# Date modified: 07/12/2013 - 13:00
# ------------------------------------------------------------------------
*/

// no direct access
defined('_JEXEC') or die;
if(!defined('DS')){
	define('DS',DIRECTORY_SEPARATOR);
}

$ext_class_selector = 'horizontal';
if ($ext_mode == 'vertical') {
	$ext_class_selector = 'vertical';
} 
?>


<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery('#mod_ext_bxslider_k2_content_<?php echo $ext_id; ?>').bxSlider({
		mode: '<?php echo $ext_mode;?>',		
		randomStart: <?php echo $ext_random_start;?>,
		minSlides: <?php echo $ext_min_slides;?>,
		maxSlides: <?php echo $ext_max_slides;?>,
		slideWidth: <?php echo $ext_slide_width;?>,
		slideMargin: <?php echo $ext_slide_margin;?>,
		adaptiveHeight: <?php echo $ext_adaptive_height;?>,
		adaptiveHeightSpeed: <?php echo $ext_adaptive_height_speed;?>,
		easing:	'<?php echo $ext_easing;?>',
		speed: <?php echo $ext_speed;?>,
		controls: <?php echo $ext_controls; ?>,
		auto: <?php echo $ext_auto;?>,
		autoControls: <?php echo $ext_auto_controls;?>,
		pause: <?php echo $ext_pause?>,
		autoDelay: <?php echo $ext_auto_delay; ?>,
		autoHover: <?php echo $ext_autohover; ?>,
		pager: <?php echo $ext_pager;?>,
		pagerType: '<?php echo $ext_pager_type;?>',
		pagerShortSeparator: '<?php echo $ext_pager_saparator;?>'
	});
});
</script>


<div class="mod_ext_bxslider_k2_content mod_ext_bxslider_k2_content_controls_<?php echo $ext_class_selector;?> <?php echo $moduleclass_sfx ?>">	
	
	<ul id="mod_ext_bxslider_k2_content_<?php echo $ext_id; ?>" >	
	<?php if(count($items)): ?>
		<?php foreach ($items as $key=>$item):	?>
		<li>


		    <?php if($params->get('itemImage') || $params->get('itemIntroText')): ?>
			
			    <?php if($params->get('itemImage') && isset($item->image)): ?>
				<div class="ext-itemimage">
					<a class="ext-moduleitemimage" href="<?php echo $item->link; ?>" title="<?php echo JText::_('K2_CONTINUE_READING'); ?> &quot;<?php echo K2HelperUtilities::cleanHtml($item->title); ?>&quot;">
						<img src="<?php echo $item->image; ?>" alt="<?php echo K2HelperUtilities::cleanHtml($item->title); ?>"/>
					</a>
				</div>
			    <?php endif; ?>

				<?php if($params->get('itemIntroText')): ?>
				<div class="ext-itemintrotext"><?php echo $item->introtext; ?></div>
				<?php endif; ?>
		  
			<?php endif; ?>

			<?php if($params->get('itemExtraFields') && count($item->extra_fields)): ?>
			<div class="ext-moduleitemextrafields">
				<b><?php echo JText::_('K2_ADDITIONAL_INFO'); ?></b>
				<ul>
					<?php foreach ($item->extra_fields as $extraField): ?>
							<?php if($extraField->value != ''): ?>
							<li class="type<?php echo ucfirst($extraField->type); ?> group<?php echo $extraField->group; ?>">
								<?php if($extraField->type == 'header'): ?>
								<h4 class="moduleItemExtraFieldsHeader"><?php echo $extraField->name; ?></h4>
								<?php else: ?>
								<span class="moduleItemExtraFieldsLabel"><?php echo $extraField->name; ?></span>
								<span class="moduleItemExtraFieldsValue"><?php echo $extraField->value; ?></span>
								<?php endif; ?>
								<div class="clr"></div>
							</li>
							<?php endif; ?>
					<?php endforeach; ?>
				</ul>
			</div>
			<?php endif; ?>
 

			<?php if($params->get('itemCategory')): ?>
			<?php echo JText::_('K2_IN') ; ?> <a class="moduleItemCategory" href="<?php echo $item->categoryLink; ?>"><?php echo $item->categoryname; ?></a>
			<?php endif; ?>

		 
			<?php if($params->get('itemReadMore') && $item->fulltext): ?>
			<a class="moduleItemReadMore" href="<?php echo $item->link; ?>">
				<?php echo JText::_('K2_READ_MORE'); ?>
			</a>
			<?php endif; ?>
	  
      <?php if($params->get('itemTitle')): ?>
			<div class="ext-itemtitle">
				<?php echo $item->title; ?>
			</div>
			<?php endif; ?>
    
		</li>
		<?php endforeach; ?>
	<?php endif; ?>	
	</ul>	
	
	<div style="clear:both;"></div>
</div>
