<?php
/**
* FileName: mod_glossaryNewest.php
* Date: April 2009
* License: GNU General Public License
* Script Version #: 2.8
* Glossary Version #: 2.8 or above
* Author: Martin Brampton - martin@remository.com (http://remository.com)
* Copyright: Martin Brampton 2006-10
**/

defined( '_VALID_MOS' ) OR defined ( '_JEXEC' ) OR die( 'Direct Access to this location is not allowed.' );

// Define CMS environment
if (defined('_CMSAPI_ABSOLUTE_PATH') AND is_readable($gclasspath = _CMSAPI_ABSOLUTE_PATH.'/components/com_glossary/glossary.class.php')) {
	require_once($gclasspath);
}
else return;
// End of CMS environment definition

$mod_glossaryNewest = new mod_glossaryNewest();
$mod_glossaryNewest->showFileList(null, $content, null, $params);