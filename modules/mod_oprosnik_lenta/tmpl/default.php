<?php 
defined('_JEXEC') or die('Restricted access');

JHtml::_('stylesheet', 'mod_oprosnik_lenta/template.css', array(), true);
jimport( 'joomla.application.application' );
jimport('joomla.utilities.utility');

JHtml::_('behavior.formvalidation');



?>

<script type="application/javascript" language="javascript">

jQuery.noConflict();
jQuery(document).ready(function(){

/*if (jQuery("input#check_a[type='radio']:checked")) jQuery('input.inp_a').css('visibility','visible');
if (!jQuery("input#check_a[type='radio']:checked")) jQuery('input.inp_a').css('visibility','hidden');
*/

jQuery('input.risunok').live('change', function() {

var id = "";
id = jQuery(":radio[name=check_risunok]").filter(":checked").attr("id");

switch (id)
{
case "check_a": jQuery('input.inp_a').css('visibility','visible');
 jQuery('input.inp_b,input.inp_c,input.inp_d,input.inp_e,input.inp_f,input.inp_g').css('visibility','hidden');
	break;
 
 case "check_b": jQuery('input.inp_b').css('visibility','visible');
 jQuery('input.inp_a,input.inp_c,input.inp_d,input.inp_e,input.inp_f,input.inp_g').css('visibility','hidden');
	break;

 case "check_c": jQuery('input.inp_c').css('visibility','visible');
 jQuery('input.inp_a,input.inp_b,input.inp_d,input.inp_e,input.inp_f,input.inp_g').css('visibility','hidden');
	break;
 
  case "check_d": jQuery('input.inp_d').css('visibility','visible');
 jQuery('input.inp_a,input.inp_b,input.inp_c,input.inp_e,input.inp_f,input.inp_g').css('visibility','hidden');
	break;
 
   case "check_e": jQuery('input.inp_e').css('visibility','visible');
 jQuery('input.inp_a,input.inp_b,input.inp_c,input.inp_d,input.inp_f,input.inp_g').css('visibility','hidden');
	break;
 
    case "check_f": jQuery('input.inp_f').css('visibility','visible');
 jQuery('input.inp_a,input.inp_b,input.inp_c,input.inp_d,input.inp_e,input.inp_g').css('visibility','hidden');
	break;
 
     case "check_g": jQuery('input.inp_g').css('visibility','visible');
 jQuery('input.inp_a,input.inp_b,input.inp_c,input.inp_d,input.inp_e,input.inp_f').css('visibility','hidden');
	break;
}



});

jQuery('input.shevron').live('change', function() {
id = jQuery(":radio[name=check_shevron]").filter(":checked").attr("id");

				
switch (id)
{
case "check_h": jQuery('input.inp_h').css('visibility','visible');
 jQuery('input.inp_i,input.inp_h1,input.inp_h2,input.inp_h3,input.inp_h4,input.inp_h5,input.inp_h6,input.inp_h7,input.inp_h8').css('visibility','hidden');
	break;
 
 case "check_i": jQuery('input.inp_i').css('visibility','visible');
 jQuery('input.inp_h,input.inp_h1,input.inp_h2,input.inp_h3,input.inp_h4,input.inp_h5,input.inp_h6,input.inp_h7,input.inp_h8').css('visibility','hidden');
	break;

 case "check_h1": jQuery('input.inp_h1').css('visibility','visible');
 jQuery('input.inp_h,input.inp_i,input.inp_h2,input.inp_h3,input.inp_h4,input.inp_h5,input.inp_h6,input.inp_h7,input.inp_h8').css('visibility','hidden');
	break;
	
 case "check_h1": jQuery('input.inp_h2').css('visibility','visible');
 jQuery('input.inp_h,input.inp_h1,input.inp_i,input.inp_h3,input.inp_h4,input.inp_h5,input.inp_h6,input.inp_h7,input.inp_h8').css('visibility','hidden');
	break;
	
 case "check_h1": jQuery('input.inp_h3').css('visibility','visible');
 jQuery('input.inp_h,input.inp_i,input.inp_h2,input.inp_h1,input.inp_h4,input.inp_h5,input.inp_h6,input.inp_h7,input.inp_h8').css('visibility','hidden');
	break;
	
 case "check_h1": jQuery('input.inp_h4').css('visibility','visible');
 jQuery('input.inp_h,input.inp_i,input.inp_h2,input.inp_h3,input.inp_h1,input.inp_h5,input.inp_h6,input.inp_h7,input.inp_h8').css('visibility','hidden');
	break;
	
 case "check_h1": jQuery('input.inp_h5').css('visibility','visible');
 jQuery('input.inp_h,input.inp_i,input.inp_h2,input.inp_h3,input.inp_h4,input.inp_h1,input.inp_h6,input.inp_h7,input.inp_h8').css('visibility','hidden');
	break;
	
 case "check_h1": jQuery('input.inp_h6').css('visibility','visible');
 jQuery('input.inp_h,input.inp_i,input.inp_h2,input.inp_h3,input.inp_h4,input.inp_h5,input.inp_h1,input.inp_h7,input.inp_h8').css('visibility','hidden');
	break;
	
 case "check_h1": jQuery('input.inp_h7').css('visibility','visible');
 jQuery('input.inp_h,input.inp_i,input.inp_h2,input.inp_h3,input.inp_h4,input.inp_h5,input.inp_h6,input.inp_h1,input.inp_h8').css('visibility','hidden');
	break;
	
 case "check_h1": jQuery('input.inp_h8').css('visibility','visible');
 jQuery('input.inp_h,input.inp_i,input.inp_h2,input.inp_h3,input.inp_h4,input.inp_h5,input.inp_h6,input.inp_h7,input.inp_h1').css('visibility','hidden');
	break;	
	
 default: break;
}
});

							
});


jQuery(document).ready(function(){
											
jQuery("input[type=text]").keypress(function(event) {
        if(event.keyCode == 13) { 
        textboxes = jQuery("input[type=text]");
        debugger;
        currentBoxNumber = textboxes.index(this);
        if (textboxes[currentBoxNumber + 1] != null) {
            nextBox = textboxes[currentBoxNumber + 1]
            nextBox.focus();
            nextBox.select();
            event.preventDefault();
            return false 
            }
        }
    });
});


//Скрипт на валидацию форм. Заполнено поле или нет.
function myValidate(f) {

	if (!jQuery(":radio[name=check_risunok]").filter(":checked").attr("id"))
	{ alert ('Выберите рисунок рифления');  jQuery('#check_a').focus(); return false;}
	
	if (!jQuery(":radio[name=check_shevron]").filter(":checked").attr("id"))
	{ alert ('Выберите тип и параметры шеврона');  jQuery('#check_h').focus(); return false;}
	
	if (!jQuery(":radio[name=lenta_zamknuta]").filter(":checked").attr("id"))
	{ alert ('Выберите замкнутость ленты в кольцо');  jQuery('#lenta_zamknuta').focus(); return false;}
	
	if (f.cfio.value == '' )	
	{ alert ('Введите ФИО'); jQuery('#cfio').focus(); return false;}
	else if (f.cphone.value == '' )
	{ alert ('Введите номер контактного телефона'); jQuery('#cphone').focus(); return false;}
	else return true;
 
}
</script>

<?php

$session =& JFactory::getSession();


/*$session->set('scounter','0');
$scounter = $session->get('scounter');

echo $scounter.'!!!';*/

$url = JURI::current();
$url = htmlentities($url, ENT_COMPAT, "UTF-8");

$pr_email = JRequest::getVar('pr_email', 0, 'post');
$attachment_email = JRequest::getVar('attachment_email', 0, 'post');

function is_email($email) {
  return preg_match("/^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-]+)*\.([a-zA-Z]{2,6})$/", $email);
}



/*if ($attachment_email && isset($_POST['submit2']))
{
$error = 0;
$fromEmail = NULL;//'tltour@mail.ru';
$fromName = NULL; //'Test Server';
$email = 'tltour@mail.ru';//;'butkach@mail.ru';//
$subject = 'Файл-опросник по шевронной ленте';

$convertedBody = "Поступило письмо от ".$_POST['cfio']." ;  тел.: ".$_POST['cphone'].";   email: ".$_POST['cemail'];

$tempdir = JPATH_BASE.DS."tmp".DS;
$namefile = $_FILES['fileulp']['name'];

if (!$_FILES['fileulp']['name'])
{
	echo '<div style="text-align:center; color:#FF0000; width:100%;">Выберите файл для отправки</div>';
	$error = 1;
	
}
if (($_FILES['fileulp']['size'] / 1024) > 2000)
{
	
	echo '<div style="text-align:center; color:#FF0000; width:100%;">Файл должен быть меньше 2 МБ</div>';
	$error = 1;
	
}

if(!$error)
{
$filetemp = $tempdir.$namefile;
move_uploaded_file($_FILES['fileulp']['tmp_name'], $tempdir.$namefile);
$attachment = $tempdir.$namefile;
//$filename = JPATH_BASE.DS.'images'.DS.'stories'.DS.'tubes.jpg';
$error = JUtility::sendMail($fromEmail, $fromName, $email, $subject, $convertedBody, true, null, null, $attachment);

  	if ($error == 1) echo '<div style="text-align:center; color:#3CB371; width:100%;">Ваше сообщение успешно отправлено</div>';
	else echo '<div style="text-align:center; color:#FF0000; width:100%;">Ваше сообщение не отправлено</div>';

unlink($attachment);
}
}*/


if ($pr_email  && isset($_POST['submit1']))
{
$check_a =0; $check_b =0; $check_c =0; $check_d =0; $check_e =0; $check_f =0; $check_g =0;
$check_h =0; $check_i =0; $check_h1 =0; $check_h2 =0; $check_h3 =0; $check_h4 =0; $check_h5 =0; $check_h6 =0; $check_h7 =0; $check_h8 =0;

    switch($_POST['check_risunok']) {
        case "check_a": $check_a = 1; break;
		case "check_b": $check_b = 1; break;
		case "check_c": $check_c = 1; break;
		case "check_d": $check_d = 1; break;
		case "check_e": $check_e = 1; break;
		case "check_f": $check_f = 1; break;
		case "check_g": $check_g = 1; break;
        default:   break;
    }

    switch($_POST['check_shevron']) {
		case "check_h": $check_h = 1; break;
		case "check_i": $check_i = 1; break;
		case "check_h1": $check_h1 = 1; break;
		case "check_h2": $check_h2 = 1; break;
		case "check_h3": $check_h3 = 1; break;
		case "check_h4": $check_h4 = 1; break;
		case "check_h5": $check_h5 = 1; break;
		case "check_h6": $check_h6 = 1; break;
		case "check_h7": $check_h7 = 1; break;
		case "check_h8": $check_h8 = 1; break;
        default:   break;
    }

if ($check_a || $check_b || $check_c || $check_d || $check_e || $check_f || $check_g || $check_i || $check_h1 || $check_h2 || $check_h3 || $check_h4 || $check_h5 || $check_h6 || $check_h7 || $check_h8) 
{

$myMessage = "Поступило письмо от ".$_POST['cfio'].";   тел.: ".$_POST['cphone'].";   email: ".$_POST['cemail']."\n \n ";


$myMessage .= "Были выбраны следующие данные: \n";
$myMessage .= "1: \n";

if ($check_a == true) 
{
	$myMessage .= "Рисунок рифления: А \n";
	$myMessage .= "H = ".$_POST['mo_h_a']."; "."L = ".$_POST['mo_l_a']."; "."k = ".$_POST['mo_k_a']."; "."alpha = ".$_POST['mo_alpha_a']."; "."b = ".$_POST['mo_b_a']."; "."c = ".$_POST['mo_c_a']." \n \n ";
 }
if ($check_b == true)
{
	$myMessage .= "Рисунок рифления: Б \n";
	$myMessage .= "H = ".$_POST['mo_h_b']."; "."L = ".$_POST['mo_l_b']."; "."k = ".$_POST['mo_k_b']."; "."b = ".$_POST['mo_b_b']." \n \n";
 }
if ($check_c == true)
{
	$myMessage .= "Рисунок рифления: B \n";
	$myMessage .= "H = ".$_POST['mo_h_c']."; "."L = ".$_POST['mo_l_c']."; "."k = ".$_POST['mo_k_c']."; "."alpha = ".$_POST['mo_alpha_c']."; "."a1 = ".$_POST['mo_a1_c']."; "."a2 = ".$_POST['mo_a2_c']."; "."a3 = ".$_POST['mo_a3_c']."; "."b = ".$_POST['mo_b_c']."; "."d = ".$_POST['mo_d_c']." \n \n";
 }
if ($check_d == true)
{
	$myMessage .= "Рисунок рифления: Г \n";
	$myMessage .= "H = ".$_POST['mo_h_d']."; "."L = ".$_POST['mo_l_d']."; "."k = ".$_POST['mo_k_d']."; "."alpha = ".$_POST['mo_alpha_d']."; "."b = ".$_POST['mo_b_d']." \n \n";
 }
if ($check_e == true)
{
	$myMessage .= "Рисунок рифления: Д \n";
	$myMessage .= "H = ".$_POST['mo_h_e']."; "."L = ".$_POST['mo_l_e']."; "."k = ".$_POST['mo_k_e']."; "."alpha = ".$_POST['mo_alpha_e']."; "."a = ".$_POST['mo_a_e']."; "."b = ".$_POST['mo_b_e']."; "."c = ".$_POST['mo_c_e']."; "."d = ".$_POST['mo_d_e']." \n \n";
 }
if ($check_f == true)
{
	$myMessage .= "Рисунок рифления: Е \n";
	$myMessage .= "H = ".$_POST['mo_h_f']."; "."L = ".$_POST['mo_l_f']."; "."k = ".$_POST['mo_k_f']."; "."alpha = ".$_POST['mo_alpha_f']."; "."b = ".$_POST['mo_b_f']." \n \n";
 }
if ($check_g == true)
{
	$myMessage .= "Рисунок рифления: Ж \n";
	$myMessage .= "H = ".$_POST['mo_h_g']."; "."L = ".$_POST['mo_l_g']."; "."k = ".$_POST['mo_k_g']."; "."b = ".$_POST['mo_b_g']."; "."c = ".$_POST['mo_c_g']." \n \n";
 }

if ($check_h == true)
{
	$myMessage .= "Тип и парамтеры шеврона: А \n";

 }
if ($check_i == true)
{
	$myMessage .= "Тип и парамтеры шеврона: Б \n";

 }
if ($check_h1 == true)
{
	$myMessage .= "Тип и парамтеры шеврона: В \n";

 }
if ($check_h2 == true)
{
	$myMessage .= "Тип и парамтеры шеврона: Г \n";

 }
 if ($check_h3 == true)
{
	$myMessage .= "Тип и парамтеры шеврона: Д \n";

 }
 if ($check_h4 == true)
{
	$myMessage .= "Тип и парамтеры шеврона: Е \n";

 }
 if ($check_h5 == true)
{
	$myMessage .= "Тип и парамтеры шеврона: Ж \n";

 }
 if ($check_h6 == true)
{
	$myMessage .= "Тип и парамтеры шеврона: З \n";

 }
 if ($check_h7 == true)
{
	$myMessage .= "Тип и парамтеры шеврона: И \n";

 }
 if ($check_h8 == true)
{
	$myMessage .= "Тип и парамтеры шеврона: К \n";

 }

$myMessage .= "\n";
 
/*if ($_POST['mo_2_1'])
{
	$myMessage .= "2. Высота эластичного борта (гофроборта):".$_POST['mo_2_1']." \n";
}*/
if ($_POST['mo_3_1'])
{
	$myMessage .= "2. Диаметр натяжного барабана:".$_POST['mo_3_1']." \n";
}
if ($_POST['mo_4_1'])
{
	$myMessage .= "3. Диаметр приводного барабана:".$_POST['mo_4_1']." \n";
}
if ($_POST['mo_5_1'] || $_POST['mo_5_2'] )
{
	$myMessage .= "4. Температурный режим работы ленты: от - ".$_POST['mo_5_1']." до + ".$_POST['mo_5_2']." \n";
}
if ($_POST['lenta_zamknuta'])
{
	$myMessage .= "5. Лента замкнута в кольцо:".$_POST['lenta_zamknuta']." \n";
}


if ($_POST['mo_6_1'] ) //|| $_POST['mo_6_2_1'] || $_POST['mo_6_2_2'] 
{	
	$myMessage .= "6. Параметры конвейерной ленты на основе которой изготовлена шевронная:\n"; 
	$myMessage .= "6.1. Маркировка ленты: ".$_POST['mo_6_1']." \n";
	//$myMessage .= "6.2. Требуемое количество: ".$_POST['mo_6_2_1']." М.п.; ".$_POST['mo_6_2_2']." М.кв. \n";
}
if ($_POST['mo_7_4'] || $_POST['mo_7_5'] || $_POST['mo_7_7'] || $_POST['mo_7_8'] || $_POST['mo_7_11']) //|| $_POST['mo_6_2_1'] || $_POST['mo_6_2_2'] 
	{	
	$myMessage .= "\n";
	$myMessage .= "Маркировка неизвестна: \n";
	 
	//$myMessage .= "Ширина ленты, мм ".$_POST['mo_7_1']." \n";
	//$myMessage .= "Требуемое количество: ".$_POST['mo_7_2']." М.п.; ".$_POST['mo_7_3']." М.кв. \n";
if ($_POST['mo_7_4'] )	$myMessage .= "Общая толщина ленты, мм: ".$_POST['mo_7_4']." \n";
if ($_POST['mo_7_5'] )	$myMessage .= "Количество тканевых прокладок, шт: ".$_POST['mo_7_5']." \n";
if ($_POST['mo_7_7'] || $_POST['mo_7_8'] )	$myMessage .= "Толщина резиновых обкладок, мм: Верхняя - ".$_POST['mo_7_7']."; Нижняя - ".$_POST['mo_7_8']."; \n"; //От - ".$_POST['mo_7_9']."; До - ".$_POST['mo_7_10']."	
if ($_POST['mo_7_11'] )	$myMessage .= "Прочие сведения (скорость движения ленты, транспортируемый материал, общая нагрузка на транспортер и т.п.): ".$_POST['mo_7_11']." \n";	
		
	}

}

	$mySubject = 'Опросный лист по шевронной ленте';
    //$myMessage = 'You received a message from \n\n';
	$recipient = 'tltour@mail.ru';//'butkach@mail.ru';//;

    $mailSender = &JFactory::getMailer();
	$mailSender->addRecipient($recipient);
	
	
//	$mailSender->setSender('1,2');
//  $mailSender->addReplyTo($recipient);

    $mailSender->setSubject($mySubject);
    $mailSender->setBody($myMessage);
	
//	$error = $mailSender->Send();
	
/*$fromEmail = 'tltour@mail.ru';
$fromName = $_POST['cfio'];
$email = 'tltour@mail.ru';
$subject = 'Test Mail Message';
$convertedBody = $myMessage;
	
	$error = JUtility::sendMail($fromEmail, $fromName, $email, $subject, $convertedBody, false, null, null, null);*/

  	if ($mailSender->Send()) 
	{		
		echo '<div style="text-align:center; color:#3CB371; width:100%;">Ваше сообщение успешно отправлено.</div>';
			if ($_POST['cemail'] != '' && is_email($_POST['cemail']))
			{	
			$recipient = $_POST['cemail'];
			
			$myMessage = "Ваш запрос с сайта www.konvejer.com получен и поступил в обработку. Наш менеджер свяжется с Вами в ближайшее время. \n \n";
			$myMessage .= "Надеемся на взаимовыгодное сотрудничество, \n \n ";
			$myMessage .= "ООО \"ТРП ТАРГЕТ\" \n ";
			$myMessage .= "Украина, г.Харьков \n ";
			$myMessage .= "пр.Ленина, 40 \n ";
			$myMessage .= "моб.+38 (067) 570 19 07 \n ";
			$myMessage .= "моб.+38 (050) 400 85 12 \n ";
			$myMessage .= "тел/факс:+38 (057) 758 16 93 \n ";
			$myMessage .= "http://www.konvejer.com \n ";

    		$mailSender = &JFactory::getMailer();
			$mailSender->addRecipient($recipient);
		    $mailSender->setSubject($mySubject);
    		$mailSender->setBody($myMessage);
				if ($mailSender->Send()) 
				{
					echo '<div style="text-align:center; color:#3CB371; width:100%;">На Ваш email отправлено письмо-подтверждение</div>';
				}
			}
	}
	else echo '<div style="text-align:center; color:#FF0000; width:100%;">Ваше сообщение не отправлено</div>';


  	/*if ($mailSender->Send()) echo '<div style="text-align:center; color:#3CB371; width:100%;">Ваше сообщение успешно отправлено</div>';
	else echo '<div style="text-align:center; color:#FF0000; width:100%;">Ваше сообщение не отправлено</div>';*/

}



/*if (!$pr_email)
{*/
?>
<div style="background:#FFF;" class="mod-oprosnik">


<?php

echo "<div style=\"width:100%; text-align:center;\"><h4>на изготовление ленты конвейерной шевронированой </h4></div>";
echo "<h5>1. Рисунок рифления (выберите необходимый)</h5>";
?>

<form onSubmit="return myValidate(this);" name="form" id="form" class="form-validate" action="" method="post" autocomplete="off" enctype="multipart/form-data">

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="mod-oprosnik-table" >
  <tr>
    <td width="64" rowspan="2"><img align="left" src="media/mod_oprosnik_lenta/images/lenta/lenta-a.jpg" style="width: 30px; margin: 0 0.2em 1em 6em;"><input class="risunok"  name="check_risunok" value="check_a"  id="check_a" type="radio"/><label for="check_a" class="check_a"></label><img align="left" src="media/mod_oprosnik_lenta/images/a.jpg" style="max-width:230px;"></td>
    <td width="290" class="right_separator">&nbsp;</td>
    <td width="39" rowspan="2"><img align="left" src="media/mod_oprosnik_lenta/images/lenta/lenta-b.jpg" style="width: 30px; margin: 0 0.2em 1em 6em;"><input class="risunok" value="check_b" name="check_risunok"  id="check_b" type="radio"/><label for="check_b" class="check_b"></label><img align="left" src="media/mod_oprosnik_lenta/images/b.jpg" style="max-width:230px;></td>
    <td width="197">&nbsp;</td>
  </tr>
  <tr>
    <td class="right_separator">
		<table class="mod-oprosnik-table" border="1px" cellpadding="0" cellspacing="0" style="position: relative; top: -15px;">
			<tr>
				<td style="width:90px;float:left;height:20px;">H, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_a" name="mo_h_a" id="mo_h_a" value=""/></td>
					<td style="clear:both;width:20px;border:none;"></td> 
				<td style="width:90px;float:left;height:20px;">&alpha;, гр</td><td class="inputtd"  style="width:90px;float:left;height:30px;"><input type="text" class="inp_a" name="mo_alpha_a" id="mo_alpha_a" value=""/></td>
			</tr>
			
			<tr>
				<td style="width:90px;float:left;height:20px;">L, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_a" name="mo_l_a" id="mo_l_a" value=""/></td>
					<td style="clear:both;width:20px;border:none;"></td> 
				<td style="width:90px;float:left;height:20px;">b, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_a" name="mo_b_a" id="mo_b_a" value=""/></td>
			</tr>
			
			<tr>
				<td style="width:90px;float:left;height:20px;">k, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_a" name="mo_k_a" id="mo_k_a" value=""/></td>
					<td style="clear:both;width:20px;border:none;"></td> 
				<td style="width:90px;float:left;height:20px;">c, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_a" name="mo_c_a" id="mo_c_a" value=""/></td>
			</tr>
        </table>
	</td>
    <td style="padding-left: 2em !important;">
        <table class="mod-oprosnik-table" border="none" cellpadding="0" cellspacing="0" style="position: relative; top: -15px;">
			<tr>
				<td style="width:90px;float:left;height:20px;">H, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_b" name="mo_h_b" id="mo_h_b" value=""/></td>
					<td style="clear:both;width:20px;border:none;"></td>
				<td style="width:90px;float:left;height:20px;">b, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_b" name="mo_b_b" id="mo_b_b" value=""/></td>
			</tr>
			
			<tr>
				<td style="width:90px;float:left;height:20px;">L, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_b" name="mo_l_b" id="mo_l_b" value=""/></td>
					<td style="clear:both;width:20px;border:none;"></td>
					<td style="width:90px;float:left;height:20px;"></td>
			</tr>
			
			<tr>
				<td style="width:90px;float:left;height:20px;">k, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_b" name="mo_k_b" id="mo_k_b" value=""/></td>
					<td style="clear:both;width:20px;border:none;"></td>
					<td style="width:90px;float:left;height:20px;"></td>
			</tr>
        </table>
    </td>
  </tr>
  <tr>
</tr>
<!--<td>&nbsp;</td>
<td class="right_separator">&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td width="10">&nbsp;</td>-->
  <tr>
    <td rowspan="2" align="center"><img align="left" src="media/mod_oprosnik_lenta/images/lenta/lenta-c.jpg" style="width: 30px; margin: 0 0.2em 1em 6em;"><input class="risunok" name="check_risunok" value="check_c" id="check_c" type="radio"/><label for="check_c" class="check_c"></label>
    <img align="left" src="media/mod_oprosnik_lenta/images/c.jpg"></td>
    <td class="right_separator">&nbsp;</td>
    <td rowspan="2"><img align="left" src="media/mod_oprosnik_lenta/images/lenta/lenta-d.jpg" style="width: 30px; margin: 0 0.2em 1em 6em;"><input class="risunok" value="check_d" name="check_risunok"  id="check_d" type="radio"/><label for="check_d" class="check_d"></label><img align="left" src="media/mod_oprosnik_lenta/images/d.jpg"></td>
    <td>&nbsp;</td>
  </tr>							
  <tr>
    <td class="right_separator">
    	<table class="mod-oprosnik-table" border="none" cellpadding="0" cellspacing="0" style="position: relative; top: 53px;">
			<tr>
				<td style="width:90px;float:left;height:20px;">H, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_c" name="mo_h_c" id="mo_h_c" value=""/></td>
					<td style="clear:both;width:20px;border:none;"></td>
				<td style="width:90px;float:left;height:20px;">a1, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_c"  name="mo_a1_c" id="mo_a1_c" value=""/></td>
			</tr>
			
			<tr>
				<td style="width:90px;float:left;height:20px;">L, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_c"  name="mo_l_c" id="mo_l_c" value=""/></td>
					<td style="clear:both;width:20px;border:none;"></td>
				<td style="width:90px;float:left;height:20px;">a2, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_c"  name="mo_a2_c" id="mo_a2_c" value=""/></td>
			</tr>
			
			<tr>
				<td style="width:90px;float:left;height:20px;">k, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_c"  name="mo_k_c" id="mo_k_c" value=""/></td>
					<td style="clear:both;width:20px;border:none;"></td>
				<td style="width:90px;float:left;height:20px;">a3, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_c"  name="mo_a3_c" id="mo_a3_c" value=""/></td>
			</tr>
			
			<tr>
				<td style="width:90px;float:left;height:20px;">&alpha;, гр</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_c"  name="mo_alpha_c" id="mo_alpha_c" value=""/></td>
					<td style="clear:both;width:20px;border:none;"></td>
				<td style="width:90px;float:left;height:20px;">b, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_c"  name="mo_b_c" id="mo_b_c" value=""/></td>
			</tr>
			
			<tr>
				<td style="width:90px;float:left;height:20px;"></td>
					<td style="clear:both;width:20px;border:none;"></td>
				<td style="width:90px;float:left;height:20px;">d, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_c"  name="mo_d_c" id="mo_d_c" value=""/></td>
			</tr>
        </table>
	</td>
    <td style="padding-left: 2em !important;">    	
		<table class="mod-oprosnik-table" border="none" cellpadding="0" cellspacing="0" style="position: relative; top: -12px;">
			<tr>
				<td style="width:90px;float:left;height:20px;">H, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_d" name="mo_h_d" id="mo_h_d" value=""/></td>
					<td style="clear:both;width:20px;border:none;"></td>
				<td style="width:90px;float:left;height:20px;">&alpha;, гр</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_d" name="mo_alpha_d" id="mo_alpha_d" value=""/></td>
			 </tr>
			
			<tr>
				<td style="width:90px;float:left;height:20px;">L, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_d"  name="mo_l_d" id="mo_l_d" value=""/></td>
					<td style="clear:both;width:20px;border:none;"></td>
				<td style="width:90px;float:left;height:20px;">b, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_d" name="mo_b_d" id="mo_b_d" value=""/></td>
			</tr>
			
			<tr>
				<td style="width:90px;float:left;height:20px;">k, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_d" name="mo_k_d" id="mo_k_d" value=""/></td>
					<td style="clear:both;width:20px;border:none;"></td>
					<td style="width:90px;float:left;height:20px;"></td>
			</tr>
        </table>
    </td>
  </tr>
</tr>
<td>&nbsp;</td>
<td class="right_separator">&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<tr><td>&nbsp;</td>
<td class="right_separator">&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td></tr>
<tr><td>&nbsp;</td>
<td class="right_separator">&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td></tr>
<tr><td>&nbsp;</td>
<td class="right_separator">&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td></tr>
<tr><td>&nbsp;</td>
<td class="right_separator">&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td></tr>
  <tr>
  <tr>
    <td rowspan="2"><img align="left" src="media/mod_oprosnik_lenta/images/lenta/lenta-e.jpg" style="width: 30px; margin: 0 0.2em 1em 6em;"><input class="risunok"  name="check_risunok" value="check_e" id="check_e" type="radio"/><label for="check_e" class="check_e"></label><img align="left" src="media/mod_oprosnik_lenta/images/e.jpg"></td>
    <td class="right_separator"></td>
    <td rowspan="2"><img align="left" src="media/mod_oprosnik_lenta/images/lenta/lenta-f.jpg" style="width: 30px; margin: 0 0.2em 1em 6em;"><input class="risunok" value="check_f" name="check_risunok"  id="check_f" type="radio"/><label for="check_f" class="check_f"></label><img align="left" src="media/mod_oprosnik_lenta/images/f.jpg"></td>
    <td></td>
  </tr>
  <tr>
        <td class="right_separator" >
		<table class="mod-oprosnik-table" border="none" cellpadding="0" cellspacing="0" style="position: relative; top: 26px;">
            <tr>
				<td style="width:90px;float:left;height:20px;">H, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_e" name="mo_h_e" id="mo_h_e" value=""/></td>
					<td style="clear:both;width:20px;border:none;"></td>
				<td style="width:90px;float:left;height:20px;">&alpha;, гр</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_e" name="mo_alpha_e" id="mo_alpha_e" value=""/></td>
            </tr>
			
            <tr>
				<td style="width:90px;float:left;height:20px;">L, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_e" name="mo_l_e" id="mo_l_e" value=""/></td>
					<td style="clear:both;width:20px;border:none;"></td>
				<td style="width:90px;float:left;height:20px;">b, мм</td><td  class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_e" name="mo_b_e" id="mo_b_e" value=""/></td>
            </tr>
			
            <tr>
				<td style="width:90px;float:left;height:20px;">k, мм</td><td  class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_e" name="mo_k_e" id="mo_k_e" value=""/></td>
					<td style="clear:both;width:20px;border:none;"></td>
				<td style="width:90px;float:left;height:20px;">c, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_e" name="mo_c_e" id="mo_c_e" value=""/></td>
            </tr>
			
            <tr>
                <td style="width:90px;float:left;height:20px;">a, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_e" name="mo_a_e" id="mo_a_e" value=""/></td>
					<td style="clear:both;width:20px;border:none;"></td>
				<td style="width:90px;float:left;height:20px;">d, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_e" name="mo_d_e" id="mo_d_e" value=""/></td>
            </tr>
        </table>
	</td>
    <td style="padding-left: 2em !important;">
		<table class="mod-oprosnik-table" border="none" cellpadding="0" cellspacing="0" style="position: relative; top: -6px;">
            <tr>
				<td style="width:90px;float:left;height:20px;">H, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_f" name="mo_h_f" id="mo_h_f" value=""/></td>
					<td style="clear:both;width:20px;border:none;"></td>
				<td style="width:90px;float:left;height:20px;">&alpha;, гр</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_f" name="mo_alpha_f" id="mo_alpha_f" value=""/></td>
          </tr>
		  
            <tr>
				<td style="width:90px;float:left;height:20px;">L, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_f" name="mo_l_f" id="mo_l_f" value=""/></td>
					<td style="clear:both;width:20px;border:none;"></td>
				<td style="width:90px;float:left;height:20px;">b, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_f" name="mo_b_f" id="mo_b_f" value=""/></td>
            </tr>
			
            <tr>
				<td style="width:90px;float:left;height:20px;">k, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_f" name="mo_k_f" id="mo_k_f" value=""/></td>
					<td style="clear:both;width:20px;border:none;"></td>
					<td style="width:90px;float:left;height:20px;"></td>
            </tr>
        </table>
    </td>
  </tr>
</tr>
<td>&nbsp;</td>
<td class="right_separator">&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<tr><td>&nbsp;</td>
<td class="right_separator">&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td></tr>
<tr><td>&nbsp;</td>
<td class="right_separator">&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td></tr>
<tr><td>&nbsp;</td>
<td class="right_separator">&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td></tr>
  <tr>  
  <tr>
    <td rowspan="2"><span class="right_separator"><img align="left" src="media/mod_oprosnik_lenta/images/lenta/lenta-g.jpg" style="width: 30px; margin: 0 0.2em 1em 6em;"><input class="risunok"  name="check_risunok" value="check_g" id="check_g" type="radio"/><label for="check_g" class="check_g"></label><img align="left" src="media/mod_oprosnik_lenta/images/g.jpg"></td>
    <td class="right_separator">&nbsp;</td>
    <td rowspan="2">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="right_separator">
		<table class="mod-oprosnik-table" border="none" cellpadding="0" cellspacing="0" style="position: relative; top: -15px;">
            <tr>
				<td style="width:90px;float:left;height:20px;">H, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_g" name="mo_h_g" id="mo_h_g" value=""/></td>
					<td style="clear:both;width:20px;border:none;"></td>
				<td style="width:90px;float:left;height:20px;">b, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_g" name="mo_b_g" id="mo_b_g" value=""/></td>
            </tr>
			
            <tr>
				<td style="width:90px;float:left;height:20px;">L, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_g" name="mo_l_g" id="mo_l_g" value=""/></td>
					<td style="clear:both;width:20px;border:none;"></td>
				<td style="width:90px;float:left;height:20px;">c, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_g" name="mo_c_g" id="mo_c_g" value=""/></td>
            </tr>
			
            <tr>
				<td style="width:90px;float:left;height:20px;">k, мм</td><td class="inputtd" style="width:90px;float:left;height:30px;"><input type="text" class="inp_g" name="mo_k_g" id="mo_k_g" value=""/></td>
					<td style="clear:both;width:20px;border:none;"></td>
					<td style="width:90px;float:left;height:20px;"></td>
            </tr>
        </table>
	</td>
    <td>&nbsp;</td>
  </tr>
</table>






</td>
</tr>
</table>

<?php echo "<h6>Тип и параметры шеврона (выберите необходимый тип профиля шеврона):</h6>";?>


<table width="100%" border="0" cellspacing="0" cellpadding="0" class="mod-oprosnik-table2" >
  <tr>
  
    <td width="25%" rowspan="1">
		<img align="left" src="media/mod_oprosnik_lenta/images/lenta/lenta-a.jpg" style="width: 30px!important; margin: 0 0.2em 5em 5.5em;">
		<input style="margin-top:0.6em" class="shevron"  name="check_shevron" value="check_h" id="check_h" type="radio"/>
		<label for="check_h" class="check_h"></label>
		<img style="width: 195px ! important;" align="left" src="media/mod_oprosnik_lenta/images/shevron/a.jpg">
	</td>
	
    <td width="25%" rowspan="1">
		<img align="left" src="media/mod_oprosnik_lenta/images/lenta/lenta-b.jpg" style="width: 30px!important; margin: 0 0.2em 3.1em 8em;">
		<input style="margin-top:0.6em"  class="shevron" value="check_i" name="check_shevron" id="check_i" type="radio"/>
		<label for="check_i" class="check_i"></label>
		<img align="left" src="media/mod_oprosnik_lenta/images/shevron/b.jpg" style="margin-left:-22px">
	</td>
	
    <td width="25%" rowspan="1">
		<img align="left" src="media/mod_oprosnik_lenta/images/lenta/lenta-c.jpg" style="width: 30px!important; margin: 0 0.2em 2.2em 5.6em;">
		<input style="margin-top:0.6em"  class="shevron" value="check_h1" name="check_shevron" id="check_h1" type="radio"/>
		<label for="check_h1" class="check_h1"></label>
		<img style="width: 285px ! important; margin-left:-33px" align="left" src="media/mod_oprosnik_lenta/images/shevron/c.jpg">
	</td>
	
    <td width="25%" rowspan="1">
		<img align="left" src="media/mod_oprosnik_lenta/images/lenta/lenta-d.jpg" style="width: 30px!important; margin: 0 0.2em 3.2em 4.8em;">
		<input style="margin-top:0.6em"  class="shevron" value="check_h2" name="check_shevron" id="check_h2" type="radio"/>
		<label for="check_h2" class="check_h2"></label>
		<img style="width: 250px ! important; margin-left:-37px" align="left" src="media/mod_oprosnik_lenta/images/shevron/d.jpg">
	</td>
	
  </tr>
  
  <tr>
  
    <td width="25%" rowspan="1">
		<img align="left" src="media/mod_oprosnik_lenta/images/lenta/lenta-e.jpg" style="width: 30px!important; margin: 0 0.2em 2.8em 5.3em;">
		<input style="margin-top:0.6em"  class="shevron"  name="check_shevron" value="check_h3" id="check_h3" type="radio"/>
		<label for="check_h3" class="check_h3"></label>
		<img style="width: 220px ! important;  margin-left:-10px" align="left" src="media/mod_oprosnik_lenta/images/shevron/e.jpg">
	</td>
	
    <td width="25%" rowspan="1">
		<img align="left" src="media/mod_oprosnik_lenta/images/lenta/lenta-f.jpg" style="width: 30px!important; margin: 0 0.2em 2.6em 7.5em;">
		<input style="margin-top:0.6em"  class="shevron" value="check_h4" name="check_shevron" id="check_h4" type="radio"/> 
		<label for="check_h4" class="check_h4"></label>
		<img style="width: 320px ! important; margin-left:15px;" align="left" src="media/mod_oprosnik_lenta/images/shevron/f.jpg">
	</td>
	
    <td width="25%" rowspan="1">
		<img align="left" src="media/mod_oprosnik_lenta/images/lenta/lenta-g.jpg" style="width: 30px!important; margin: 0 0.2em -0.1em 5em;">
		<input style="margin-top:0.6em"  class="shevron" value="check_h5" name="check_shevron" id="check_h5" type="radio"/> 
		<label for="check_h5" class="check_h5"></label>
		<img style="width: 220px ! important;  margin-left:5px" align="left" src="media/mod_oprosnik_lenta/images/shevron/g.jpg">
	</td>
	
    <td width="25%" rowspan="1">
		<img align="left" src="media/mod_oprosnik_lenta/images/lenta/lenta-h.jpg" style="width: 30px!important; margin: 0 0.2em 1.5em 5.2em;">
		<input style="margin-top:0.6em"  class="shevron" value="check_h6" name="check_shevron" id="check_h6" type="radio"/>
		<label for="check_h6" class="check_h6"></label>
		<img style="margin-left:-30px" align="left" src="media/mod_oprosnik_lenta/images/shevron/h.jpg">
	</td>
	
  </tr>
<tr><td>&nbsp;</td></tr>
  <tr>
  
    <td width="25%" rowspan="1">
		<img align="left" src="media/mod_oprosnik_lenta/images/lenta/lenta-j.jpg" style="width: 30px!important; margin: 0 0.2em 3.4em 5.5em;">
		<input style="margin-top:0.6em"  class="shevron"  name="check_shevron" value="check_h7" id="check_h7" type="radio"/>
		<label for="check_h7" class="check_h7"></label>
		<img style="width: 220px ! important; margin-left:-12px" align="left" src="media/mod_oprosnik_lenta/images/shevron/i.jpg">
	</td>
	
    <td width="25%" rowspan="1">
		<img align="left" src="media/mod_oprosnik_lenta/images/lenta/lenta-k.jpg" style="width: 30px!important; margin: 0 0.2em 1em 8em;">
		<input  style="margin-top:0.6em" class="shevron" value="check_h8" name="check_shevron" id="check_h8" type="radio"/> 
		<label for="check_h8" class="check_h8"></label>
		<img style="width: 250px ! important; margin-left:15px" align="left" src="media/mod_oprosnik_lenta/images/shevron/j.jpg">
	</td>
	
    <td width="25%" rowspan="1">
	</td>
	
    <td width="25%" rowspan="1">
	</td>
	
  </tr>
</table>
<br>


<div class="mod-oprosnik2">

<!--p> <div>2. Высота эластичного борта (гофроборта):</div> <input type="text" name="mo_2_1" id="mo_2_1" value=""/> мм («=0» если борта нет)</p-->
<p><div>2. Диаметр натяжного барабана:</div> <input type="text" name="mo_3_1" id="mo_3_1" value=""/> <span style="font:bold 14px Roboto">мм</span> </p>
<p><div>3. Диаметр приводного барабана:</div> <input type="text" name="mo_4_1" id="mo_4_1" value=""/> <span style="font:bold 14px Roboto">мм</span> </p>
<p><div>4. Температурный режим работы ленты: <span style="font:bold 14px Roboto;margin-left:15px">от -</span></div> <input type="text" name="mo_5_1" id="mo_5_1" value=""/> <span class="spando" style="font:bold 14px Roboto">до + </span><input type="text" name="mo_5_2" id="mo_5_2" value=""/> <span class="spandoc" style="font:bold 14px Roboto">C&ordm; </span></p>
<p><div style="width:300px">5. Лента замкнута в кольцо:</div> <input type="radio" name="lenta_zamknuta" id="lenta_zamknuta_da" value="Да"/> <label for="lenta_zamknuta_da" class="lenta_zamknuta_da"></label><span class="spanda"  style="font:bold 14px Roboto; margin-left:-80px;">Да </span><input type="radio" name="lenta_zamknuta" id="lenta_zamknuta_net" value="Нет"/> <label for="lenta_zamknuta_net" class="lenta_zamknuta_net"></label><span class="spannet"  style="font:bold 14px Roboto">Нет</span></p>
</div>

<h5>6. Параметры конвейерной ленты на основе которой изготовлена шевронная:</h5>
<div class="mod-oprosnik3">
<div>6.1. Маркировка ленты (если есть, напр. 2.2-1000-ТК200-5-5/2РБ)&nbsp;&nbsp;&nbsp;<input type="text" style="width: 280px; margin-left: 45px;" name="mo_6_1" id="mo_6_1" value=""/> </div>
<!--div>6.2. Требуемое количество:&nbsp;<input type="text" style="width:60px;" name="mo_6_2_1" id="mo_6_2_1" value=""/>&nbsp;М.п.&nbsp;&nbsp;или&nbsp;<input type="text" style="width:60px;" name="mo_6_2_2" id="mo_6_2_2" value=""/>&nbsp;М.кв.</div> 
</div-->
<br>
<div class="mod-oprosnik4">
<div class="noname-markirovka">Если маркировка неизвестна:</div>
<br>
<!--p><div>Ширина ленты, мм&nbsp;</div> <input type="text" name="mo_7_1" id="mo_7_1" value=""/></p>
<p><div>Требуемое количество:&nbsp;</div><input type="text" name="mo_7_2" id="mo_7_2" value=""/>&nbsp;М.п.&nbsp;&nbsp;или&nbsp;<input type="text" name="mo_7_3" id="mo_7_3" value=""/>&nbsp;М.кв.</p-->
<p><div>Общая толщина ленты:</div><input type="text" name="mo_7_4" id="mo_7_4" value=""/><span style="font:bold 14px Roboto">мм</span></p>
<p><div>Количество тканевых прокладок:</div> <input type="text" name="mo_7_5" id="mo_7_5" value=""/><span style="font:bold 14px Roboto">шт</span></p>
<p><div style="width:373px; float:left;">Толщина резиновых обкладок:</div><span style="text-align: left; font: bold 14px Roboto; width: 68px; float: left; padding-top: 5px;">Верхняя:</span> <input style="float:left" type="text" name="mo_7_7" id="mo_7_7" value=""/><span style="font: bold 14px Roboto; float: left; padding-top: 5px;">мм</span><span style="text-align: left; font: bold 14px Roboto; width: 68px; float: left; padding-top: 5px; margin-left: 30px;">Нижняя:</span> <input  type="text" name="mo_7_8" id="mo_7_8" value=""/><span style="font:bold 14px Roboto">мм</span></p>
<!--p>Температурный режим эксплуатации, С&ordm;</p>
<p><div style="text-align:right;">От&nbsp;</div> <input type="text" name="mo_7_9" id="mo_7_9" value=""/></p>
<p><div style="text-align:right;">До&nbsp;</div> <input type="text" name="mo_7_10" id="mo_7_10" value=""/></p-->
<div id="form-man">
	<p class="form-man-p1">Поля, отмеченные <span class="star">*</span> обязательны для заполнения</p>
	<div class="form-man-id">
		<p><div>Ваше имя <span class="star">*</span>&nbsp;</div> <input type="text"  class="inputbox required" name="cfio" id="cfio" value=""/></p>
		<p style="margin:0;"><div>Email <span class="star">*</span>&nbsp;</div> <input type="text" class="inputbox required" name="cemail" id="cemail" value=""/></p>
		<p><div>Контактный телефон <span class="star">*</span>&nbsp;</div> <input type="text"  class="inputbox required" name="cphone" id="cphone" value=""/></p>
	</div>
	<div class="form-man-text">
	<div style="text-align:left;">Дополнительная информация</div><textarea cols="60" rows="8" name="mo_7_11" id="mo_7_11"/></textarea>
	</div>
</div>
<div style="text-align:center; width:100%"> <button id="form-man-but" name="submit1" type="submit">Отправить</button></div>

</div>
<input type="hidden" name="pr_email" id="pr_email" value="1" />
<input type="hidden" name="check" id="check" value="" />

<!--/form-->
</div>


</form>

</div>
<br>
<br>
<br>
<br>
