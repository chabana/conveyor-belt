<?php
	defined('_JEXEC') or die('Restricted access');
	// Подключение файла helper.php
	require_once dirname(__FILE__).'/helper.php';
	// берем параметры из файла конфигурации
	$user_count = $params->get('usercount');
	// берем items из файла helper
	$items = ModMymodulHelper::getItems($user_count);
	// Эти параметры вводятся в административной панели в управлении модулем
	// И отвечает за показ количества пользователей
	$name_count = $params->get('name_count');
	// включение шаблона для отображения
	require(JModuleHelper::getLayoutPath('mod_oprosnik_lenta'));
?>
