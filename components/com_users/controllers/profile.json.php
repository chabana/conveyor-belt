<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

require_once JPATH_COMPONENT . '/controller.php';

/**
 * Profile controller class for Users.
 *
 * @package     Joomla.Site
 * @subpackage  com_users
 * @since       1.6
 */
class UsersControllerProfile extends UsersController
{
	/**
	 * Returns the updated options for help site selector
	 *
	 * @return  void
	 *
	 * @since   3.2
	 * @throws  Exception
	 */
	public function gethelpsites()
	{
		jimport('joomla.filesystem.file');

		// Set FTP credentials, if given
		JClientHelper::setCredentialsFromRequest('ftp');

		if (($data = file_get_contents('http://update.joomla.org/helpsites/helpsites.xml')) === false)
		{
			throw new Exception(JText::_('COM_CONFIG_ERROR_HELPREFRESH_FETCH'), 500);
		}
		elseif (!JFile::write(JPATH_ADMINISTRATOR . '/help/helpsites.xml', $data))
		{
			throw new Exception(JText::_('COM_CONFIG_ERROR_HELPREFRESH_ERROR_STORE'), 500);
		}

		$options = JHelp::createSiteList(JPATH_ADMINISTRATOR . '/help/helpsites.xml');
		echo json_encode($options);
		JFactory::getApplication()->close();
	}

    public function persons()
    {
        $app	= JFactory::getApplication();

        $result['success'] = true;

        $user = JFactory::getUser();

        $data['email']    = $app->input->post->get('email', array(), 'array');
        $data['name']     = $app->input->post->get('name', array(), 'array');
        $data['position'] = $app->input->post->get('position', array(), 'array');
        $data['phone']    = $app->input->post->get('phone', array(), 'array');

        // Get the model.
        /** @var UserModelContacts $model */
        $model = $this->getModel('Contacts', 'UserModel');

        $model->saveData($data, $user->id);

        echo new JResponseJson($result);

        $app->close();
    }

    public function checkReport()
    {
        $app	= JFactory::getApplication();
        $reportId   = $this->input->getInt('report_id', null, 'array');

        /** @var UserModelReports $model */
        $model = $this->getModel('Reports', 'UserModel');

        $user = JFactory::getUser();
        /** @var ReportModelSoap $soapModel */
        $soapModel = $this->getModel('Soap', 'ReportModel', array('login' => $user->username, 'password' => $user->soap_password));

        if ($soapModel->hasErrors()) {
            echo new JResponseJson(array('status' => false));

            $app->close();
        }

        $result = $soapModel->getStatusReport($reportId);
        if ($result['success'] == 'false') {
            echo new JResponseJson(array('status' => false));

            $app->close();
        }

        $result['status_text'] = $model->getStatusText(0);

        if ($result['success'] == 'true') {
            /** @var UserModelReports $reportHistoryModel */
            $reportHistoryModel = $this->getModel('Reports', 'UserModel');

            $result['status_text'] = $model->getStatusText($result['status']);
            $result['status_class'] = $model->getStatusClass($result['status']);

            $reportHistoryModel->updateStatus($reportId, $result['status']);
        }

        echo new JResponseJson($result);

        $app->close();
    }
}
