<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

require_once JPATH_COMPONENT . '/controller.php';

/**
 * Profile controller class for Users.
 *
 * @package     Joomla.Site
 * @subpackage  com_users
 * @since       1.6
 */
class UsersControllerProfile extends UsersController
{
    public function manuals()
    {
        $app  = JFactory::getApplication();
        $document	= JFactory::getDocument();

        $vName   = $this->input->getCmd('view', 'login');
        $vFormat = $document->getType();

        $view = $this->getView($vName, $vFormat);
        $view->hasError = false;
        $user = JFactory::getUser();

        /** @var UserModelReports $reportHistoryModel */
        $reportHistoryModel = $this->getModel('Reports', 'UserModel');
        /** @var ReportModelSoap $model */
        $model = $this->getModel('Soap', 'ReportModel', array('login' => $user->username, 'password' => $user->soap_password));
        if ($model->hasErrors()) {
            return $this->sendError($app, $view);
        }

        $startDate = $app->input->post->get('period-start-date', array(), 'string');
        $endDate  = $app->input->post->get('period-end-date', array(), 'string');

        $data['month_start'] = $data['month_stop'] = 1;
        $data['year_start'] = $data['year_stop'] = date('Y');

        if (!empty($startDate)) {
            list($data['month_start'], $data['year_start']) = explode(' ', $startDate);
            $data['month_start'] = $reportHistoryModel->getMonthDigit($data['month_start']);
        }

        if (!empty($endDate)) {
            list($data['month_stop'], $data['year_stop']) = explode(' ', $endDate);
            $data['month_stop'] = $reportHistoryModel->getMonthDigit($data['month_stop']);
        }

        $result = $model->getManuals($data);
        if ($result['success'] == 'false') {
            return $this->sendError($app, $view);
        }

        $result['emails'] = explode(',', $result['emails']);
        $result['emails'] = array_map('trim', $result['emails']);

        $view->manualsArr = $result;

        $this->display();
    }

    public function reports()
    {
        $app  = JFactory::getApplication();
        $document	= JFactory::getDocument();

        $vName   = $this->input->getCmd('view', 'login');
        $vFormat = $document->getType();

        $view = $this->getView($vName, $vFormat);
        $view->hasError = false;
        $user = JFactory::getUser();

        /** @var UserModelReports $reportHistoryModel */
        $reportHistoryModel = $this->getModel('Reports', 'UserModel');

        /** @var ReportModelSoap $model */
        $model = $this->getModel('Soap', 'ReportModel', array('login' => $user->username, 'password' => $user->soap_password));

        if ($model->hasErrors()) {
            return $this->sendError($app, $view);
        }

        $startDate = $app->input->post->get('period-start-date', array(), 'string');
        $endDate  = $app->input->post->get('period-end-date', array(), 'string');

        $data['month_start']= $data['month_stop'] = 1;
        $data['year_start'] = $data['year_stop'] = date('Y');

        if (!empty($startDate)) {
            list($data['month_start'], $data['year_start']) = explode(' ', $startDate);
            $data['month_start'] = $reportHistoryModel->getMonthDigit($data['month_start']);
        }

        if (!empty($endDate)) {
            list($data['month_stop'], $data['year_stop']) = explode(' ', $endDate);
            $data['month_stop'] = $reportHistoryModel->getMonthDigit($data['month_stop']);
        }

        $manualsArr = $model->getManuals($data);
        if ($manualsArr['success'] == 'false') {
            return $this->sendError($app, $view);
        }

        $data['adv_camp'] = $app->input->post->get('adv_camp', array(), 'array');
        $data['city']     = $app->input->post->get('city', array(), 'array');
        $data['media']    = $app->input->post->get('media', array(), 'array');
        $data['emails']   = $app->input->post->get('emails', array(), 'array');

        list($insertData['adv_camp'], $data['params']) = $this->processForInserting($manualsArr, $data);

        $result = $model->generateReport($data);
        if ($result['success'] == 'false') {
            return $this->sendError($app, $view);
        }

        if ($result['success'] == 'true') {
            $insertData['user_id'] = $user->id;
            $insertData['month_start'] = $data['month_start'];
            $insertData['year_start'] = $data['year_start'];
            $insertData['month_stop'] = $data['month_stop'];
            $insertData['year_stop'] = $data['year_stop'];
            $insertData['report_status'] = 0;
            $insertData['report_id'] = $result['report_id'];

            $reportHistoryModel->saveData($insertData);
        }

        $view->reportHistoryArr = $reportHistoryModel->getData($user->id);

        $this->display();
    }

    /**
     * @param array $manualsArr
     * @param array $params
     *
     * @return string
     */
    private function processForInserting($manualsArr, $params)
    {
        $result = array();
        $resultSoap = array();

        if (!empty($params['adv_camp']) && !empty($manualsArr['adv_camp'])) {
            foreach ($manualsArr['adv_camp'] as $advCamp) {
                if (in_array($advCamp['id'], $params['adv_camp'])) {
                    $resultSoap['adv_camp'][] = array('id' => $advCamp['id']);
                    $result[] = $advCamp['name'];
                }
            }
        }

        if (!empty($params['city']) && !empty($manualsArr['city'])) {
            foreach ($manualsArr['city'] as $city) {
                if (in_array($city['id'], $params['city'])) {
                    $resultSoap['city'][] = array('id' => $city['id']);
                    $result[] = $city['name'];
                }
            }
        }

        if (!empty($params['media']) && !empty($manualsArr['media'])) {
            foreach ($manualsArr['media'] as $media) {
                if (in_array($media['id'], $params['media'])) {
                    $resultSoap['media'][] = array('id' => $media['id']);
                    $result[] = $media['name'];
                }
            }
        }

        if (!empty($params['emails'])) {
            $resultSoap['emails'] =  $params['emails'];
            $result = array_merge($result, $params['emails']);
        }

        return array(implode(', ', $result), $resultSoap);
    }

    /**
     * @param JApplicationCms $app
     * @param JViewLegacy $app
     *
     * @return JController
     */
    private function sendError(JApplicationCms $app, JViewLegacy $view)
    {
        $app->clearHeaders();
        $app->setHeader('HTTP/1.1 403 Forbidden', 403);
        $app->sendHeaders();

        $view->hasError = true;

        return $this->display();
    }
}
