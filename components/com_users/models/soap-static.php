<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Profile model class for Users.
 *
 * @package     Joomla.Site
 * @subpackage  com_users
 * @since       1.6
 */
class ReportModelSoap
{
    const WSDL = 'http://212.248.10.101:8080/wsdl/Imi_api';

    private $client;

    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $password;

    public function __construct($config)
    {
//        $this->client = new SoapClient(self::WSDL);

        if ($config['login']) {
            $this->login = $config['login'];
        }

        if ($config['password']) {
            $this->password = $config['password'];
        }
    }

    /**
     * @param string $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param array $params
     *
     * @return array
     */
    public function getManuals($params)
    {
//        $result = $this->client->get_manuals($this->login, $this->password, $params['month_start'], $params['year_start'], $params['month_stop'], $params['year_stop']);

        $result = '{"success":"true","kind":"manual","message":"","adv_camp":[{"id":"2435","name":"Продвижение 3й-кв"}],"city":[{"id":"45","name":"Воронеж"},{"id":"52","name":"Кунгур"},{"id":"53","name":"Кстово"}],"media":[{"id":"5","name":"Интернет"}],"emails":"yantarinka@ya.ru"}';

        return json_decode($result, true);
    }

    /**
     * @param array $params
     *
     * @return array
     */
    public function generateReport($params)
    {
//var_dump($client->generate_report('test', 'test', '{"adv_camp":[{"id":1914}, {"id":687}, {"id":2312}], "city":[{"id":79}], "media":[{"id":1}]}', 1, 2010, 1, 2015));
//        $result = $this->client->generate_report($this->login, $this->password, {"adv_camp":[{"id":1914}, {"id":687}, {"id":2312}], "city":[{"id":79}], "media":[{"id":1}]}, $params['month_start'], $params['year_start'], $params['month_stop'], $params['year_stop']);

        $result = '{"success":"true","report_id":1}';

        return json_decode($result, true);
    }

    /**
     * @param int $reportId
     *
     * @return array
     */
    public function getStatusReport($reportId)
    {
//        $result = $this->client->get_status_report($this->login, $this->password, $reportId);

        $result = '{"success":"true","status":1}';

        return json_decode($result, true);
    }


    public function setState(){}
}