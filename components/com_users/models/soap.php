<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Profile model class for Users.
 *
 * @package     Joomla.Site
 * @subpackage  com_users
 * @since       1.6
 */
class ReportModelSoap
{
    const WSDL = 'http://212.248.10.101:8080/wsdl/Imi_api';

    private $client;

    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $password;

    private $errors;

    public function __construct($config)
    {
        try {
            $this->client = new SoapClient(self::WSDL);
        } catch (SoapFault $e) {
            $this->errors[] = $e->getMessage();
        }

        if ($config['login']) {
            $this->login = $config['login'];
//            $this->login = 'mediaimpact';
        }

        if ($config['password']) {
            $this->password = $config['password'];
//            $this->password = 'Th3sQ94d';
        }
    }

    /**
     * @param string $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param array $params
     *
     * @return array
     */
    public function getManuals($params)
    {
        try {
            $result = $this->client->get_manuals($this->login, $this->password, $params['month_start'], $params['year_start'], $params['month_stop'], $params['year_stop']);
        } catch (SoapFault $e) {
            $this->errors[] = $e->getMessage();

            return array('success' => 'false', 'message' => $e->getMessage());
        }

        return json_decode($result, true);
    }

    /**
     * @param array $params
     *
     * @return array
     */
    public function generateReport($params)
    {
        try {
            $result = $this->client->generate_report($this->login, $this->password, json_encode($params['params']), $params['month_start'], $params['year_start'], $params['month_stop'], $params['year_stop']);
        } catch (SoapFault $e) {
            $this->errors[] = $e->getMessage();

            return array('success' => 'false', 'message' => $e->getMessage());
        }

        return json_decode($result, true);
    }

    /**
     * @param int $reportId
     *
     * @return array
     */
    public function getStatusReport($reportId)
    {
        try {
            $result = $this->client->get_status_report($this->login, $this->password, $reportId);
        } catch (SoapFault $e) {
            $this->errors[] = $e->getMessage();

            return array('success' => 'false', 'message' => $e->getMessage());
        }

        return json_decode($result, true);
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return bool
     */
    public function hasErrors()
    {
        return !empty($this->errors);
    }

    public function setState(){}
}