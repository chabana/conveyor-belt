<?php
defined('_JEXEC') or die;

class UserModelReports extends JModelForm
{
    /**
     * @var array
     */
    private $columns = array('user_id', 'create_date', 'adv_camp', 'month_start', 'year_start', 'month_stop', 'year_stop', 'report_status', 'report_id');

    /**
     * @var string
     */
    private $table = '#__report_history';

    /**
     * @var array
     */
    private $status = array(0 => 'в очереди', 1 => 'выполнено', 2 => 'в очереди', 3 => 'ошибка');

    /**
     * @var array
     */
    private $status_class = array(0 => 'queue', 1 => 'done', 2 => 'queue', 3 => 'error');

    private $month = array(
        1 => 'Январь',
        2 => 'Февраль',
        3 => 'Март',
        4 => 'Апрель',
        5 => 'Май',
        6 => 'Июнь',
        7 => 'Июль',
        8 => 'Август',
        9 => 'Сентябрь',
        10 => 'Октябрь',
        11 => 'Ноябрь',
        12 => 'Декабрь',
    );

    /**
     * @var		object	The user profile data.
     * @since   1.6
     */
    protected $data;

    public function __construct($config = array())
    {
        parent::__construct($config);
    }

    public function getData($userId)
    {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $query->select($db->quoteName($this->columns));
        $query->from($db->quoteName($this->table));
        $query->where($db->quoteName('user_id') . ' = '. $db->quote($userId));
        $query->order('create_date DESC');

        $db->setQuery($query);

        $result  = $db->loadObjectList();

        if (!empty($result)) {
            foreach ($result as &$view) {
                $view->date_view = date('d.m.Y', strtotime($view->create_date));
                $view->status_view = $this->status[(int) $view->report_status];
                $view->status_class = $this->status_class[(int) $view->report_status];
                $view->period_view = $this->month[$view->month_start].' '.$view->year_start .' — '. $this->month[$view->month_stop].' '.$view->year_stop;
            }
        }

        return $result;
    }

    /**
     * @param $data
     * @param $userId
     */
    public function saveData($data)
    {
        $db = $this->getDbo();

        $nowDate = $db->quote(JFactory::getDate()->toSql());
        $query = $db->getQuery(true)
            ->insert($db->quoteName($this->table))
            ->columns($db->quoteName($this->columns));

        $values = array(
            $data['user_id'],
            $nowDate,
            $db->quote($data['adv_camp']),
            $data['month_start'],
            $data['year_start'],
            $data['month_stop'],
            $data['year_stop'],
            $data['report_status'],
            $data['report_id']
        );

        $query->values(implode(',', $values));
        $db->setQuery($query);
        $db->execute();

        if (!$this->checkCountReportHistory($data['user_id'])) {
            return false;
        }
    }

    /**
     * @param $userId
     *
     * @return mixed
     */
    public function checkCountReportHistory($userId)
    {
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $query->clear()
            ->select('count(*)')
            ->from($this->table)
            ->where($db->quoteName('user_id') . ' = '. $db->quote($userId));

        $db->setQuery($query);
        $count = $db->loadResult();

        if ($count > 6) {
            $query->clear()
                  ->select('id')
                  ->from($this->table)
                  ->where($db->quoteName('user_id') . ' = '. $db->quote($userId))
                  ->order('create_date DESC');

            $db->setQuery($query, 0, 6);
            $notInIds = $db->loadColumn();

            $db = $this->getDbo();
            $query = "DELETE FROM {$this->table} WHERE id NOT IN (".implode(',' , $notInIds).") AND user_id = ".$userId;
            $db->setQuery($query);

            return $db->execute();
        }

    }

    public function getStatusText($status)
    {
        return $this->status[$status];
    }

    public function getStatusClass($status)
    {
        return $this->status_class[$status];
    }

    public function updateStatus($reportId, $status)
    {
        $db = $this->getDbo();
        $query = $db->getQuery(true);

        $fields = array(
            $db->quoteName('report_status') . ' = ' . $status
        );

        $conditions = array(
            $db->quoteName('report_id') . ' = '.$reportId
        );

        $query->update($db->quoteName($this->table))->set($fields)->where($conditions);

        $db->setQuery($query);

        return $db->execute();
    }

    public function getForm($data = array(), $loadData = true)
    {
        return null;
    }

    /**
     * @param $monthName
     *
     * @return int|mixed
     */
    public function getMonthDigit($monthName)
    {
        $key = array_search($monthName, $this->month);

        return $key ? $key:1;
    }
}