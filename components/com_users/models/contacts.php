<?php
defined('_JEXEC') or die;

class UserModelContacts extends JModelForm
{
    private $columns = array('user_id', 'name', 'position', 'email', 'phone');

    private $table = '#__user_contacts';

    /**
     * @var		object	The user profile data.
     * @since   1.6
     */
    protected $data;

    public function __construct($config = array())
    {
        parent::__construct($config);
    }

    public function getData($userId)
    {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $query->select($db->quoteName($this->columns));
        $query->from($db->quoteName($this->table));
        $query->where($db->quoteName('user_id') . ' = '. $db->quote($userId));
        $query->order('id ASC');

        $db->setQuery($query);

        return $db->loadObjectList();
    }

    /**
     * @param $data
     * @param $userId
     */
    public function saveData($data, $userId)
    {
        $db = $this->getDbo();

        if (!$this->deleteAllContacts($userId)) {
            return false;
        }

        foreach ($data['name'] as $key => $name) {
            if (!empty($name)) {
                $email = isset($data['email'][$key]) ? $data['email'][$key]:'';
                $position = isset($data['position'][$key]) ? $data['position'][$key]:'';
                $phone = isset($data['phone'][$key]) ? $data['phone'][$key]:'';

                $values = array($userId, $db->quote($name), $db->quote($position), $db->quote($email), $db->quote($phone));

                $query = $db->getQuery(true)
                            ->insert($db->quoteName($this->table))
                            ->columns($db->quoteName($this->columns));

                $query->values(implode(',', $values));

                $db->setQuery($query);
                $db->execute();
            }
        }
    }

    public function deleteAllContacts($userId)
    {
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        // delete all custom keys for user 1001.
        $conditions = array(
            $db->quoteName('user_id') . ' = '. $db->quote($userId)
        );

        $query->delete($db->quoteName($this->table));
        $query->where($conditions);
        $db->setQuery($query);

        return $db->execute();
    }

    public function getForm($data = array(), $loadData = true)
    {
        return null;
    }
}