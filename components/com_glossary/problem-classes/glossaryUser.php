<?php

/**************************************************************
* This file is part of Glossary
* Copyright (c) 2008-12 Martin Brampton
* Issued as open source under GNU/GPL
* For support and other information, visit http://remository.com
* To contact Martin Brampton, write to martin@remository.com
*
* Please see glossary.php for more details
*/

if (!defined('_CMSAPI_ABSOLUTE_PATH')) die ('This software requires the Jaliro environment');

class glossaryUser {
	private static $fullusers = array();
	private static $users = array();
	
	protected $interface = null;
	protected $database = null;

	/** @var int ID for the user in the database */
	public $id=0;
	/** @var bool Is the current user of administrator status? */
	public $admin=false;
	/** @var bool Is the current user logged in?  */
	public $logged=false;
	/** @var string User name if loggged in */
	public $name='';
	/** @var string User full name if logged in */
	public $fullname='';
	/** @var string User type if logged in */
	public $usertype='';
	/** @var string User current IP address */
	public $currIP='';

	private $authoriser = null;

	public function __construct ( $id=0, $my=null ) {
		$this->interface = glossaryInterface::getInstance();
		$this->database = $this->interface->getDB();
		$this->id = $id;
		if ($id) {
			if (!$my) $my = self::getFullUser($id);
			if (!empty($my->gid) OR !empty($my->groups)) {
				$this->name = $my->username;
				$this->fullname = $my->name;
				$this->usertype = $my->usertype;
				$this->logged = true;
				if ('Joomla' == _CMSAPI_CMS_BASE AND '2.5' == _CMSAPI_JOOMLA_VERSION AND count(array_intersect($my->groups, array(7,8)))) $this->admin = true;
				elseif ((strtolower($my->usertype)=='administrator') OR (strtolower($my->usertype)=='superadministrator')
					OR (strtolower($my->usertype)=='super administrator') OR !empty($my->groups['Super Users'])){
					$this->admin = true;
				}
			}
		}
		$this->currIP = $this->interface->getIP();
	}

	public function __toString () {
		return __CLASS__.' :: '.jaliroDebug::dumpValues(get_object_vars($this));
	}

	public function isAdmin () {
		return $this->admin;
	}
	public function isUser () {
		if ($this->isAdmin()) return false;
		return $this->isLogged();
	}
	public function isLogged () {
		return $this->logged;
	}
	public function fullname () {
		return $this->fullname;
	}

	public function sendMailFrom ($emailto, $subject, $text='') {
		$email = $this->getEmailAddress();
		if ($email) return $this->interface->sendMail($email, $this->fullname(), $emailto, $subject, $text, null, null, null, null, null, null);
		else return false;
	}

	public function sendMailTo ($subject, $text='', $emailfrom='', $fromname='') {
		if (!$emailfrom) {
			if ($this->interface->getCfg('Mailfrom')) {
				$emailfrom = $this->interface->getCfg('Mailfrom');
				$fromname = $this->interface->getCfg('Fromname');
			}
			else {
				$emailfrom = self::superAdminMail();
				$fromname = '';
			}
		}
		$email = $this->getEmailAddress();
		$transformer = array(
			'{toname}' => $this->fullname,
			'{touser}' => $this->name,
			'{toemail}' => $email,
			'{fromname}' => $fromname,
			'{fromemail}' => $emailfrom
		);
		$text = str_replace(array_keys($transformer), array_values($transformer), $text);
		if ($email) return $this->interface->sendMail($emailfrom, $fromname, $email, $subject, $text, null, null, null, null, null, null);
		else return false;
	}

	public function getEmailAddress () {
		return self::getFullUser($this->id)->email;
	}

	public static function superAdminMail () {
		$sql="select email from #__users where usertype='superadministrator' or usertype='super administrator'";
		$this->database->setQuery( $sql );
		return $this->database->loadResult();
	}

	public static function getUser ($id=0, $my=null) {
		return isset(self::$users[$id]) ? self::$users[$id] : self::$users[$id] = new self($id, $my);
	}

	protected static function getFullUser ($id) {
		return isset(self::$fullusers[$id]) ? self::$fullusers[$id] : self::$fullusers[$id] = $this->interface->getIdentifiedUser($id);
	}
}
