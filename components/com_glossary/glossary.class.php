<?php

/**************************************************************
* This file is part of Glossary
* Copyright (c) 2008-10 Martin Brampton
* Issued as open source under GNU/GPL version 2
*
* Please note that this code is released subject to copyright
* and is licensed to you under the terms of the GPL version 2.
* It is NOT in the public domain.
*
* Although the GPL grants generous rights to you, it does require
* you to observe certain limitations.  Please study the GPL
* if you need details.

* For support and other information, visit http://remository.com
* To contact Martin Brampton, write to martin@remository.com
*
* The glossary component was originally based on Arthur Konze's
* Akobook Guestbook component and on the Weblinks component.
*
* Up to version 1.3 the Glossary was developed by Michelle Farren;
* development was then carried forward up to version 1.5 by Sascha
* Claren. Bernhard Zechmann created 1.9.x versions (www.zechmann.com)
*
* All upcoming versions are developed and released by Martin Brampton
* with the Glossary component totally rewritten at release 2.5.
* Subsequent versions consist of code entirely written by Martin
* Brampton except where specifically stated.
*
**/

if (!defined('_CMSAPI_ABSOLUTE_PATH')) return;

@define( 'MARKDOWN_PARSER_CLASS',  'Markdown_Parser' );

// Define the current version, component name, and default ordering
// These three lines are component specific, the rest of the code is generic
if (!defined('_GLOSSARY_VERSION')) define('_GLOSSARY_VERSION', '3.0');
// Ordering not required for glossary, defined as part of A CMS API
if (!defined ('_GLOSSARY_DEFAULT_ORDERING')) define ('_GLOSSARY_DEFAULT_ORDERING', 1);

require_once (dirname(__FILE__).'/com_glossary_constants.php');

class glossaryInterface extends cmsapiInterface {
	protected $glossaryUser = null;
	
	public static function getInstance () {
		return (@parent::$instances['com_glossary'] instanceof self) ? parent::$instances['com_glossary'] : parent::$instances['com_glossary'] = new self('com_glossary');
	}

	// Returns a Glossary user, based on the CMS user
	public function getUser () {
		if (!is_object($this->glossaryUser)) {
			$my = parent::getUser();
			$this->glossaryUser = glossaryUser::getUser ($my->id,$my);
		}
		return $this->glossaryUser;
	}
}

// In case we want to override or extend any methods
abstract class glossaryDatabaseRow extends aliroDatabaseRow {
	protected $DBclass = 'aliroDatabase';
}

abstract class glossaryAbstract extends aliroDatabaseRow {
	/** @private static string component name */
	private static $cname = 'com_glossary';
	/** @private static array Glossary class directories */
	private static $directories = array(
		'controller-admin-classes',
		'controller-classes',
		'plugin-classes',
		'problem-classes',
		'module-classes',
		'view-admin-classes',
		'view-classes',
		'markdown'
	);
	protected $DBclass = 'aliroDatabase';
	protected $interface = null;
	protected $database = null;

	public function __construct () {
		$this->interface = self::getInterface();
		$this->database = $this->interface->getDB();
	}

	public static function getInterface () {
		return glossaryInterface::getInstance();
	}

	public static function getDB () {
		return glossaryAbstract::getInterface()->getDB();
	}

	public static function setAutoload () {
		cmsapiInterface::setAutoload (_CMSAPI_ABSOLUTE_PATH.'/components/com_glossary/');
	}
	
	public static function setNewAutoload () {
		cmsapiInterface::setNewAutoload (_CMSAPI_ABSOLUTE_PATH.'/components/com_glossary/');
	}
}

if (defined('_CMSAPI_ABSOLUTE_PATH')) glossaryAbstract::setAutoload();