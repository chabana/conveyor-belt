<?php

/**************************************************************
* This file is part of Glossary
* Copyright (c) 2008-9 Martin Brampton
* Issued as open source under GNU/GPL
* For support and other information, visit http://remository.com
* To contact Martin Brampton, write to martin@remository.com
*
* Please see glossary.php for more details
*/

if (!defined('_CMSAPI_ABSOLUTE_PATH')) die ('This software requires the Jaliro environment');

class glossaryAlphabetHTML extends glossaryHTML {
	
	public function view ($glossary, $defaultgloss, $letters, $currentletter, $numeric, $linkcurrent) {
		$baselink = 'index.php?option=com_glossary&task=list';
		if ($glossary->id AND $glossary->id != $defaultgloss->id) $baselink .= '&glossid='.$glossary->id;
		$baselink .= '&letter=';
		if ($numeric) $letterhtml[] = ('9' == $currentletter ? '0-9' : "<a href=\"{$baselink}9\">" . '0-9' . "</a>");
		if ($letters) foreach ($letters as $letter) {
			$displayletter = 'all' == $letter ? _GLOSSARY_ALL : $letter;
			if ($currentletter == $letter AND !$linkcurrent) $letterhtml[] = <<<NO_LINK

			<span class="glossletselect">{$this->show($displayletter)}</span>

NO_LINK;

			else {
				$query = $baselink;
				if ('all' != $letter) $query .= ''.urlencode($letter);
				$link = $this->interface->sefRelToAbs($query);
				$letterhtml [] = <<<LETTER_LINK

			<a href="$link">{$this->show($displayletter)}</a>

LETTER_LINK;

			}
		}
		$alphabet = empty($letterhtml) ? '' : implode (' ', $letterhtml);
		return <<<LIST_ALPHA
		
		<div class="glossaryalphabet">
			$alphabet
		</div>
		
LIST_ALPHA;
		
	}
	
}
