<?php

/**************************************************************
* This file is part of Glossary
* Copyright (c) 2008-9 Martin Brampton
* Issued as open source under GNU/GPL
* For support and other information, visit http://remository.com
* To contact Martin Brampton, write to martin@remository.com
*
* Please see glossary.php for more details
*/

if (!defined('_CMSAPI_ABSOLUTE_PATH')) die ('This software requires the Jaliro environment');

class glossaryToolbar {
	protected $act;
	protected $task;
	// Create an instance, get the controlling parameters from the request
	function glossaryToolbar () {
		$interface = glossaryInterface::getInstance();
		if ($this->act = $interface->getParam ($_REQUEST, 'act', 'about'));
		else $this->act = 'about';
		if ($this->task = $interface->getParam($_REQUEST, 'task', 'list'));
		else $this->task = 'list';

        if($this->task=='list'){
            JSubMenuHelper::addEntry(JText::_('COM_GLOSSARY_MANAGE_GLOSSARIES'),
            'index.php?option=com_glossary&act=glossaries', $this->act == 'glossaries');
            JSubMenuHelper::addEntry(JText::_('COM_GLOSSARY_MANAGE_ENTRIES'),
            'index.php?option=com_glossary&act=entries', $this->act == 'entries');
            JSubMenuHelper::addEntry(JText::_('COM_GLOSSARY_CONFIG'),
            'index.php?option=com_glossary&act=config', $this->act == 'config');
            JSubMenuHelper::addEntry(JText::_('COM_GLOSSARY_ABOUT'),
            'index.php?option=com_glossary&act=about', $this->act == 'about');
        }

		$this->makeBar();
	}
	
	// create a toolbar based on the parameters found in $_REQUEST
	function makeBar () {
		$this->start();
		$act = $this->act;
		if (method_exists($this,$act)) $this->$act();
		$this->finish();
	}
	// Any initial actions
	function start () {
		cmsapiMenuBar::startTable();
        /*if ('cpanel' != $this->act) cmsapiMenuBar::custom ('cpanel', 'home-2.png', 'home-2_f2.png', _CMSAPI_CPANEL_RETURN, false );*/
	}

	function finish () {
		cmsapiMenuBar::spacer();
		cmsapiMenuBar::endTable();
	}

	// The cancel option is always formed the same way
	function cancel_Button () {
		cmsapiMenuBar::custom( 'list', 'cancel.png', 'cancel_f2.png', _CMSAPI_CANCEL, false );
	}

	function entries () {
		if ($this->task == 'add') $this->addMenu('Entries');
		elseif ($this->task == 'edit' OR $this->task == 'apply') $this->editMenu('Entry');
		else $this->listMenu('');
	}

	function glossaries () {
		if ($this->task == 'add') $this->addMenu('Glossary');
		elseif ($this->task == 'edit' OR $this->task == 'apply') $this->editMenu('Glossary');
		else $this->listMenu('');
	}

	function config () {
		cmsapiMenuBar::save( 'save', 'Save Config' );
	}

	// The menu for adding something is always the same apart from the text
	function addMenu ($entity) {
        cmsapiMenuBar::custom( 'apply', 'apply.png', 'apply_f2.png', 'Save', false );
        cmsapiMenuBar::save( 'save', 'Save and close' );
       	$this->cancel_Button();
	}
	// The menu for editing something is always the same apart from the text
	function editMenu ($entity) {
        cmsapiMenuBar::custom( 'apply', 'apply.png', 'apply_f2.png', 'Save', false );
        cmsapiMenuBar::save( 'save', 'Save and close' );
		$this->cancel_Button();
	}
	// The menu for a list of items is always the same apart from the text
	function listMenu ($entity) {
        cmsapiMenuBar::addNew( 'add', 'New '.$entity );
        cmsapiMenuBar::editList( 'edit', 'Edit '.$entity );
		cmsapiMenuBar::publishList( 'publish', 'Publish '.$entity );
		cmsapiMenuBar::unpublishList( 'unpublish', 'UnPublish '.$entity );
		cmsapiMenuBar::deleteList( '', 'delete', 'Delete '.$entity );
	}
	
}