<?php

/**************************************************************
 * This file is part of Glossary
 * Copyright (c) 2008-9 Martin Brampton
 * Issued as open source under GNU/GPL
 * For support and other information, visit http://remository.com
 * To contact Martin Brampton, write to martin@remository.com
 *
 * Please see glossary.php for more details
 */

if (!defined('_CMSAPI_ABSOLUTE_PATH')) die ('This software requires the Jaliro environment');

class listGlossaryConfigHTML extends glossaryAdminHTML
{

    public function view($spec, $config)
    {
        JHtml::_('behavior.framework');
        JHtml::_('formbehavior.chosen', 'select');

        JToolbarHelper::title($this->show(_GLOSSARY_COMPONENT_TITLE) . ' - ' . $this->show(_GLOSSARY_EDIT_CONFIG),'equalizer.png');

        echo <<<CONFIG_HEADING

		<form action="{$this->interface->indexFileName()}" method="post" id="adminForm" name="adminForm">


<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
CONFIG_HEADING;

        foreach ($spec as $i => $specdetail) {
            $tabname = $specdetail[0];
            $pagenum = $i + 1;

            $active = '';
            if ($i == 0) {
                $active = 'active';
            }
            echo '<li class="' . $active . '"><a href="#' . 'page' . $pagenum . '" data-toggle="tab">' . $tabname . '</a></li>';
        }

        echo '</ul><div id="my-tab-content" class="tab-content">';

        foreach ($spec as $i => $specdetail) {

            $fields = $specdetail[1];
            $pagenum = $i + 1;

            $active = '';
            if ($i == 0) {
                $active = 'active';
            }

            echo <<<TAB_START
			 <div class="tab-pane $active" id="page$pagenum">


	<table width="100%" border="0" cellpadding="4" cellspacing="2" class="adminform">
		
TAB_START;

            foreach ($fields as $fieldname => $field) {
                $value = empty($config->$fieldname) ? '' : $config->$fieldname;
                echo $this->viewField($field[0], $field[1], $fieldname, $field[2], $field[3], $value);
            }

            echo <<<TAB_END
			
	</table>
			</div>
TAB_END;


        }

        echo <<<END_CONFIG
		</div>

			<input type="hidden" name="task" value="" />
			<input type="hidden" name="act" value="$this->act" />
			<input type="hidden" name="option" value="com_glossary" />

		</form>

END_CONFIG;

    }

    private function viewField($legend, $description, $fieldname, $type, $extra, $value)
    {
        switch ($type) {
            case 'yesno':
                $inputdata = $this->viewYesNo($fieldname, $value);
                break;
            case 'input':
                $inputdata = $this->viewInput($fieldname, $extra, $value);
                break;
            case 'menu':
                $inputdata = $extra;
                break;
            default:
                $inputdata = 'Invalid configuration field specified';

        }
        return <<<CONFIG_FIELD
		
		<tr>
			<td>$legend</td>
			<td>$inputdata</td>
			<td>$description</td>
		</tr>
		
CONFIG_FIELD;

    }

    private function viewYesNo($fieldname, $value)
    {
        $selyes = $value ? 'selected="selected"' : '';
        $selno = $value ? '' : 'selected="selected"';
        $yes = _CMSAPI_YES;
        $no = _CMSAPI_NO;
        return <<<YES_NO
		
				<select name="$fieldname" class="inputbox">
					<option value="1" $selyes>$yes</option>
					<option value="0" $selno>$no</option>
				</select>
		
YES_NO;

    }

    private function viewInput($fieldname, $size, $value)
    {

        return <<<SIMPLE_INPUT
		
				<input type="text" class="inputbox" name="$fieldname" size="$size" value="$value" />
				
SIMPLE_INPUT;

    }


}