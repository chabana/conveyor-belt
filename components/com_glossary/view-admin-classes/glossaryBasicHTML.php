<?php

/**************************************************************
* This file is part of Glossary
* Copyright (c) 2008 Martin Brampton
* Issued as open source under GNU/GPL
* For support and other information, visit http://remository.com
* To contact Martin Brampton, write to martin@remository.com
*
* See header in glossary.php for further details
*/

if (!defined('_CMSAPI_ABSOLUTE_PATH')) die ('This software requires the Jaliro environment');

class glossaryBasicHTML extends glossaryHTML {
	protected $pageNav = '';
	protected $act = '';
	protected $limit = 10;

	function __construct (&$controller, $limit) {
		$this->act = $_REQUEST['act'];
		$this->limit = $limit;
		$this->pageNav = $controller->pageNav;
	}

	function setUpCalendar () {
		$live_site = $this->interface->getCfg('live_site');
		$links = <<<LINKS_CALENDAR

		<!-- import the calendar CSS -->
		<link rel="stylesheet" type="text/css" media="all" href="$live_site/includes/js/calendar/calendar-mos.css" title="green" />
		<!-- import the calendar script -->
		<script type="text/javascript" src="$live_site/includes/js/calendar/calendar_mini.js"></script>
		<!-- import the language module -->
		<script type="text/javascript" src="$live_site/includes/js/calendar/lang/calendar-en.js"></script>

LINKS_CALENDAR;
		$this->interface->addCustomHeadTag($links);
	}

	function calendarSelector ($fieldname) {
		static $first = true;
		if ($first) $html = <<<CALENDAR_FIRST

			<input name="reset" type="reset" class="button" onclick="return showCalendar('$fieldname', 'y-mm-dd');" value="..." />

CALENDAR_FIRST;

		else $html = <<<CALENDAR_AFTER

			<input type="reset" class="button" value="..." onclick="return showCalendar('$fieldname', 'y-mm-dd');" />

CALENDAR_AFTER;

		$first = false;
		return $html;
	}

	function tickBox ($object, $property) {
		if (is_object($object) AND $object->$property) $checked = "checked='checked'";
		else $checked = '';
		echo "<td><input type='checkbox' name='$property' value='1' $checked /></td>";
	}

	function yesNoList ($object, $property) {
		$yesno[] = $this->repository->makeOption( 0, _NO );
		$yesno[] = $this->repository->makeOption( 1, _YES );
		if ($object) $default = $object->$property;
		else $default = 0;
		echo '<td valign="top">';
		echo $this->repository->selectList($yesno, $property, 'class="inputbox" size="1"', $default);;
		echo '</td></tr>';
	}

	function yesNoRadio ($object, $property, $title) {
		$yes = is_object($object) AND $object->$property;
		$yescheck = $yes ? 'checked="checked"' : '';
		$nocheck = $yes ? '' : 'checked="checked"';
		$yestext = _YES;
		$notext = _NO;

		$html = <<<INPUT_BOX

		<div>
			<label for="$property" style="width=30%"><strong>$title</strong></label>
			<input class="inputbox" type="radio" id="$property" name="$property" $yescheck value="1" />$yestext
			<input class="inputbox" type="radio" name="$property" $nocheck value="0" />$notext
		</div>
INPUT_BOX;

		return $html;
	}

	function inputTop ($title, $redstar=false, $maxsize=0) {
		?>
		<tr>
		  	<td width="30%" valign="top" align="right">
				<b><?php if ($redstar) echo '<font color="red">*</font>'; echo $title; if ($maxsize) echo "</b>&nbsp;<br /><i>$maxsize</i>&nbsp;"; ?></b>&nbsp;
			</td>
		<?php
	}

	function blankRow () {
		?>
			<tr><td>&nbsp;</td></tr>
		<?php
	}

	function fileInputBox ($title, $name, $value, $width, $tooltip=null) {
		?>
		<tr>
		  	<td width="30%" valign="top" align="right">
		  	<b><?php echo $title; ?></b>
			</td>
			<td align="left" valign="top">
				<input class="inputbox" type="text" name="<?php echo $name; ?>" size="<?php echo $width; ?>" value="<?php echo $value; ?>" />
				<?php if ($tooltip) echo tooltip($tooltip); ?>
			</td>
		</tr>
		<?php
	}

	function simpleInputBox ($title, $name, $value, $width, $tooltip=null) {
		$html = <<<INPUT_BOX

		<div>
			<label for="$name" style="width=30%"><strong>$title</strong></label>
			<input id="$name" class="inputbox" type="text" name="$name" size="$width" value="$value" />
		</div>
INPUT_BOX;

		if ($tooltip) $html .= $tooltip;
		return $html;
	}

	function narrowInputBox ($title, $name, $value, $width, $tooltip=null) {
		$html = <<<INPUT_BOX

		<label for="$name"><strong>$title</strong></label>
		<div>
			<input id="$name" class="inputbox" type="text" name="$name" size="$width" value="$value" />
		</div>
INPUT_BOX;

		if ($tooltip) $html .= $tooltip;
		return $html;
	}

	function fileUploadBox ($title, $width, $tooltip=null) {
		?>
		<tr>
		  	<td width="30%" valign="top" align="right">
		  	<b><?php echo $title; ?></b>
			</td>
			<td align="left" valign="top">
				<input class="inputbox" type="file" name="userfile" size="<?php echo $width ?>" />
				<?php if ($tooltip) echo tooltip($tooltip); ?>
			</td>
		</tr>
		<?php
	}

	function simpleUploadBox ($title, $width, $tooltip=null) {
		$html = <<<SIMPLE_UPLOAD

		<label for="userfile"><strong>$title</strong></label>
		<div>
			<input class="inputbox" type="file" id="userfile" name="userfile" size="$width" />
		</div>

SIMPLE_UPLOAD;

		if ($tooltip) $html .= $tooltip;
		return $html;
	}

	function fileInputArea ($title, $maxsize, $name, $value, $rows, $cols, $editor=false, $tooltip=null) {
		?>
		<tr>
		  	<td width="30%" valign="top" align="right">
		  	<b><?php echo $title; echo "</b>&nbsp;<br /><i>$maxsize</i>&nbsp;";?></b>
			</td>
		<?php
		echo '<td valign="top">';
		if ($editor) $this->interface->editorArea( 'description', $value, $name, 450, 200, $rows, $cols );
		else echo "<textarea class='inputbox' name='$name' rows='$rows' cols='$cols'>$value</textarea>";
		if ($tooltip) echo tooltip($tooltip);
		echo '</td></tr>';
	}

	function simpleInputArea ($title, $maxsize, $name, $value, $rows, $cols, $editor=false, $tooltip=null) {
		// last params were $rows, $cols
		ob_start();
		if ($editor) $editbox = $this->interface->editorAreaText( 'description', $value, $name, 450, 350, $cols, $rows );
		else $editbox = "<textarea class='inputbox' id='$name' name='$name' rows='$rows' cols='$cols'>$value</textarea>";
		$editbox .= ob_get_clean();

		return <<<INPUT_AREA

		<div class="cmsapiinputarea">
			<label for="$name"><strong>$title</strong>
			<br /><em>$maxsize</em></label>
			<div>
				$editbox
				$tooltip
			</div>
		</div>

INPUT_AREA;

	}

	function tickBoxField ($object, $property, $title) {
		?>
		<tr>
			<td width="30%" valign="top" align="right">
				<b><?php echo $title; ?></b>&nbsp;
			</td>
		<?php
		if (is_object($object) AND $object->$property) $checked = "checked='checked'";
		else $checked = '';
		echo "<td><input type='checkbox' name='$property' value='1' $checked /></td>";
		echo '</tr>';
	}

	function simpleTickBox ($title, $name, $checked=false) {
		if ($checked) $check = 'checked="checked"';
		else $check = '';
		?>
		<tr>
			<td width="30%" valign="top" align="right">
				<b><?php echo $title; ?></b>&nbsp;
			</td>
			<td>
				<input type="checkbox" name="<?php echo $name; ?>" value="1" <?php echo $check; ?> />
			</td>
		</tr>
		<?php
	}

	function simpleFormEnd () {
		echo <<<FORM_END

		<tr>
			<td>
				<div class="cmsapiblock">&nbsp;</div>
				<input type="hidden" name="option" value="{$this->cname}" />
				<input type="hidden" name="task" value="" />
			</td>
		</tr>
		</table>
		</form>

FORM_END;

	}

	function listHeadingStart ($count) {
		echo <<<LIST_HEAD
		
		<table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
			<thead>
			<tr>
				<th width="5" align="left">
					<input type="checkbox" name="toggle" value="" onclick="checkAll($count);" />
				</th>
				
LIST_HEAD;

	}

	function headingItem ($width, $title) {
		echo "\n<th width=\"$width\" align=\"left\">$title</th>";
	}

	function commonScripts ($edit_fields) {
		?>
		<script type="text/javascript">
        function submitbutton(pressbutton) {
                <?php
				if (is_array($edit_fields)) foreach ($edit_fields as $field) $this->interface->getEditorContents( $field );
				else $this->interface->getEditorContents ($edit_fields);
				?>
                submitform( pressbutton );
        }
        </script>
        <?php
	}

	function editFormEnd ($id, $oldpath) {
		echo <<<FORM_END
		
		</table>
		<div>
			<input type="hidden" name="cfid" value="$id" />
			<input type="hidden" name="limit" value="$this->limit" />
			<input type="hidden" name="oldpath" value="$oldpath" />
			<input type="hidden" name="option" value="$this->cname" />
			<input type="hidden" name="task" value="" />
			<input type="hidden" name="act" value="{$_REQUEST['act']}" />
		</div>

		</form>
		
FORM_END;

	}

	function multiOptionList ($name, $title, $options, $current, $tooltip=null) {
		$alternatives = explode(',',$options);
		$already = explode(',', $current);
		?>
		<tr>
	    <td width="30%" valign="top" align="right">
	  	<b><?php echo $title; ?></b>&nbsp;
	    </td>
	    <td valign="top">
		<?php
		foreach ($alternatives as $one) {
			if (in_array($one,$already)) $mark = 'checked="checked"';
			else $mark = '';
			$value = $name.'_'.$one;
			echo "<input type=\"checkbox\" name=\"$value\" $mark />$one";
		}
		if ($tooltip) echo '&nbsp;'.tooltip($tooltip);
		echo '</td></tr>';
	}

	function tooltip ($text) {
		return '<a href="javascript:void(0)"  onmouseover="return escape('."'".$text."'".')">'.$this->interface->makeImageURI('tooltip.png').'</a>';
	}

}

