<?php

/**************************************************************
* This file is part of Glossary
* Copyright (c) 2008 Martin Brampton
* Issued as open source under GNU/GPL
* For support and other information, visit http://remository.com
* To contact Martin Brampton, write to martin@remository.com
*
* See header in glossary.php for further details
*/

if (!defined('_CMSAPI_ABSOLUTE_PATH')) die ('This software requires the Jaliro environment');

class glossaryAdminHTML extends glossaryBasicHTML {
	protected $configuration = null;
	protected $interface = null;
	protected $pageNav = null;
	protected $clist = '';
	protected $act = '';

	public function __construct ($controller, $limit, $clist, $full_name) {
	    parent::__construct($controller, $limit);
		$this->configuration = aliroComponentConfiguration::getInstance($full_name);
		$this->interface = glossaryAbstract::getInterface();
		$this->clist = $clist;
	}

	protected function show ($string) {
		return $string;
	}

	function fieldset ($legend, $fields) {
		return <<<FIELD_SET

		<div class="cmsapifieldset">
		<fieldset>
			<legend>$legend</legend>
			$fields
		</fieldset>
		</div>

FIELD_SET;

	}

	function simpleCheckBox (&$object, $property, $title, $value=1) {
		if (is_object($object) AND @$object->$property) $checked = "checked='checked'";
		else $checked = '';
		$html = <<<INPUT_BOX

		<div>
			<label for="$property" style="width=30%"><strong>$title</strong></label>
			<input id="$property" class="inputbox" type="checkbox" $checked name="$property" value="$value" />
		</div>

INPUT_BOX;

		return $html;
	}

	function editLink ($id, $linkname, $containerid=0) {
		$url = $this->interface->indexFileName()."?option={$this->cname}&amp;act={$this->act}&amp;task=edit&amp;cfid={$id}";
		if ($containerid) $url .= "&amp;containerid=$containerid";
		return <<<EDIT_LINK
		
		<a href="$url">$linkname</a>
		
EDIT_LINK;

	}


}