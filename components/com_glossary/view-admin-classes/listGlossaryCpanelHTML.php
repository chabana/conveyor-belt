<?php

/**************************************************************
 * This file is part of Glossary
 * Copyright (c) 2008 Martin Brampton
 * Issued as open source under GNU/GPL
 * For support and other information, visit http://remository.com
 * To contact Martin Brampton, write to martin@remository.com
 *
 * More details in glossary.php
 */

if (!defined('_CMSAPI_ABSOLUTE_PATH')) die ('This software requires the Jaliro environment');

class listGlossaryCpanelHTML extends glossaryAdminHTML
{

    function addIcon($image, $url, $text, $newWindow = false, $modal = false)
    {
        $lang =& JFactory::getLanguage();

        $newWindow = ($newWindow) ? ' target="_blank"' : '';
        $modal = ($modal) ? ' class="modal" rel="{handler: \'iframe\', size: {x: 415, y: 250}}" ' : '';

        return $icon_html='
        <div>
            <div class="icon">
                <a href="'. $url.'"'.$newWindow.$modal.'>'
            .JHtml::_('image', 'components/com_glossary/images/admin/' . $image, NULL, NULL)
            .'<span>'.$text.'</span>
                </a>
            </div>
        </div>';
    }

    private function display($service)
    {


        return $this->addIcon($service[2], "{$this->interface->indexFileName()}?option=com_glossary&act={$service[1]}", $service[0]);


    }

    public function view()
    {

        JToolbarHelper::title($this->show(_GLOSSARY_COMPONENT_TITLE) . ' - ' . $this->show(_GLOSSARY_CPANEL));


        $basic = array(
            array(_GLOSSARY_CPANEL_MANAGE_GLOSSARIES, 'glossaries', 'categories.png'),
            array(_GLOSSARY_CPANEL_MANAGE_ENTRIES, 'entries', 'addedit.png'),
            array(_GLOSSARY_CPANEL_CONFIG, 'config', 'config.png'),
            array(_GLOSSARY_CPANEL_ABOUT, 'about', 'user.png'),
        );

        $controls = '';
        foreach ($basic as $service) $controls .= $this->display($service);

        $stat_pane = JHtml::_('sliders.start', 'stat-pane');
        $welcome_message = JHtml::_('sliders.panel', _GLOSSARY_CPANEL_WELCOME.' '._GLOSSARY_COMPONENT_TITLE);

        echo <<<CONTROL_PANEL



		<table width="100%" border="0">
	    <tr>
            <td width="50%" valign="top">
                <div id="cpanel">
                {$controls}
                </div>
            </td>

            <td width="50%" valign="top">
			{$stat_pane}
			{$welcome_message}

                <table class="adminlist">
                    <tr>
                        <td>
                            <div>
                                <p><b>Glossary</b><br>
        The glossary component was originally based on Arthur Konze's
		Akobook Guestbook component and on the Weblinks component. It has
		subsequently been extensively modified by Martin Brampton, and
		version 2.5 was a total rewrite, although retaining essentially the
		same design.  Version 2.5 is the starting point for further development.
		</p>

        <p><b>License</b><br>
        Glossary is free software but is strictly copyright;
        you can redistribute it and/or modify it under the terms
        of the <a href="http://www.gnu.org/licenses/gpl.html" target="_blank">GNU General
        Public License version 2</a> as published by the Free Software Foundation. This program is
        distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
        even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
        See the GNU General Public License for more details.</p>
		<p>Up to version 1.3 the Glossary was developed by Michelle Farren; development
		was then carried forward up to version 1.5 by Sascha Claren.
		Bernhard Zechmann created 1.9.x versions ( <a href="http://www.zechmann.com" target="_blank">www.zechmann.com </a> )
		All upcoming versions are developed and released by Martin Brampton
		( <a href="http://www.remository.com" target="_blank">www.remository.com </a> )

		</p>
                            </div>
                        </td>
                    </tr>
                </table>
			</td>

	</tr>
	</table>




CONTROL_PANEL;

    }
}