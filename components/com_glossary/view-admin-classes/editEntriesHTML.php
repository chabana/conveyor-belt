<?php

/**************************************************************
* This file is part of Glossary
* Copyright (c) 2008-9 Martin Brampton
* Issued as open source under GNU/GPL
* For support and other information, visit http://remository.com
* To contact Martin Brampton, write to martin@remository.com
*
* Please see glossary.php for more details
*/

if (!defined('_CMSAPI_ABSOLUTE_PATH')) die ('This software requires the Jaliro environment');

class editEntriesHTML extends glossaryAdminHTML {
	
	function edit ($entry, $glossaries) {

        JToolbarHelper::title($this->show(_GLOSSARY_COMPONENT_TITLE) . ' - ' . $this->show(_GLOSSARY_EDIT_ENTRIES));
        JHtml::_('formbehavior.chosen', 'select');

		$yescheck = $entry->published ? 'checked="checked"' : '';
		$nocheck = $entry->published ? '' : 'checked="checked"';

		$parser_class = MARKDOWN_PARSER_CLASS;
		$parser = new $parser_class;

		echo <<<EDIT_ENTRY

	<div class="adminform glossarymain">
	    <form action="{$this->interface->indexFileName()}" method="post" id="adminForm" name="adminForm">
		    <div class="span6 fltlft glossaries">
                <ul class="adminformlist">
                    <li>
                    <label for="catid">{$this->show(_GLOSSARY_GLOSSARY_SELECT)}</label>
                    <select name="catid" id="catid" class="glossary inputbox">
                        {$this->makeGlossarySelect($glossaries, $entry)}
                    </select>
                    </li>
                    <li>
                        <label for="tletter">{$this->show(_GLOSSARY_LETTER)}</label>
                        <input name="tletter" id="tletter" type="text" class="inputbox" value="$entry->tletter" />
                    </li>
                    <li>
                        <label for="tterm">{$this->show(_GLOSSARY_TERM)}<span class="star">&nbsp;*</span></label>
                        <input name="tterm" id="tterm" type="text" class="widthspec inputbox" size="100" value="$entry->tterm" />
                    </li>
                    <li>
                        <label for="trelated">{$this->show(_GLOSSARY_RELATED_TERMS)}</label>
                        <input name="trelated" id="trelated" type="text" class="widthspec inputbox" size="100" value="$entry->trelated" />
                    </li>
                    <li>
                        <label for="taliases">{$this->show(_GLOSSARY_ALIASES)}</label>
                        <input name="taliases" id="taliases" type="text" class="widthspec inputbox" size="100" value="$entry->taliases" />
                    </li>
                    <li>
                        <label for="tdefinition">{$this->show(_GLOSSARY_DEFINITION)}<span class="star">&nbsp;*</span></label>
                        <textarea name="tdefinition" id="tdefinition" class="widthspec inputbox" rows="6" cols="40">$entry->tdefinition</textarea>
                    </li>
                    <li>
                            <label for="tdefinitionmd">{$this->show(_GLOSSARY_APPEARS_AS)}{$this->showHTML($parser->transform($entry->tdefinition))}</label>

                    </li>



                            <input type="hidden" name="task" value="" />
                            <input type="hidden" name="act" value="$this->act" />
                            <input type="hidden" name="option" value="com_glossary" />
                            <input type="hidden" name="glossid" value="$entry->catid" />
                            <input type="hidden" name="id" value="$entry->id" />
                </ul>
            </div>
            <div class="span6 fltlft glossaries">
                <ul class="adminformlist">
                    <li>
                            <label for="tname">{$this->show(_GLOSSARY_AUTHOR_NAME)}</label>
                            <input name="tname" id="tname" type="text" class="widthspec inputbox" size="100" value="$entry->tname" />
                    </li>
                    <li>
                            <label for="tloca">{$this->show(_GLOSSARY_LOCALITY)}</label>
                            <input name="tloca" id="tloca" type="text" class="widthspec inputbox" size="100" value="$entry->tloca" />
                    </li>
                    <li>
                            <label for="tmail">{$this->show(_GLOSSARY_MAIL)}</label>
                            <input name="tmail" id="tmail" type="text" class="widthspec inputbox" size="100" value="$entry->tmail" />
                    </li>
                    <li>
                            <label for="tpage">{$this->show(_GLOSSARY_PAGE)}</label>
                            <input name="tpage" id="tpage" type="text" class="widthspec inputbox" size="100" value="$entry->tpage" />
                    </li>
                    <li>
                            <label for="tdate">{$this->show(_GLOSSARY_DATE)}</label>
                            <input name="tdate" id="tdate" type="text" class="widthspec inputbox" size="100" value="$entry->tdate" />
                    </li>
                    <li class="radio">
                            <label for="published">{$this->show(_GLOSSARY_PUBLISHED)}</label>
                            <input name="published" id="published" type="radio" class="inputbox" value="1" $yescheck /><span>{$this->show(_CMSAPI_YES)}</span>
                            <input name="published" type="radio" class="inputbox" value="0" $nocheck /><span>{$this->show(_CMSAPI_NO)}</span>
                    </li>
                </ul>
            </div>
    	</form>

    </div>
		
EDIT_ENTRY;

	}
	
	function makeGlossarySelect ($glossaries, $entry) {
		$optionlist = '';
		foreach ($glossaries as $glossary) {
			$selected = ($glossary->id == $entry->catid) ? ' selected="selected"' : '';
			$optionlist .= <<<OPTION_ENTRY
		
			<option value="$glossary->id"$selected>$glossary->name</option>
			
OPTION_ENTRY;

		}
		return $optionlist;
	}
	
}