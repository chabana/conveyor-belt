<?php

/**************************************************************
* This file is part of Glossary
* Copyright (c) 2008 Martin Brampton
* Issued as open source under GNU/GPL
* For support and other information, visit http://remository.com
* To contact Martin Brampton, write to martin@remository.com
*
* More details in glossary.php
*/

if (!defined('_CMSAPI_ABSOLUTE_PATH')) die ('This software requires the Jaliro environment');

class listGlossaryGlossariesHTML extends glossaryAdminHTML {

	function __construct ($controller, $limit, $clist, $full_name) {
		parent::__construct($controller, $limit, $clist, $full_name);
	}

	function view ($glossaries, $search) {

        JToolbarHelper::title($this->show(_GLOSSARY_COMPONENT_TITLE) . ' - ' . $this->show(_GLOSSARY_LIST_GLOSSARIES));

        JHtml::_('formbehavior.chosen', 'select');

        $listing = '';
		//$i = 0;

		foreach ($glossaries as $i=>$glossary) {
			$listing .= '<tr>
					<td>
						<input type="checkbox" id="cb'.$i.'" name="cfid[]" value="'.$glossary->id.'" onclick="Joomla.isChecked(this.checked);" />
					</td>
					<td>'.$glossary->id.'</td>
					<td><a href="'.$this->interface->indexFileName().'?option=com_glossary&amp;act=glossaries&amp;task=edit&amp;id='.$glossary->id.'">'.$glossary->name.'</a></td>
					<td>'.$glossary->description.'</td>
					<td>'.JHtml::_('jgrid.published', $glossary->published, $i, '', true, 'cb').'</td>
				</tr>';

			//$i++;
		}

		//$count = count($glossaries);
		echo <<<GLOSSARY_LIST
		

		<form action="{$this->interface->indexFileName()}" method="post" id="adminForm" name="adminForm">

		<table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist table">
			<thead>
				<tr>
					<th width="5" align="left">
						<input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" />
					</th>
					<th>{$this->show(_GLOSSARY_ID)}</th>
					<th>{$this->show(_GLOSSARY_NAME)}</th>
					<th>{$this->show(_GLOSSARY_DESCRIPTION)}</th>
					<th>{$this->show(_GLOSSARY_PUBLISHED)}</th>
				</tr>
			</thead>
			
GLOSSARY_LIST;
			
		$this->pageNav->listFormEnd('com_glossary');

        if (!$listing) $listing = '<tr><td colspan="8"><div class="alert alert-no-items">'._GLOSSARY_NO_RESULTS.'</div></td></tr>';

        echo <<<GLOSSARY_LIST
					
			<tbody>
				$listing
			</tbody>
		</table>
		</form>				
				
GLOSSARY_LIST;

	}
}