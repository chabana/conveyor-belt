<?php

/**************************************************************
 * This file is part of Glossary
 * Copyright (c) 2008-9 Martin Brampton
 * Issued as open source under GNU/GPL
 * For support and other information, visit http://remository.com
 * To contact Martin Brampton, write to martin@remository.com
 *
 * Please see glossary.php for more details
 */

if (!defined('_CMSAPI_ABSOLUTE_PATH')) die ('This software requires the Jaliro environment');

class editGlossariesHTML extends glossaryAdminHTML
{

    function edit($glossary)
    {

        $title = $this->show(_GLOSSARY_COMPONENT_TITLE) . ' - ' . $this->show(_GLOSSARY_EDIT_GLOSSARIES);
        JToolbarHelper::title($title);


        $yescheck = $glossary->published ? 'checked="checked"' : '';
        $nocheck = $glossary->published ? '' : 'checked="checked"';

        echo <<<EDIT_GLOSSARY
		<div class="adminform glossarymain">
            <form action="{$this->interface->indexFileName()}" method="post" id="adminForm" name="adminForm">
                <div class="span6 fltlft glossaries">
                    <ul class="adminformlist">
                        <li>
                            <label for="name">{$this->show(_GLOSSARY_NAME)}<span class="star">&nbsp;*</span></label>
                            <input name="name" id="name" type="text" class="inputbox" value="$glossary->name" />
                        </li>
                        <li>
                            <label for="description">{$this->show(_GLOSSARY_DESCRIPTION)}</label>
                            <input name="description" id="description" type="text" class="inputbox" size="100" value="$glossary->description" />
                        </li>
                        <li>
                            <label for="language">{$this->show(_GLOSSARY_GLOSS_LANGUAGE)}</label>
                            <input name="language" id="language" type="text" class="inputbox" value="$glossary->language" />
                        </li>
                        <li class="radio">
                            <label for="published">{$this->show(_GLOSSARY_PUBLISHED)}</label>
                             <input name="published" id="published" type="radio" class="inputbox" value="1" $yescheck /><span>{$this->show(_CMSAPI_YES)}</span>
                             <input name="published" type="radio" class="inputbox" value="0" $nocheck /><span>{$this->show(_CMSAPI_NO)}</span>
                        </li>
                        <input type="hidden" name="task" value="" />
                        <input type="hidden" name="act" value="$this->act" />
                        <input type="hidden" name="option" value="com_glossary" />
                        <input type="hidden" name="id" value="$glossary->id" />
                        </li>
                    </ul>
                 </div>
            </form>
		</div>
EDIT_GLOSSARY;

    }
}