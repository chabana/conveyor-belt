<?php

/**************************************************************
* This file is part of Glossary
* Copyright (c) 2008-9 Martin Brampton
* Issued as open source under GNU/GPL
* For support and other information, visit http://remository.com
* To contact Martin Brampton, write to martin@remository.com
*
* More details in glossary.php
*/

if (!defined('_CMSAPI_ABSOLUTE_PATH')) die ('This software requires the Jaliro environment');

class listGlossaryEntriesHTML extends glossaryAdminHTML {

	function __construct ($controller, $limit, $clist, $full_name) {
		parent::__construct($controller, $limit, $clist, $full_name);
	}

public function view ($entries, $glossaries, $search, $defn, $catid) {

        JToolbarHelper::title($this->show(_GLOSSARY_COMPONENT_TITLE) . ' - ' . $this->show(_GLOSSARY_LIST_ENTRIES));
        JHtml::_('formbehavior.chosen', 'select');

		$parser_class = MARKDOWN_PARSER_CLASS;
		$parser = new $parser_class;
		$listing = '';
		$gmanager = glossaryGlossaryManager::getInstance();
		$i = 0;
		foreach ($entries as $i=>$entry) {
			$glossary = $gmanager->getByID($entry->catid);
			$glossaryname = is_object($glossary) ? $glossary->name : '';
			$entry->tdefinition = $parser->transform($entry->tdefinition);
			$link = <<<EDIT_LINK
{$this->interface->indexFileName()}?option=com_glossary&amp;act=entries&amp;task=edit&amp;id=$entry->id
EDIT_LINK;
			$listing .= '<tr>
					<td>
						<input type="checkbox" id="cb'.$i.'" name="cfid[]" value="'.$entry->id.'" onclick="Joomla.isChecked(this.checked);" />
					</td>
					<td>'.$entry->id.'</td>
					<td><a href="'.$link.'">'.$this->showHTML($entry->tterm).'</a></td>
					<td>'.$entry->tletter.'</td>
					<td>'.$glossaryname.'</td>
					<td width="30%">'.$this->showHTML($entry->tdefinition).'</td>
					<td>'.$entry->tdate.'</td>
					<td>'.JHtml::_('jgrid.published', $entry->published, $i, '', true, 'cb').'</td>
				</tr>';
			


			$i++;
		}
		$count = count($entries);
        $filter_term = _GLOSSARY_FILTER_TERM;
        $filter_definition= $this->show(_GLOSSARY_FILTER_DEFINITION);
		echo <<<GLOSSARY_HEAD
		

		<form action="{$this->interface->indexFileName()}" method="post" id="adminForm" name="adminForm">
		<table id="glossaryheader" cellpadding="4" cellspacing="0" border="0" width="100%">

    	<tr>

    		<td>
			<div class="js-stools-container-bar">
                <label class="element-invisible" for="filter_search" aria-invalid="false">
                {$filter_term}</label>
                <div class="btn-wrapper input-append">
                    <input type="text" placeholder="{$filter_term}" value="{$search}" id="filter_search" name="search">
                        <button title="" class="btn hasTooltip" type="submit"  data-original-title="Search"><i class="icon-search"></i></button>
                </div>

                <div class="btn-wrapper">
                <button title="" class="btn hasTooltip js-stools-btn-clear" onclick="document.id('filter_search').value='';this.form.submit();" type="button" data-original-title="Clear">Clear</button>
                </div>
			</div>

			</td>
    		<td>

    		<div class="js-stools-container-bar">
                <label class="element-invisible" for="filter_defn" aria-invalid="false">
                {$filter_definition}</label>
                <div class="btn-wrapper input-append">
                    <input type="text" placeholder="{$filter_definition}" value="{$defn}" id="filter_defn" name="defn">
                        <button title="" class="btn hasTooltip" type="submit"  data-original-title="Search"><i class="icon-search"></i></button>
                </div>

                <div class="btn-wrapper">
                <button title="" class="btn hasTooltip js-stools-btn-clear" onclick="document.id('filter_defn').value='';this.form.submit();" type="button" data-original-title="Clear">Clear</button>
                </div>
			</div>

			</td>
    		<td>
				<select name="catid" id="glossaryselect" onchange="document.adminForm.submit();">
					{$this->glossarySelect($glossaries, $catid)}
				</select>
			</td>

    			<noscript><input type="submit" value="{$this->show(_GLOSSARY_REFRESH)}" /></noscript>

    	</tr>
    	</table>
		<table id="glossarylist" cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist table">
			<thead>
				<tr>
					<th width="5" align="left">
						<input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" />
					</th>
					<th>{$this->show(_GLOSSARY_ID)}</th>
					<th>{$this->show(_GLOSSARY_TERM)}</th>
					<th>{$this->show(_GLOSSARY_LETTER)}</th>
					<th>{$this->show(_GLOSSARY_GLOSSARY_CAT)}</th>
					<th>{$this->show(_GLOSSARY_DEFINITION)}</th>
					<th>{$this->show(_GLOSSARY_DATE)}</th>
					<th>{$this->show(_GLOSSARY_PUBLISHED)}</th>
				</tr>
			</thead>
			
GLOSSARY_HEAD;

		$this->pageNav->listFormEnd('com_glossary');
		if (!$listing) $listing = '<tr><td colspan="8"><div class="alert alert-no-items">'._GLOSSARY_NO_RESULTS.'</div></td></tr>';

		echo <<<GLOSSARY_LIST

			<tbody>
				$listing
			</tbody>
		</table>
		</form>
				
GLOSSARY_LIST;

	}
	
	private function glossarySelect($glossaries, $catid) {
		$options = '';
		foreach ($glossaries as $glossary) {
			$selected = ($catid == $glossary->id) ? 'selected="selected"' : '';
			$options .= <<<GLOSS_OPTION
	
			<option value="$glossary->id" $selected>$glossary->name</option>
		
GLOSS_OPTION;

		}
		return $options;
	}
	
}