<?php

/**************************************************************
* This file is part of Glossary
* Copyright (c) 2008-9 Martin Brampton
* Issued as open source under GNU/GPL
* For support and other information, visit http://remository.com
* To contact Martin Brampton, write to martin@remository.com
*
* Please see glossary.php for more details
*/

// CMS independent check to prevent direct execution
if (basename(@$_SERVER['REQUEST_URI']) == basename(__FILE__)) die ('This software is for use within a larger system');

class glossaryInstaller extends cmsapiInstaller {
	protected static $dbcreators = array(
'#__glossary' => "CREATE TABLE `jos_glossary` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tletter` char(1) NOT NULL DEFAULT '',
  `tterm` varchar(255) NOT NULL DEFAULT '',
  `tfirst` varchar(255) NOT NULL,
  `tdefinition` text NOT NULL,
  `trelated` text NOT NULL,
  `tname` varchar(20) NOT NULL DEFAULT '',
  `tloca` varchar(60) DEFAULT NULL,
  `tmail` varchar(60) DEFAULT NULL,
  `tpage` varchar(150) DEFAULT NULL,
  `tdate` datetime DEFAULT NULL,
  `tcomment` text,
  `tedit` enum('y','n') NOT NULL DEFAULT 'n',
  `teditdate` datetime DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `catid` int(3) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `term` (`tterm`,`catid`),
  FULLTEXT KEY `tdefinition` (`tdefinition`)
) DEFAULT CHARSET=utf8",
'#__glossaries' => "CREATE TABLE `jos_glossaries` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `language` varchar(25) NOT NULL DEFAULT '',
  `published` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `isdefault` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8",
'#__glossary_aliases' => "CREATE TABLE `jos_glossary_aliases` (
  `termalias` varchar(255) NOT NULL,
  `tfirst` varchar(255) NOT NULL,
  `termid` int(11) NOT NULL,
  PRIMARY KEY (`termid`,`termalias`),
  KEY `termaliases` (`termalias`)
) DEFAULT CHARSET=utf8",
'#__glossary_cache' => "CREATE TABLE `jos_glossary_cache` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stamp` int(11) NOT NULL DEFAULT '0',
  `md5hash` char(32) NOT NULL,
  `sha1hash` char(40) NOT NULL,
  `fixup` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_hash` (`md5hash`,`sha1hash`)
) DEFAULT CHARSET=utf8",
);
	
	protected $cname = 'com_glossary';
	protected $ctitle = 'Glossary';
	protected $calias = 'aglossary';
	
	public function createDB () {
		foreach (self::$dbcreators as $tablename=>$tablecreate) {
			if (!$this->database->tableExists($tablename)) {
				$this->database->doSQL($tablecreate);
			}
		}
	}
	
	public function updateDB () {
		$manager = new aliroDatabaseManager();
		$tablejson = file_get_contents(_CMSAPI_ABSOLUTE_PATH.'/components/com_glossary/json/gen_database.json');
		$manager->updateTables ($tablejson);
	}
	
	public function customGlossaryInstall () {
		// This code is application specific
		call_user_func(array('glossaryEntry', 'setFirstWords'), 5000);
		// glossaryEntry::setFirstWords(5000);
		$this->database->setQuery("SELECT COUNT(*) FROM #__glossaries");
		if (!$this->database->loadResult()) {
			if ($this->database->fieldExists('#__categories', 'name')) {
				$this->database->setQuery("SELECT id, name, description, published FROM #__categories WHERE section = 'com_glossary'");
				$categories = $this->database->loadObjectList();
			}
			else $categories = null;
			if ($categories) {
				foreach ($categories as $category) {
					$this->database->setQuery("INSERT INTO #__glossaries (id, name, description, published) VALUES ('$category->id', '$category->name', '$category->description', $category->published)");
					$this->database->query();
				}
			}
			else {
				$this->database->setQuery("INSERT INTO #__glossaries (name, description, published) VALUES ('Glossary', 'Glossary of terms used on this site', 1)");
				$this->database->query();
			}
		}
	}
}
