<?php

/**************************************************************
* This file is part of Glossary
* Copyright (c) 2008-9 Martin Brampton
* Issued as open source under GNU/GPL
* For support and other information, visit http://remository.com
* To contact Martin Brampton, write to martin@remository.com
*
* Please see glossary.php for more details
*/

// NOTE These values are used ONLY on installation.  Once installation has
// been done, the configuration information is held in the database.
//
// Set properties of pretty much any kind e.g.
// $this->property = 'some value';

if (!defined('_CMSAPI_ABSOLUTE_PATH')) return;

$this->utf8 = "0";
$this->admin_utf8 = "0";
$this->show_list = "1";
$this->allowentry = "1";
$this->autopublish = "1";
$this->notify = "0";
$this->notify_email = "";
$this->from_email = "";
$this->thankuser = "1";
$this->mail_thanks = 'Thank you for contributing a new term to the glossary.';
$this->perpage = "15";
$this->maxdefn = "0";
$this->pagespread = "4";
$this->sorting = "";
$this->showrating = "";
$this->anonentry = "0";
$this->hideauthor = "0";
$this->showcategories = "0";
$this->beginwith = "nothing";
$this->shownumberofentries = "1";
$this->showcatdescriptions = "1";
$this->useeditor = "0";
$this->show_alphabet = "1";
$this->show_alphabet_below = "0";
$this->show_search="1";
$this->show_author="0";
$this->show_soundex="1";
$this->strip_accents = "0";
$this->language = "";
$this->triggerplugins = "0";
$this->termsonly = "0";