<?php

/**************************************************************
* This file is part of$current_dir Glossary
* Copyright (c) 2008-9 Martin Brampton
* Issued as open source under GNU/GPL
* For support and other information, visit http://remository.com
* To contact Martin Brampton, write to martin@remository.com
*
* See header in glossary.php for further details
*/

// Don't allow direct linking
if (!defined( '_VALID_MOS' ) AND !defined('_JEXEC')) die( 'Direct Access to this location is not allowed.' );

if (!function_exists('version_compare') OR version_compare(PHP_VERSION, '5.2.3') < 0) die ('Sorry, this version of Glossary requires PHP version 5.2.3 or later');
if (!class_exists('plgSystemJaliro') OR !plgSystemJaliro::$active OR !defined('_CMSAPI_ABSOLUTE_PATH')) die ('Sorry, this version of Glossary requires the Jaliro plugin to be installed and active');

// Define CMS and Glossary environment
require_once(_CMSAPI_ABSOLUTE_PATH.'/components/com_glossary/glossary.class.php');

$installer = new glossaryInstaller();
$installer->makeMenuEntry();


new cmsapiAdminManager('glossary', 'com_glossary');

require_once(JPATH_ADMINISTRATOR.'/components/com_glossary/toolbar.glossary.php');