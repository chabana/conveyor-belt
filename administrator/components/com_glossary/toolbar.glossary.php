<?php

/**************************************************************
* This file is part of Glossary
* Copyright (c) 2008-9 Martin Brampton
* Issued as open source under GNU/GPL
* For support and other information, visit http://remository.com
* To contact Martin Brampton, write to martin@remository.com
*
* Please see glossary.php for more details
*/

// ensure this file is being included by a parent file
if (!defined( '_VALID_MOS' ) AND !defined('_JEXEC')) die( 'Direct Access to this location is not allowed.' );


$interface = glossaryInterface::getInstance();
$mosConfig_absolute_path = $interface->getCfg('absolute_path');
$mosConfig_lang = $interface->getCfg('lang');
$mosConfig_live_site = $interface->getCfg('live_site');
$mosConfig_sitename = $interface->getCfg('sitename');
$Large_Text_Len = 300;
$Small_Text_Len = 150;
$gconfig = aliroComponentConfiguration::getInstance('com_glossary');
$forcelang = $gconfig->language != '';
$interface->loadLanguageFile ($gconfig, $forcelang, 'images/glossary/language');

$toolbar = new glossaryToolbar();