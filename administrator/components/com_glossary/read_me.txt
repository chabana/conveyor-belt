<----------->
COMPONENT Glossary 2.8 for Joomla 2.5, Joomla 1.5, J-One-Plus, Maliro and Aliro
July 2012
Martin Brampton
<----------->

New version to run over Jaliro on Joomla 2.5, Joomla 1.5, Aliro, J-One-Plus or Maliro

<----------->
COMPONENT Glossary 2.7 Joomla 1.5, Joomla 1.0.x, Mambo and Aliro
July 2011
Martin Brampton
<----------->

Tidy up release.

<----------->
COMPONENT Glossary 2.62, 2.63, 2.64 for Joomla 1.5, Joomla 1.0.x, Mambo and Aliro
March 2009
Martin Brampton
<----------->

Bug fixes.

<----------->
COMPONENT Glossary 2.61 for Joomla 1.5, Joomla 1.0.x, Mambo and Aliro
March 2009
Martin Brampton
<----------->

Bug fixes, mainly to do with character sets.

<----------->
COMPONENT Glossary 2.60 for Joomla 1.5, Joomla 1.0.x, Mambo and Aliro
February 2009
Martin Brampton
<----------->

Enhancements:

So far as possible, any English strings have been replaced by language file entries
The search has been enhanced by adding a Soundex option (best for English unfortunately)
Items beginning with a number are grouped under heading '0-9'
More configuration options.

Major Bugs fixed:

Stopped publication of new user terms when auto-publish is set to no
Incorrect rendering of page controls
Categories now shown in alphabetic order