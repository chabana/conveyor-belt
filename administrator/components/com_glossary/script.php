<?php

/**************************************************************
 * This file is part of Glossary
 * Copyright (c) 2008-9 Martin Brampton
 * Issued as open source under GNU/GPL
 * For support and other information, visit http://remository.com
 * To contact Martin Brampton, write to martin@remository.com
 *
 * Please see glossary.php for more details
 */

// CMS independent check to prevent direct execution
if (basename(@$_SERVER['REQUEST_URI']) == basename(__FILE__)) die ('This software is for use within a larger system');

if (!function_exists('version_compare') OR version_compare(PHP_VERSION, '5.2.3') < 0) {
    echo 'Sorry, this version of Glossary requires PHP version 5.2.3 or above<br />';
    echo 'Please uninstall Glossary and upgrade your PHP version.<br />';
    echo 'Install and activate the Jaliro Plugin, then reinstall Glossary.<br />';
    return;
}
if (!class_exists('plgSystemJaliro') OR !plgSystemJaliro::$active OR !defined('_CMSAPI_ABSOLUTE_PATH')) {
    echo 'Sorry, this version of Glossary requires the Jaliro plugin to be installed and active<br />';
    echo 'Please uninstall Glossary. Install and active the Jaliro Plugin, then reinstall Glossary.<br />';
    return;
}


class com_glossaryInstallerScript
{
    /**
     * method to install the component
     *
     * @return void
     */
    function install($parent)
    {
        // Define CMS environment
        require_once(_CMSAPI_ABSOLUTE_PATH . '/components/com_glossary/glossary.class.php');

// End of CMS environment definition

        $gconfig = aliroComponentConfiguration::getInstance('com_glossary', true);
// TO DO does something need to be done here?
//$gconfig->deleteCache();
        // $parent is the class calling this method
        /*if ('Joomla' == _CMSAPI_CMS_BASE) {
           $integrator = new cmsapiInstaller();
           $message = $integrator->installModulesPlugins();
           if ($message) echo '<br />Installation of modules/plugins had errors:<br />'.$message;
        }*/
        glossaryAbstract::setNewAutoload();
        $installer = new glossaryInstaller();
        $installer->createDB();
        $installer->updateDB();
        $installer->customGlossaryInstall();
    }

    /**
     * method to uninstall the component
     *
     * @return void
     */
    function uninstall($parent)
    {
        // Define CMS environment
        require_once(_CMSAPI_ABSOLUTE_PATH . '/components/com_glossary/glossary.class.php');

// End of CMS environment definition

        $gconfig = aliroComponentConfiguration::getInstance('com_glossary', true);
// TO DO does something need to be done here?
//$gconfig->deleteCache();
        // $parent is the class calling this method
        /*if ('Joomla' == _CMSAPI_CMS_BASE) {
            $integrator = new cmsapiInstaller();
            $message = $integrator->installModulesPlugins(true);
            if ($message) echo '<br />Uninstallation of modules/plugins had errors:<br />' . $message;
        }*/
        aliroDatabase::getInstance()->doSQL("DELETE FROM #__menu WHERE link LIKE '%com_glossary%'");
    }

    /**
     * method to update the component
     *
     * @return void
     */
    function update($parent)
    {
        // $parent is the class calling this method

    }

    /**
     * method to run before an install/update/uninstall method
     *
     * @return void
     */
    function preflight($type, $parent)
    {
        // $parent is the class calling this method
        // $type is the type of change (install, update or discover_install)

    }

    /**
     * method to run after an install/update/uninstall method
     *
     * @return void
     */
    function postflight($type, $parent)
    {
        // $parent is the class calling this method
        // $type is the type of change (install, update or discover_install)

    }
}


